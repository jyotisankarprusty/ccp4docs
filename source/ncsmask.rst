NCSMASK (CCP4: Supported Program)
=================================

NAME
----

**ncsmask** - averaging mask manipulation program

SYNOPSIS
--------

| **ncsmask** [ **MSKIN** *foo.msk* / **XYZIN** *foo.pdb* ] **MSKOUT**
  *bar.msk*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

This program performs operations on non-crystallographic symmetry masks.
It is ideal as a precursor to \`\ ``dm``', since it contains a more
sophisticated treatment of mask overlap, and may avoid some problems
commonly caused by poorly defined masks.

Input can be from a PDB file, in which case a mask is generated from
spheres around all atoms. A crude 'sphere mask' can be constructed by
making a PDB file with only a few atoms and specifying a large radius.

Alternatively input can be from a CCP4 format map/mask file.

The mask may then be modified by expanding or contracting, smoothing the
boundary, reducing from a multimer to a monomer (in some cases),
removing crystallographic- or non-crystallographic symmetry overlap and
altering the mask box size.

This program should not normally be used for generating solvent masks,
since it does not impose crystallographic symmetry or unit cell repeat.
See `mapmask <mapmask.html>`__ for further details. However, these masks
can be used in DM 1.7.1 or later.

INPUT/OUTPUT FILES
------------------

 XYZIN
    is the input PDB file
 MSKIN
    is the input mask file
 MSKOUT
    is the output mask file

KEYWORDED INPUT
---------------

The GRID and SYMMETRY cards are useful if input is from a .pdb file,
otherwise the information is taken from the input mask header. Available
keywords are:

    `**AVERAGE** <#average>`__, `**AXIS** <#axis>`__,
    `**EXPAND** <#expand>`__, `**GRID** <#grid>`__,
    `**MONOMER** <#monomer>`__, `**NOTRIM** <#notrim>`__,
    `**OVERLAP** <#overlap>`__, `**PEAK** <#peak>`__,
    `**RADIUS** <#radius>`__, `**SMOOTH** <#smooth>`__,
    `**SYMMETRY** <#symmetry>`__, `**XYZLIM** <#xyzlim>`__

GRID <nx> <ny> <nz> \| SPACING <x> [ <y> <z> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Set the sampling grid over the unit cell to <nx>\*<ny>\*<nz>. The
  default is 1.0 Angstrom spacing.
| Alternatively, the SPACING subkeyword allows you to set the grid
  spacing; if only *x* is specified then this gives *x* Angstrom spacing
  along all axes, otherwise *x*, *y* and *z* are the spacings along each
  axis.

The GRID keyword is ignored if a mask is input.

SYMMETRY <spacegroup name/number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the spacegroup symmetry for the mask. The default is taken from the
input mask if a mask is input, or P1 if a .pdb file is input.

AXIS <x> <y> <z>
~~~~~~~~~~~~~~~~

Change the default output axis order. The first axis specified will vary
fastest.

XYZLIM <x1> <x2> <y1> <y2> <z1> <z2>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Changes the default mask extent. <x1>-<z2> are either in grid units or
fractional coordinates. If the new extent is smaller than the input mask
extent, the input will be truncated, if the new extent is larger the
mask will be padded with zeros. The default is either the extent of the
input mask or the extent of the input .pdb file + sphere radius + 3.0
Angstrom. See however the `NOTRIM <#notrim>`__ card.

RADIUS <rad>
~~~~~~~~~~~~

Set the sphere radius for building a mask out of atoms. Default=3.0
Angstrom. A much larger value could be used to build a mask around
C-alpha or sulphur coordinates, for example.

PEAK <num>
~~~~~~~~~~

Pick the <num> largest connected regions of the mask, and delete all
others. \`PEAK 1' is particularly useful for tidying up a correlation
mask (see `maprot <maprot.html>`__).

EXPAND <rad>
~~~~~~~~~~~~

Expand/contract the mask. This function expands the mask by <rad>
Angstrom in all directions. The mask will not be expanded beyond the
current mask extent, if this is required the `XYZLIM <#xyzlim>`__ card
should be used to increase the mask extent. A negative value will
contract the mask.

SMOOTH <rad>
~~~~~~~~~~~~

Smooth the mask using a sphere of radius <rad>. This will eliminate
features smaller than <rad> from the surface of the mask. Maximum radius
~5 grid points.

AVERAGE <nncs>
~~~~~~~~~~~~~~

Enter NC-symmetry elements for overlap removal. (Note that this card has
the same format as the \`\ ``dm``' \`AVERAGE' card). This card is
followed by <nncs> rotation/translation matrices on subsequent lines in
either CCP4 or O/RAVE format.

 CCP4 Formats (see also `lsqkab <lsqkab.html#rotate>`__):
    ::

        ROTATE EULER <alpha> <beta> <gamma>     (Euler angles)
        TRANSLATE <t1> <t2> <t3>

 or
    ::

        ROTATE POLAR <omega> <phi> <kappa>      (Polar angles)
        TRANSLATE <t1> <t2> <t3>

 or
    ::

        ROTATE MATRIX <r11> <r12> <r13> <r21> <r22> <r23> <r31> <r32> <r33>
        TRANSLATE <t1> <t2> <t3>

 O/RAVE Format
    OMAT
    <r11> <r21> <r31>
    <r12> <r22> <r32>
    <r13> <r23> <r33>
    <t1> <t2> <t3>
    where
    x' = <r11>x + <r12>y + <r13>z + <t1>
    y' = <r21>x + <r22>y + <r23>z + <t2>
    z' = <r31>x + <r32>y + <r33>z + <t3>
    (note that the rotation matrix is transposed with respect to CCP4
    matrix format.)

These are the operations which map the density in the region covered by
the input mask onto the other equivalent regions. The first operator
must be the identity matrix. The mask is input in CCP4 mask format on
the input file label MSKIN, and should cover just one monomer or
averaging domain, NOT the whole unit cell.

MONOMER
~~~~~~~

Reduce a multimer mask to cover only a monomer. This option will
generate a mask covering an arbitrarily chosen monomer from a multimer
in the case where the NCS consists ONLY of a SINGLE PROPER ROTATION AXIS
of any order. An arbitrary segment covering an angle of 2\*pi/<nncs> is
selected and the rest of the mask is removed.

OVERLAP [ <ncycle> ]
~~~~~~~~~~~~~~~~~~~~

Perform overlap removal. Overlap removal will be performed by removing
pixels from the surface of the mask. The mask is overlapped with its
crystallographic- and non-crystallographic symmetry related partners
(see `SYMMETRY <#symmetry>`__ and `AVER <#average>`__ cards). The
overlap removal calculation may not remove all overlap on the first
cycle, if <ncycle> is specified, overlap removal will be repeated this
number of times.

NOTRIM
~~~~~~

Disable mask trimming. The default is to reduce the mask extent to
obtain the minimum box which will contain the final output mask. With
NOTRIM, the mask extent will be the same as the input mask or the values
on the `XYZLIM <#xyzlim>`__ card if it is present.

NOTES
-----

The calculations are performed in the following order:

#. Mask input/generation
#. Peak picking
#. Expansion/Contraction
#. Smoothing
#. Monomer generation
#. Overlap removal
#. Extent trimming

If you want to do them in a different order, run the program several
times.

Read your output. When removing overlap, check that the input and output
masks fill a sensible volume of the unit cell. Don't complain until you
have done this.

EXAMPLES
--------

To remove the overlap from a mask for input to \`\ ``dm``':

::

    #
    ncsmask mskin ins.msk mskout ins1.msk << eof
    AVERAGE 2
    OMAT
     1.000  0.000  0.000
     0.000  1.000  0.000
     0.000  0.000  1.000
     0.000  0.000  0.000
    OMAT
     -0.88284    -0.46960    -0.00849
     -0.46899     0.88238    -0.03808
      0.02537    -0.02964    -0.99924
     82.80000     0.10000   -34.10000
    OVERLAP
    END
    eof

To make a mask from pdb file and remove overlap (note that \`\ ``dm``'
doesn't care about the grid on which input masks are calculated):

::

    #
    ncsmask xyzin ins.pdb mskout ins1.msk << eof
    SYMMETRY R3
    RADIUS 3.0
    AVERAGE 2
    ROTATE EULER 0.0 0.0 0.0
    TRANSLATE 0.0 0.0 0.0
    ROTATE EULER -29.6  180.0    0.0
    TRANSLATE 0.3  0.1 -0.1
    OVERLAP
    END
    eof

SEE ALSO
--------

`dm <dm.html>`__, `lsqkab <lsqkab.html>`__, `mapmask <mapmask.html>`__,
`maprot <maprot.html>`__, `bones2pdb <bones2pdb.html>`__.
