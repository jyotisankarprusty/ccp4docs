|All residues| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 2a. Gly & Pro Ramachandran plots
=====================================

|image2|

Description
-----------

The Gly & Pro Ramachandran plots gives separate **Ramachandran plots**
for just the **Gly** and **Pro** residues in the ensemble.

The favourable- and unfavourable regions of both these residue types
differ markedly from those of the other residues. The **darker** the
shaded area on the plots, the **more favourable** the region. The data
on which the shading is based has come from a data set of **163**
non-homologous, high-resolution protein chains chosen from structures
solved by X-ray crystallography to a resolution of **2.0Å** or better
and an *R*-factor no greater than **20%**.

The **numbers in brackets**, following each residue name, show the total
number of **data points** on that graph. The **red numbers** above the
data points are the **reside-numbers** of the residues in question (*ie*
showing those residues lying in unfavourable regions of the plot).

--------------

|All residues| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |All residues| image:: ../uupr.gif
   :target: plot_02.html
.. | | image:: ../12p.gif
.. |image2| image:: plot_02a.gif
   :width: 306px
   :height: 434px
   :target: plot_02a.gif
.. |All residues| image:: ../uupr.gif
   :target: plot_02.html
