|previous page| | | |plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

1. Ramachandran plot
====================

There are **14** parameters defining the `**Ramachandran
plot** <../examples/plot_01.html>`__, as follows:-

--------------

::

    1. Ramachandran plot
    --------------------
    Y     <- Shade in the different regions (Y/N)?
    Y     <- Print the letter-codes identifying the different regions (Y/N)?
    Y     <- Draw line-borders around the regions (Y/N)?
    N     <- Show only the core region (Y/N)?
    1     <- Label residues in: 0=disallow,1=generous,2=allow,3=core regions
    N     <- Produce a COLOUR PostScript file (Y/N)?
    WHITE        <- Background colour
    WHITE        <- Region 0: Disallowed
    CREAM        <- Region 1: Generous
    YELLOW       <- Region 2: Allowed
    RED          <- Region 3: Most favourable, core, region
    BLACK        <- Colour of markers in favourable regions
    RED          <- Colour of markers in unfavourable regions
    N     <- Produce "publication version" of Ramachandran plot (Y/N)?

--------------

Description of options:-
------------------------

-  **Shade in the different regions** - The second option defines
   whether the different regions of the Ramachandran plot are to be
   shaded in.

   Without shading, the regions can still be made out if their borders
   are drawn in (see `Draw line-borders around the
   regions <#DRBORD>`__). In black-and-white, the shading shows the most
   favourable regions in the darkest grey, with the less favourable
   regions being shown in progressively lighter tones. If you are only
   interested in the core region, then set the option `Show only the
   core region <#SHCORE>`__ below to **Y**.

-  **Print the letter-codes identifying the different regions** - The
   next option defines whether the different regions of the Ramachandran
   plot are to be labelled using the appropriate codes: **A**, **B**,
   **L**, *etc*.
-  **Draw line-borders around the regions** - This option defines
   whether borders are drawn round the different regions of the
   Ramachandran plot. If you are only interested in the core region,
   then set the option `Show only the core region <#SHCORE>`__ below to
   **Y**.
-  **Show only the core region** - This option defines whether only the
   core regions (*ie* the most favoured regions: **A**, **B**, and
   **L**) of the Ramachandran plot are to be outlined and/or shaded.
   This gives an easy-to-see guide as to how many residues are inside
   and outside the cores.
-  **Label residues in: 0=disallow,1=generous,2=allow,3=core** - This
   option defines which residues are labelled on the Ramachandran plot.
   Option **0** labels only the residues in the \`\`disallowed''
   regions. Option **1** labels residues in the disallowed and
   \`\`generous'' regions. And so on. In all cases, Gly residues are
   left unlabelled and are identified by a triangular marker.
-  **Produce a COLOUR PostScript file** - This option defines whether a
   colour or black-and-white plot is required. Note that if the `Colour
   all plots <manopt_0a.html>`__ option is set to **Y**, a colour
   PostScript file will be produced irrespective of the setting here.

   The colour definitions on the following lines use the \`names' of
   each colour as defined in the colour table at the bottom of the
   parameter file (see `Colours <manopt_cols.html>`__). If the name of a
   colour is mis-spelt, or does not appear in the colour table, then
   **white** will be taken instead. Each colour can be altered to suit
   your taste and aesthetic judgement, as described in the
   `Colours <manopt_cols.html>`__ section.

-  **Background colour** - This option defines the background colour of
   the page on which the plots are drawn.
-  **Colours** - The various additional colours on the lines that follow
   define the colour of each of the 4 different regions on the
   Ramachandran plot, and the colours of the markers that are located in
   the favourable and unfavourable regions.
-  **Produce "publication version" of plot** - This option allows you to
   plot just the Ramachandran plot with no border around the edges and
   none of the statistics below it, as would be suitable for
   publication. An example is given `here <manopt_01.gif>`__.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_0b.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_02.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_02.html
