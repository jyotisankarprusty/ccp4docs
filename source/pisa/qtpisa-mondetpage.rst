|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Result Pages <qtpisa-resultpages.html>`__ •
`List of Monomers <qtpisa-monlistpage.html>`__ • Monomer Details

.. raw:: html

   </div>

To be written

The Monomers Details Page displays extended summary of and residue-level
data of `monomeric unit <qtpisa-glossary.html#monomeric-unit>`__
selected in the `Result Tree <qtpisa-glossary.html#pisa-result-tree>`__.

***Monomer Summary***

+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
|                     | Interfaces                                                                                                                                                                                                               | Surface                                                                                                                   | Total                                                                                                    |
+=====================+==========================================================================================================================================================================================================================+===========================================================================================================================+==========================================================================================================+
| Atoms               | Total number of atoms in all `interfaces <qtpisa-glossary.html#interface>`__ formed by the monomer. An atom is considered to be in an interface if its accessibility to solvent changes upon interface formation.        | Total number of atoms on the monomer's surface (that is, atoms with non-zero `ASA <qtpisa-glossary.html#asa>`__).         | Total number of atoms in the monomer.                                                                    |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Residues            | Total number of residues in all `interfaces <qtpisa-glossary.html#interface>`__ formed by the monomer. A residue is considered to be in an interface if its accessibility to solvent changes upon interface formation.   | Total number of residues on the monomer's surface (that is, residues with non-zero `ASA <qtpisa-glossary.html#asa>`__).   | Total number of residues in the monomer.                                                                 |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Area, sq.A          | Combined interface area of all `interfaces <qtpisa-glossary.html#interface>`__ formed by the monomer.                                                                                                                    | Total `surface area <qtpisa-glossary.html#surface-area>`__ of the monomer.                                                |                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Solvation Energy,   | Combined solvation energy of all `interfaces <qtpisa-glossary.html#interface>`__ formed by the monomer.                                                                                                                  | Combined `solvation effect <qtpisa-glossary.html#sef>`__ of surface residues.                                             | Monomer's `solvation energy of folding <qtpisa-glossary.html#se-folding>`__ (not defined for ligands).   |
| kcal/mol            |                                                                                                                                                                                                                          |                                                                                                                           |                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Hydrogen bonds      | Total number of hydrogan bonds formed by the monomer in crystal packing.                                                                                                                                                 |                                                                                                                           |                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Salt bridges        | Total number of salt bridges formed by the monomer in crystal packing.                                                                                                                                                   |                                                                                                                           |                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Disulphide bonds    | Total number of disulphide bonds formed by the monomer in crystal packing.                                                                                                                                               |                                                                                                                           |                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+

|  

***Residue accesibility and solvation effects***

Residue

ASA

BSA

SEF

SE

Residue ID:

[c]:r n.i

where

+-----+-------+---------------------------+
| c   |  :    | character chain ID        |
+-----+-------+---------------------------+
| r   |  :    | residue name              |
+-----+-------+---------------------------+
| s   |  :    | residue sequence number   |
+-----+-------+---------------------------+
| i   |  :    | residue insertion code    |
+-----+-------+---------------------------+

Residue's `accessible surface area <qtpisa-glossary.html#asa>`__, in
square angstroms.

Residue's sum contribution into `buried surface
areas <qtpisa-glossary.html#bsa>`__ of all interfaces, formed by the
monomer in crystal packing, in kcal/mol.

Residue's `solvation effect <qtpisa-glossary.html#sef>`__, in kcal/mol.

Residue's sum contribution into the `solvation
energy <qtpisa-glossary.html#solvation-energy>`__ of all interfaces,
formed by the monomer in crystal packing, in kcal/mol.

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
