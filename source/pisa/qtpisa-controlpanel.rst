|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • Control panel

.. raw:: html

   </div>

Control Panel is located on the left side of QtPISA window. It consists
of three main parts:

--------------

+------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image2|   |        | `Toolbar <qtpisa-toolbar.html>`__: *buttons for most frequently used functions.*                                                                                                                                                                                                                                                                                                                                                                 |
+------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image3|   |        | Result Tree. *PISA generates a considerable amount of output data, presented mostly in tabular form. For clarity and convenience, tables are groupped in `Result Pages <qtpisa-resultpages.html>`__, and pages are arranged in a tree structure, which is navigatable using the tree widget in the panel. A particular result page is displayed in the right part of QtPISA window, when a corresponding item of the Result Tree is selected.*   |
+------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |image4|   |        | Progress Bar: *shows up only during calculations or loading results.*                                                                                                                                                                                                                                                                                                                                                                            |
+------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

--------------

**Note 1**: the tree items are expandable/collapsable, and may have
several levels of embedment.

| **Note 2**: the buttons in the `Toolbar <qtpisa-toolbar.html>`__, as
  well as the main menu items (such as Print/Save/View/Help), always
  work in the context of the currently selected result page. This means
  that, e.g., Save may address to either a `Monomeric
  Unit <qtpisa-glossary.html#monomeric-unit>`__, or an
  `Interface <qtpisa-glossary.html#interface>`__, or an
  `Assembly <qtpisa-glossary.html#assembly>`__, depending on the
  currently selected item of the Result Tree.
|  

--------------

| ***See also***

-  `The Toolbar <qtpisa-toolbar.html>`__

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
.. |image2| image:: toolbar.png
.. |image3| image:: result-tree.png
.. |image4| image:: progress-bar.png

