|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Start and run <qtpisa-start.html>`__ •
Configuration

.. raw:: html

   </div>

For PISA to function properly, the following directories need to be
created and files placed:

+-------------+---------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Directory   | Contents            | Description                                                                                                                                                                                                                                              |
+=============+=====================+==========================================================================================================================================================================================================================================================+
| SCRATCH     | temporary           | Scratch area for keeping temporary files                                                                                                                                                                                                                 |
|             | files               |                                                                                                                                                                                                                                                          |
+-------------+---------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SRS         | graph.srs           | CCP4's Storage, Retrieaval and Search database, which contains descriptions of some 10,000 ligand (small chemical) structures.                                                                                                                           |
|             | index.srs           |                                                                                                                                                                                                                                                          |
|             | struct.srs          |                                                                                                                                                                                                                                                          |
+-------------+---------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| MolRef      | molref.idx          | Database with chemical properties of ligand (small chemical) structures.                                                                                                                                                                                 |
|             | molref.rdt          |                                                                                                                                                                                                                                                          |
+-------------+---------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| PisaStore   | agents.dat          | List of common precipitation agents, parameters for free Gibbs energy calculations, statistical distributions of interface properties in the PDB, translation between symmetry operations used by CCP4 and RCSB and space symmetry group descriptions.   |
|             | asm\_params.dat     |                                                                                                                                                                                                                                                          |
|             | intfstats.dat       |                                                                                                                                                                                                                                                          |
|             | rcsb\_symops.dat    |                                                                                                                                                                                                                                                          |
|             | syminfo\_pisa.lib   |                                                                                                                                                                                                                                                          |
+-------------+---------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Help        | index.html          | HTML-formatted Help pages.                                                                                                                                                                                                                               |
|             | qtpisa-\*.html      |                                                                                                                                                                                                                                                          |
|             | qtpisa-help.css     |                                                                                                                                                                                                                                                          |
|             | \*.png              |                                                                                                                                                                                                                                                          |
+-------------+---------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

These directories need to be specified in the Configuration Settings
dialog, similar to one below:

|image2|

The dialog can be raised by selecting the View/Settings menu item
(qtpisa/Preferences on Mac OSX), by clicking on the Settings button in
the `Toolbar <qtpisa-toolbar.html>`__, or pressing the Ctrl-G (⌘-G on
Mac OSX) hot key.

The directories may be typed directly in the corresponding fields, or
specified by navigation in file system, using browse buttons on the
right from the fields.

In addition to the directories, configuration includes specification of
a molecular graphics viewer, which PISA may use for displaying
`monomeric units <qtpisa-glossary.html#monomeric-unit>`__,
`interfaces <qtpisa-glossary.html#interface>`__ and
`assemblies <qtpisa-glossary.html#assembly>`__. Available options
include None (visualisation switched off),
`Rasmol <http://rasmol.org/>`__, `Jmol <http://jmol.sourceforge.net/>`__
and `CCP4 MG <http://www.ccp4.ac.uk/MG/>`__. Set the Viewer combobox to
the desired option, and specify path to the viewer in the following
field. For Jmol, give path to Jmol.jar, for CCP4 MG on Mac OSX, give
path to QtMG.app folder. In other cases, give path to viewer's
executable.

The last configuration settings is for the `interface interaction
radar <qtpisa-intfdetpage.html#radar>`__, which switches it from Simple
to Detail mode. In the latter mode, the radar gives a more detail
statistical information on the interaction value of interfaces. However,
the additional information is more difficult to interpret, and expected
to be useful only in rare cases. Therefore, Simple mode is the
recommended one, while Detail setting should be considered as an
experimental feature.

The changes actualise only when Apply button is pressed. In order to
discard changes, push Cancel button. All configuration may be read from
text-formatted `Configuration File <qtpisa-cfgfile.html>`__, which is
also used for running pisa in `command-prompt <qtpisa-cmdprompt.html>`__
(GUI-less) mode. In order to read configuratuon file, push Load CFG
button, and navigate to the file, after which push Apply button.

Once configured, QtPISA stores all configuration items in user's home
area, and will load them automatically at next start. QtPISA may be
reconfigured as many times, as necessary. Note that pushing the Apply
button resets QtPISA, which erases all current calculation results.

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
.. |image2| image:: settings.png

