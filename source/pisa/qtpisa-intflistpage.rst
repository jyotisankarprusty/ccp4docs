|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Result Pages <qtpisa-resultpages.html>`__ •
List of Interfaces

.. raw:: html

   </div>

List of Interfaces gives the summary of all
`interfaces <qtpisa-glossary.html#interface>`__, identified in input
data. This includes interfaces, found in the asymmetric unit
(represented by input file and expanded, if necessary, by applying
`NCS <qtpisa-glossary.html#NCS>`__ operations), but also interfaces
between ASUs and unit cells, for which the necessary symmetry expansion
of ASU is performed. Only unique interfaces are listed in the page. The
following data is displayed for each interface:

+--------------------------------------+--------------------------------------+
| Monomer 1                            | `Monomer                             |
|                                      | ID <qtpisa-glossary.html#monomer-id> |
|                                      | `__                                  |
|                                      | of 1st interfacing `monomeric        |
|                                      | unit <qtpisa-glossary.html#monomeric |
|                                      | -unit>`__.                           |
|                                      | 1st monomer is always in its         |
|                                      | original position, as given in the   |
|                                      | input file.                          |
+--------------------------------------+--------------------------------------+
| Monomer 2                            | `Monomer                             |
|                                      | ID <qtpisa-glossary.html#monomer-id> |
|                                      | `__                                  |
|                                      | of 2nd interfacing `monomeric        |
|                                      | unit <qtpisa-glossary.html#monomeric |
|                                      | -unit>`__.                           |
|                                      | 2nd monomer is in position           |
|                                      | identified by the symmetry           |
|                                      | operation, specified in 4th column.  |
+--------------------------------------+--------------------------------------+
| Spec.                                | Special inteface properties:         |
|                                      | +-------+---------+---------+------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      | |       | **f**   |   :     | fixed  |
|                                      | interface. This means that PISA cons |
|                                      | iders the interfacing monomeric unit |
|                                      | s as permanently attached to each ot |
|                                      | her. See `here <qtpisa-datapage.html |
|                                      | #fixed-interfaces>`__ why PISA may p |
|                                      | ermanently fix ligands on macromolec |
|                                      | ules.                                |
|                                      |             |                        |
|                                      | +-------+---------+---------+------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      | |       | **x**   |   :     | crysta |
|                                      | lographically propagating interface. |
|                                      |  If such interface is considered to  |
|                                      | be permanently engaged, i.e., that i |
|                                      | nterfacing monomers are permanently  |
|                                      | bound to each other, then, due to sy |
|                                      | mmetry properties of given crystal,  |
|                                      | these monomers form an infinite subs |
|                                      | tructure.   |                        |
|                                      | +-------+---------+---------+------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
|                                      | |       | **o**   |   :     | overla |
|                                      | p detected. This means that coordina |
|                                      | te data is not perfect, and interfac |
|                                      | ing monomers overlap in interface ar |
|                                      | ea.                                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |             |                        |
|                                      | +-------+---------+---------+------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+
| Sym. op-n                            | Symmetry operation (in fractional    |
|                                      | space), applied to 2nd monomer.      |
+--------------------------------------+--------------------------------------+
| Sym. ID                              | Symmetry ID, corresponding to        |
|                                      | symmetry operation in the previous   |
|                                      | column. The ID has form N\_KLM,      |
|                                      | where N is serial number of the      |
|                                      | symmetry operation in the            |
|                                      | corresponding space symmetry group   |
|                                      | (as used by the                      |
|                                      | `PDB <http://www.pdb.org>`__); KLM   |
|                                      | give fractional coordinates of the   |
|                                      | unit cell relative to the original   |
|                                      | cell, placed at 555.                 |
+--------------------------------------+--------------------------------------+
| Area                                 | `Interface                           |
|                                      | area <qtpisa-glossary.html#interface |
|                                      | -area>`__.                           |
+--------------------------------------+--------------------------------------+
| Delta G                              | `Solvation                           |
|                                      | energy <qtpisa-glossary.html#solvati |
|                                      | on-energy>`__                        |
|                                      | gain upon interface formation,       |
|                                      | kcal/mol.                            |
+--------------------------------------+--------------------------------------+
| P-value                              | `Hydrophobic                         |
|                                      | P-value <qtpisa-glossary.html#hp-val |
|                                      | ue>`__                               |
|                                      | of the interface. The lower P-value, |
|                                      | the more specific, or statistically  |
|                                      | surprising, the interface.           |
+--------------------------------------+--------------------------------------+
| Nhb                                  | Number of hydrogen bonds formed      |
|                                      | between the interfacing monomers.    |
+--------------------------------------+--------------------------------------+
| Nsb                                  | Number of salt bridges formed        |
|                                      | between the interfacing monomers.    |
+--------------------------------------+--------------------------------------+
| Nds                                  | Number of disulphide bonds formed    |
|                                      | between the interfacing monomers.    |
+--------------------------------------+--------------------------------------+

| 
|  

--------------

| ***See further***

`Interface details <qtpisa-intfdetpage.html>`__

-  Chemical bonds
-  Interfacing residues

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
