MAPTONA4 (CCP4: Supported Program)
==================================

NAME
----

**maptona4** - Convert binary map file to and from na4 ascii format

SYNOPSIS
--------

| **maptona4 mapin** *foo.map* **mapout** *foo.na4*
| **maptona4 mapin** *foo.na4* **mapout** *foo.map*

DESCRIPTION
-----------

Input files
~~~~~~~~~~~

 MAPIN (stream 1)
    input map, formatted (ascii) or binary CCP4 format. The program
    reads the first line of the map to determine automatically whether
    the file is formatted or not.
 SYMOP (stream 4)
    symmetry library (doesn't normally need to be specified)

Output file:
~~~~~~~~~~~~

 MAPOUT(stream 2)
    output map: this will be formatted if the input map is binary and
    vice versa.

No Input is required, other than the command line, and input file
assignments.

ACCURACY
--------

The program gets index values from the ranges for the density values for
REAL to CHARACTER\*4 inter-conversion, with REALs converted to INTEGER
in the range 0 to 2\*\*24-1 (Negative numbers are converted to 2's
complement.) Then it converts INTEGER to CHAR\*4 using 64-character
translation table.

After an ascii readable header is output to the foo.na4 file the map
data is stored as characters. A sample of an output file is given below:

::


    MAPTONA4 HEADER
    TITLE   
    ISOMORPHOUS DIFFERENCE PATTERSON FOR TEST DERIVATIVE 3.0A.                      
    AXIS           Z       X       Y
    GRID          36      64      48
    XYZLIM         0      18       0      32       0      47
    SPACEGROUP             4
    MODE           2
    CELL        35.720    62.990    44.760    90.000   102.400    90.000
    RHOLIM      -930.269         7562.85         1.75892         133.812    
    INDXR4     0  10
    END HEADER

    SECTION       0

    RWhWJ22J5OF4xfIBybD004hXzWQIzTmXzzyKznCQzmSO0dcw15Z30ORyzfn+zess03FS0Jeb0D0szlqt
    zZc0039e0X290SXD0GRI0SXE0X29039ezZc0zlqt0D0s0Jea03FSzesszfn+0ORy15Z30dcwzmSOznCQ
    zzyKzTmXzWQI04hWybD0xfIB5OF4J22I
    HonnElom4qWlxB-Uxjd9zscIzl9KzQx4zyP308ij09XB0r8C1Okh0rq1zy8AzjOj07ZY0VAT0Vhg07hE
    zmXZzw0F0+lh04nF00iA0P980iDZ0FlVzeVczfaq01L-02HezmoNzZ3AzVBjzsLK0NVC072ezE1QzInp
    08Kg02SQzaxv04fLzT5wxIhIziUY-IcH

The maximum error is relative to the largest absolute value in the
column so in general you will have an error of about 1 in 10\*\*7, which
is not likely to cause any problems.

EXAMPLES
--------

::


      maptona4  mapin mir.map  mapout mir.na4
      maptona4  mapin mir.na4  mapout new_mir.map

AUTHOR
------

| Phil Evans MRC-LMB (original mapexchange program)
| Ian Tickle Birkbeck College (na4 modifications)

SEE ALSO
--------

Other map/mask conversion programs:

-  `xdlmapman <xdlmapman.html>`__
-  `mama2ccp4 <mama2ccp4.html>`__
