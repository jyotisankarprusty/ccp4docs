PDB\_MERGE (CCP4: Supported Program)
====================================

NAME
----

**pdb\_merge** - merge two coordinate files into one

SYNOPSIS
--------

| **pdb\_merge xyzin1** *foo1\_in.pdb* **xyzin2** *foo2\_in.pdb*
  **xyzout** *foo\_out.pdb*
| [Key-worded input file]

DESCRIPTION
-----------

``pdb_merge`` can be used to merge two coordinate files into one. The
program currently has two modes:

merge chains
    The residue and chain labels in XYZIN1 and XYZIN2 are taken as is,
    and transferred to XYZOUT. This means that if both input files have
    a chain A, then XYZOUT will have a chain A containing all the atoms
    from both input files. If you want e.g. to join two domains into one
    chain, then this might be what you want. But if the input chains are
    not prepared correctly, then you may have residues with duplicate
    atoms.
nomerge
    For this option, it is assumed that chains in XYZIN1 and XYZIN2 are
    distinct from each other. Therefore, if a chain label is found in
    XYZIN2 that is already used in XYZIN1, then it is relabelled. For
    example, if both XYZIN1 and XYZIN2 contain chain A, then XYZOUT will
    contain chain A (from XYZIN1) and chain B (from XYZIN2).

If you want to merge more than 2 files, then ``pdb_merge`` should be run
iteratively.

INPUT AND OUTPUT FILES
----------------------

XYZIN1
~~~~~~

First input coordinate file. The input format is detected automatically
(PDB, mmCIF or MMDB).

XYZIN2
~~~~~~

Second input coordinate file. The input format is detected automatically
(PDB, mmCIF or MMDB).

XYZOUT
~~~~~~

Output coordinate file, in format selected by `OUTPUT <#output>`__
keyword (default PDB format)

KEYWORDED INPUT
---------------

NOMERGE
~~~~~~~

Specify the nomerge option, see `DESCRIPTION <#description>`__ above.
The default is to merge chains with the same chain label.

OUTPUT [ PDB \| CIF \| BIN ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (default is PDB)
| Select output file format: PDB = standard PDB format, CIF = mmCIF
  format, and BIN = MMDB binary format.

END
~~~

End keyworded input.

PROGRAM OUTPUT
--------------

The program currently gives a short summary of the commands received.

SEE ALSO
--------

`ffjoin <ffjoin.html>`__

AUTHOR
------

Martyn Winn, Daresbury Laboratory, UK
