CCP4 v7.0 Program References
============================

Any publication arising from use of the CCP4 software suite should
include both references to the specific programs used (see below) and
the following reference to the CCP4 suite:

| M. D. Winn et al. *Acta. Cryst.* **D67**, 235-242 (2011)
| "Overview of the CCP4 suite and current developments"
| [ doi:10.1107/S0907444910045749 ]

Such citations are vital to the CCP4 project, and individual program
authors, in maintaining funding and thus being able to develop CCP4
further.

The following list is meant as an aid to citation of individual
programs, but *is not complete*. It has been extracted from the
individual program documentation, so look there for more details. In
addition, many relevant articles can be found in the proceedings of the
2010 Study Weekend, published as a special issue of Acta Cryst D.

--------------

refmac
------

-  "Application of Maximum Likelihood Refinement" G. Murshudov, A.Vagin
   and E.Dodson, (1996) in the Refinement of Protein structures,
   Proceedings of Daresbury Study Weekend.
-  "Refinement of Macromolecular Structures by the Maximum-Likelihood
   method" G.N. Murshudov, A.A.Vagin and E.J.Dodson, (1997) in Acta
   Cryst. **D53**, 240-255.
-  "Incorporation of Prior Phase Information Strengthen
   Maximum-Likelihood Structure Refinemen" N.J.Pannu, G.N.Murshudov,
   E.J.Dodson and R.J.ReadA (1998) Acta Cryst. section **D54**,
   1285-1294.
-  "Efficient anisotropic refinement of Macromolecular structures using
   FFT" G.N.Murshudov, A.Lebedev, A.A.Vagin, K.S.Wilson and E.J.Dodson
   (1999) Acta Cryst. section **D55**, 247-255.
-  "Use of TLS parameters to model anisotropic displacements in
   macromolecular refinement" M. Winn, M. Isupov and G.N.Murshudov
   (2000) Acta Cryst. 2001:D57 122-133
-  "Fisher's information matrix in maximum likelihood molecular
   refinement." Steiner R, Lebedev, A, Murshudov GN. Acta Cryst. 2003
   D59: 2114-2124
-  "Macromolecular TLS refinement in REFMAC at moderate resolutions,"
   Winn MD, Murshudov GN, Papiz MZ. Method in Enzymology, 2003:374
   300-321
-  "Direct incorporation of experimental phase information in model
   refinement" Skubak P, Murshudov GN, Pannu NS. Acta Cryst. 2004 D60:
   2196-2201
-  "REFMAC5 dictionary: organisation of prior chemical knowledge and
   guidelines for its use." Vagin, AA, Steiner, RS, Lebedev, AA,
   Potterton, L, McNicholas, S, Long, F and Murshudov, GN. Acta Cryst.
   2004 D60: 2284-2295

abs
---

-   Hao, Q. **(2004) *J. Appl. Cryst.* 37, 498-499. `ABS: a program to
   determine absolute configuration and evaluate anomalous scatterer
   substructure <http://scripts.iucr.org/cgi-bin/paper?hi5556>`__.**

acorn
-----

-  Foadi,J., Woolfson,M.M., Dodson,E.J., Wilson,K.S., Yao Jia-xing and
   Zheng Chao-de (2000) *Acta. Cryst.* **D56**, 1137-1147.
-  Yao Jia-xing (2002) *Acta. Cryst.* **D58**, 1941-1947.
-  Yao Jia-xing, Woolfson,M.M., Wilson,K.S. and Dodson,E.J. (2002) *Z.
   Kristallogr.* **217**, 636-643.
-  Yao Jia-xing, Woolfson,M.M., Wilson,K.S. and Dodson,E.J. (2005)
   *Acta. Cryst.* **D61**, 1465-1475.

almn
----

-  R.A.Crowther, The Fast Rotation Function in *The Molecular
   Replacement Method* ed. M.G. Rossman, *Int. Sci. Rev. Ser.*, no. 13,
   pp. 173-178 (1972).
-  E.J.Dodson, in *Molecular Replacement*, Proceedings of the Daresbury
   Study Weekend, (1985) DL/SCI/R23

amore
-----

-  J.Navaza, *Acta Cryst.* **A50**, 157-163 (1994)
   (General reference.)
-  J.Navaza. *Acta Cryst.* **A43**, 645-653 (1987)
   (Radial quadrature instead of bessel expansion)
-  J.Navaza. *Acta Cryst.* **A46**, 619-620 (1990)
   (Stable recurrence relationship for rotation matrices.)
-  G.A.Bentley, Some applications of the phased translation function
   using calculated phases in *Molecular Replacement*, Proceedings of
   the Daresbury Study Weekend, (1992) DL/SCI/R33
-  E.E.Castellano et al., Fast Rigid-body Refinement for
   Molecular-replacement Techniques, *J. Appl. Cryst.* **25**, 281-4
   (1992).
-  J.Navaza. *Acta Cryst.* **D49**, 588-591 (1993)
-  F.L.Hirshfeld *Acta Cryst.* **A24**, 301-311 (1968)

anisoanl
--------

-  Martyn Winn, *CCP4 Newsletter* March 2001, **39**
   `ANISOANL - analysing anisotropic displacement
   parameters <http://www.ccp4.ac.uk/newsletter39/03_anisoanl.html>`__
-  R.E.Rosenfield, K.N.Trueblood and J.D.Dunitz, *Acta Cryst*, **A34**,
   828 - 829 (1978)
   Rigid-body postulate.
-  T.R.Schneider, *Proc. CCP4 Study Weekend*, 133 - 144 (1996).
   Application of rigid-body postulate to protein SP445.
-  V. Schomaker and K.N.Trueblood, *Acta Cryst.*, **B24**, 63 - 76
   (1968)
   Original description of TLS.
-  V. Schomaker and K.N.Trueblood, *Acta Cryst.*, **B54**, 507 - 514
   (1998)
   Description of THMA program for small molecules, which fits TLS
   parameters (and more) to refined U values.

areaimol
--------

-  B.Lee and F.M.Richards, *J.Mol.Biol.*, **55**, 379-400 (1971)
-  E.B.Saff and A.B.J.Kuijlaars, *The Mathematical Intelligencer*,
   **19**, 5-11 (1997)
   http://www.math.vanderbilt.edu/~esaff/texts/161.pdf

beast
-----

-  R.J. Read (2001), "Pushing the boundaries of molecular replacement
   with maximum likelihood." *Acta Cryst.* D\ **57**: 1373-1382.
   `Click here to
   download. <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html#beastref>`__
-  R.J. Read (1999), "Detecting outliers in non-redundant diffraction
   data." *Acta Cryst.* D\ **55**: 1759-1764. `Click here to
   download. <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html#outliarref>`__
-  C. Chothia & A.M. Lesk (1986), "The relation between the divergence
   of sequence and structure in proteins." *EMBO J.* **5**: 823-826.

bulk
----

-  A. Fokine, G. Capitani, M.G. Grutter, A. Urzhumtsev, *J. Appl.
   Cryst.* **36** 352-355 (2003)
-  A. Fokine and A. Urzhumtsev, *Acta Cryst.* **A58** 72-74 (2002)
-  A. Fokine and A. Urzhumtsev, *Acta Cryst.* **D58** 1387-1392 (2002)

cavenv
------

-  A. Volbeda, private communication or with reference (in french): Anne
   Volbeda, Speleologie des hydrogenases a nickel et a fer. In: "Les
   Ecoles Physique et Chimie du Vivant, numero 1 - avril 1999, Analyse
   de l'organisation tridimensionnelle des proteines", pp 47-52.

ccp4i
-----

-  Elizabeth Potterton, Peter Briggs, Maria Turkenburg and Eleanor
   Dodson (2003) *Acta. Cryst.* **D59** 1131-1137 "A graphical user
   interface to the CCP4 program suite"

chainsaw
--------

-  N. Stein, *J. Appl. Cryst.* **41**, 641 - 643 (2008).
   CHAINSAW: a program for mutating pdb files used as templates in
   molecular replacement.
-  R. Schwarzenbacher et al., *Acta Cryst.* **D60**, 1229 - 1236 (2004).
   The importance of alignment accuracy for molecular replacement.

crossec
-------

-   Don T. Cromer. \`Calculation of Anomalous Scattering Factors at
   Arbitrary Wavelengths', *J. Applied Cryst.* **16** 437-8 (1983)
-   Cromer and Liberman. *J. Chem. Phys.* **53** 1891-1898 (1970)
-   Jensen. *Physics Letters* **74A** 41-44 (1979)
-   Cromer and Liberman *Acta Cryst.* **A37** 267-268 (1981)
-   Henke, Gullikson and Davis, Atomic Data and Nuclear Data Tables Vol.
   54 No.2 181-342 (1993).
   (See http://xray.uu.se/hypertext/henke.html)
   Note however that they are not given in fine intervals. This is only
   important in the immediate region of an absorption edge where they
   change quickly. In any case near edge features mean that any values
   of f' or f'' for an isolated atom in this region could be unreliable
   as they depend on the environment of the atom.
-   Alain Soyer, *J.Appl.Cryst.* **28** 244 (1995).
   The program Fhkl can plot values from the Henke et al and Cromer et
   al methods, though it is designed for other purposes.
-   D. Waasmaier and A. Kirfel, \`New Analytical Scattering Factor
   Functions for Free Atoms and Ions', *Acta Cryst.* **A51** (1995).
   `FTP Uni
   Wuerzburg <ftp://wrzx02.rz.uni-wuerzburg.de/pub/local/Crystallography/index.txt>`__

ctruncate
---------

-  S. French and K. Wilson, Acta Cryst. A34, 517-525 (1978).
-  T. O. Yeates, Acta Cryst. A44, 142-144 (1980).
-  J. E. Padilla and T. O. Yeates, Acta Cryst. D59, 1124-1130 (2003).
-  P. Zwartz, CCP4 Newsletter 42.
-  Z. Dauter, Acta Cryst. D62, 867 (2006)
-  P. Zwartz, Acta Cryst. D61, 1437 (2006)

detwin
------

-   Rees, D.C. Acta Cryst. (1980) A36, 578-581. The influence of
   Twinning by Merohedry on Intensity Statistics.
-   Redinbo, M.R. and Yeates, T.O. (1993) Acta Cryst. D49, 375-380.
   Structure Determination of Plastocyanin from a Specimen with a
   Hemihedral Twinning Fraction of One-Half.
-   Gomis-Ruth, F.X., Fita, I., Kiefersauer, R., Huber, R., Aviles, F.X.
   and Navaza, J. (1995) Acta Cryst D51, 819-823. Determination of
   Hemihedral Twinning and Initial Structural Analysis of Crystals of
   the Procarboxypeptidase A Ternary Complex.
-   Yeates, T.O. (1997) Methods in Enzymology 276, 344-358. Detecting
   and Overcoming Crystal Twinning.
-   Murray-Rust, P. (1973) Acta Cryst B29, 2559-2566.

distang
-------

-  IUPAC-IUB conventions, *J. Mol. Biol.*, **50**, 1 (1972).

dm
--

-  K. Cowtan (1994), Joint CCP4 and ESF-EACBM Newsletter on Protein
   Crystallography, 31, p34-38.
-  Baker D., Bystroff C., Fletterick R., Agard D. (1994) Acta Cryst D49
   429-439
-  Bricogne, G. (1974) Acta Cryst A30 395-405
-  Brunger, A. T. (1992) Nature 355, 472-474.
-  Cowtan K. D., Main, P. (1993) Acta Cryst D49 148-157
-  Sayre, D. (1974) Acta Cryst A30 180-184
-  Schuller D. (1996) Acta Cryst D52 425-434
-  Swanson, S. (1994) Acta Cryst D50 695-708
-  Wang, B. C. (1985) Methods in Enzymology 115, 90-112
-  Zhang, K. Y. J., Main P. (1990) Acta Cryst A46 377-381
-  Abrahams, J. P. (1995) Acta Cryst D51 371-376
-  K. D. Cowtan, P. Main (1996) Acta Cryst D52 43-48
-  K. D. Cowtan (1999) Acta Cryst D55 1555-1567

dmmulti
-------

-  Baker D., Bystroff C., Fletterick R., Agard D. (1994) Acta Cryst D49
   429-439
-  Bricogne, G. (1974) Acta Cryst A30 395-405
-  Brunger, A. T. (1992) Nature 355, 472-474
-  Cowtan K. D., Main, P. (1993) Acta Cryst D49 148-157
-  Sayre, D. (1974) Acta Cryst A30 180-184
-  Schuller D. (1996) Acta Cryst D52 425-434
-  Swanson, S. (1994) Acta Cryst D50 695-708
-  Wang, B. C. (1985) Methods in Enzymology 115, 90-112
-  Zhang, K. Y. J., Main P. (1990) Acta Cryst A46 377-381

dyndom
------

-  Method:
   S.Hayward, A.Kitao, H.J.C.Berendsen, *Model-Free Methods of Analyzing
   Domain Motions in Proteins from Simulation: A Comparison of Normal
   Mode Analysis and Molecular Dynamics Simulation of Lysozyme*
   **Proteins, Structure, Function and Genetics**, 27, 425, 1997.
-  DynDom main reference:
   S.Hayward, H.J.C.Berendsen, *Systematic Analysis of Domain Motions in
   Proteins from Conformational Change; New Results on Citrate Synthase
   and T4 Lysozyme* **Proteins, Structure, Function and Genetics**, 30,
   144, 1998.
-  Applications:
   B.L.de Groot, S.Hayward, D.van Aalten, A.Amadei, H.J.C.Berendsen,
   *Domain Motions in Bacteriophage T4 Lysozyme; a Comparison between
   Molecular Dynamics and Crystallographic Data* **Proteins, Structure,
   Function and Genetics**, 31, 116, 1998.
   D.Roccatano, A.E.Mark,S.Hayward, *Investigation of the Mechanism of
   Domain Closure in Citrate Synthase by Molecular Dynamics Simulation*
   **J. Mol. Biol.** , 310, 1039, 2001.

fffear
------

-  K. Cowtan (1998), Acta Cryst. D54, 750-756. Modified phased
   translation functions and their application to molecular fragment
   location.
-  K. Cowtan (1998), Acta Cryst. D54, 750-756. Modified phased
   translation functions and their application to molecular fragment
   location.
-  Kleywegt G. J., Jones T. A. (1997) Acta Cryst., D53, 179-185.
   Template convolution to enhance or detect structural features in
   macromolecular electron-density maps.
-  Rossman M. G., Arnold E. (1993) International Tables for
   Crystallography Volume C, Section 2.3: Patterson and molecular
   replacement techniques (Kluwer Academic Publishers).

fffear fragment library
-----------------------

-  K. Cowtan (2001), to be published.
-  K. Cowtan (2001), Fast Fourier feature recognition, Acta Cryst. D57,
   1435-1444.
-  H. M. Berman, J. Westbrook, Z. Feng, G. Gilliland, T. N. Bhat, H.
   Weissig, I. N. Shindyalov, P.E.Bourne (2000) Nucleic Acids Research
   28, 235-242. The Protein Data Bank.
-  L. Holm, C. Sander (1996) Science 273, 595-602. Mapping the protein
   universe.
-  T. Oldfield (1992) J. Mol. Graphics 10, 247-252. SQUID - A program
   for the analysis and display of data from crystallography and
   molecular-dynamics.

ffjoin
------

-  K. Cowtan (1998), Acta Cryst. D54, 750-756. Modified phased
   translation functions and their application to molecular fragment
   location.
-  K. Cowtan (1998), Acta Cryst. D54, 750-756. Modified phased
   translation functions and their application to molecular fragment
   location.
-  Kleywegt G. J., Jones T. A. (1997) Acta Cryst., D53, 179-185.
   Template convolution to enhance or detect structural features in
   macromolecular electron-density maps.

fft
---

-  A.Immirzi, *Crystallographic Computing Techniques*, ed. F.R.Ahmed,
   Munksgaard, p399, (1966).
-  L.F.Ten Eyck, *Acta Cryst.*, **A29**, 183, (1973).
-  R.J.Read and A.J.Schierbeek *J. Appl. Cryst.* **21** 490-495 (1988).

fhscal
------

-  Kraut J, Sieker LC, High DF and Freer ST, *Proc. Nat. Acad. Sci.
   USA*, **48**, 1417-1424 (1962).

findncs
-------

-  Lu, G. (1999) FINDNCS: A program to detect non-crystallographic
   symmetries in protein crystals from heavy-atom sites J. Appl. Cryst.
   **32** 365.

freerflag
---------

-  A.T. Brünger, *Nature* **355**, 472-4 (1992)
-  A.T. Brünger, "Free *R* Value: Cross-validation in crystallography",
   *Methods in Enzym.* **277**, 366-396 (1997).
   See `The Brunger Lab
   Publications <http://xplor.csb.yale.edu/home/papers/>`__ for more
   references on the Free *R*.

fsearch
-------

-  Hao, Q. (2001), *Acta Cryst.* **D57** 1410-1414. "Phasing from an
   Envelope".
-  Q. Liu, A. J. Weaver, T. Xiang, D. J. Thiel and Q. Hao, (2003) *Acta
   Cryst.* **D59** 1016-1019. "Low-resolution molecular replacement
   using a six-dimensional search"

gesamt
------

-  E.Krissinel (2012), Enhanced Fold Recognition using Efficient Short
   Fragment Clustering, *in preparation*

getax
-----

-  C. Vonrhein and G. E. Schulz, *Acta Cryst.*, **D55**, 225 - 229
   (1999)
   Locating proper non-crystallographic symmetry in low-resolution
   electron-density maps with the program GETAX.

lsqkab
------

-  Kabsch W. *Acta. Cryst.* **A32** 922-923 (1976).

maprot
------

-  Stein *et al.*, *Structure* **2**, 45-47 (1994)

matthews\_coef
--------------

-   Matthews, *J.Mol.Biol* **33**, 491-497 (1968).
-   Kantardjieff and Rupp, *Protein Science* **12**, 1865-1871 (2003).

omit
----

-  T.N. Bhat, "CALCULATION OF AN OMIT MAP", *J. Appl. Cryst.*, **21**,
   279-281 (1988)
-  F.M.D.Vellieux and B.W.Dijkstra, "Computation of Bhat's OMIT maps
   with different coefficients", *J. Appl. Cryst.*, **30**, 396-399
   (1997)

overlapmap
----------

-   Branden C. and Jones A., Nature 343 687-689 (1990)
-   Jones Y. and Stuart D, Proc. of CCP4 Study Weekend on Isomorphous
   Replacement And Anomalous Scattering, 1991 39-48.

pdb\_extract
------------

-   Automated and accurate deposition of structures solved by X-ray
   diffraction to the Protein Data Bank
   H. Yang, V. Guranovic, S. Dutta, Z. Feng, H. M. Berman and J. D.
   Westbrook *Acta Cryst.* **D60**, 1833-1839 (2004)

phaser-2.5.0
------------

Citation:
~~~~~~~~~

Other references:
~~~~~~~~~~~~~~~~~

Read, R.J. (2001). `Pushing the boundaries of molecular replacement with
maximum
likelihood <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html#beastref>`__.
*Acta Cryst*. D\ **57**, 1373-1382

Storoni, L.C., McCoy, A.J. & Read, R.J. (2004). `Likelihood-enhanced
fast rotation
functions <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html#LERF>`__\ *.
Acta Cryst* D\ **60**, 432-438

McCoy, A.J., Grosse-Kunstleve, R.W., Storoni, L.C. & Read, R.J. (2005).
`Likelihood-enhanced fast translation
functions <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html#LETF>`__\ *.
Acta Cryst* D\ **61**, 458-464

| McCoy, A.J., Storoni, L.C. and Read, R.J. (2004) `Simple algorithm for
  a maximum-likelihood SAD
  function <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html#SAD>`__
  *Acta Cryst.* D\ **60**, 1220-1228.

polypose
--------

-   R. Diamond, Protein Science, 1, 1279-1287 (1992)

postref
-------

-  Winkler F.K., Schutt C.E. & Harrison S.C., Acta Cryst., (1979), A35,
   901 - 911.
-  Greenhough T.J. & Helliwell J.R., J. Appl. Cryst., (1982), 15, 493 -
   508
-  Greenhough T.J., CCP4 Information Quarterly for Protein
   Crystallography, Feb. 1983, Daresbury Laboratory.

r500
----

-  Clowney et al., *J. Am. Chem. Soc.* **118**, 509-518 (1996)
   (Geometric Parameters in Nucleic Acids: Nitrogenous Bases.)
-  Engh, R.A. & Huber, R. *International Tables for Crystallography
   (Vol. F).* **18.3**, 382-392 (2006)
   (Structure quality and target parameters.)
-  M. Jaskolski, M. Gilski, Z. Dauter & A. Wlodawer *Acta Cryst.*
   **D63** 611-620 (2007)
   (Stereochemical restraints revisited: how accurate are refinement
   targets and how much should protein structures be allowed to deviate
   from them?)
-  Kleywegt, G.G. & Jones, T.A. *Structure* **4**, 1395-1400 (1996)
   (Phi/Psi-chology: Ramachandran revisited. )

rampage
-------

-  S.C. Lovell, I.W. Davis, W.B. Arendall III, P.I.W. de Bakker, J.M.
   Word, M.G. Prisant, J.S. Richardson, D.C. Richardson, Structure
   validation by Calpha geometry: phi,psi and Cbeta deviation.
   *Proteins: Struct. Funct. Genet.* **50** 437-450 (2003).

rantan
------

-  Yao Jia-xing, (1981). *Acta. Cryst.* **A**\ 37, 642-644.
-  Germain,G. and Woolfson,M.M. (1968) *Acta. Cryst.* **B**\ 24, 91-97.
-  Karle,J. and Hauptman,H. (1956) *Acta. Cryst.* **9**, 635.
-  Yao Jia-xing, (1983). *Acta. Cryst.* **A**\ 39, 35-37.

rapper
------

-  P.I.W. de Bakker, M.A. DePristo, D.F. Burke, T.L. Blundell (2002) *Ab
   initio* construction of polypeptide fragments: Accuracy of loop decoy
   discrimination by an all-atom statistical potential and the AMBER
   force field with the Generalized Born solvation model. *Proteins
   Struct. Funct. Genet.* **51** 21-40.

-  S.C. Lovell, I.W. Davis, W.B. Arendall III, P.I.W. de Bakker, J.M.
   Word, M.G. Prisant, J.S. Richardson, D.C. Richardson (2003) Structure
   validation by Calpha geometry: phi,psi and Cbeta deviation.
   *Proteins: Struct. Funct. Genet.* **50** 437-450.

-  M.A. DePristo, P.I.W. de Bakker, S.C. Lovell, T.L. Blundell (2002)
   *Ab initio* construction of polypeptide fragments: Efficient
   generation of accurate, representative ensembles. *Proteins Struct.
   Funct. Genet.* **51** 41-55.

-  M.A. DePristo, P.I.W. de Bakker, R.P. Shetty, T.L. Blundell (2003)
   Discrete restraint-based protein modeling and the Cα-trace problem.
   *Protein Science* **12** 2032-2046.

-  R.P. Shetty, P.I.W. de Bakker, M.A. DePristo, T.L. Blundell (2003)
   The advantages of fine-grained side chain conformer libraries.
   *Protein Engineering* **16** 963-969.

-  M.A. DePristo, P.I.W. de Bakker, T.L. Blundell (2004) Heterogeneity
   and inaccuracy in protein structures solved by X-ray crystallography.
   *Structure (Camb.)* **12** 831-838.

-  M.A. Depristo, P.I.W. de Bakker, R.J. Johnson, T. L. Blundell. (2005)
   Crystallographic refinement by knowledge-based exploration of complex
   energy landscapes. *Structure* **13 (9)** 1311-1319.

-  N. Furnham, T. L. Blundell, M.A. Depristo, T. C. Terwilliger. (2006)
   Is one Solution Good Enough? *Nature Structural & Molecular Biology*
   **13 (3)** 184-185.

-  N. Furnham, Andrew S. Dore, Dimitri Y. Chirgadze, Paul I. W. de
   Bakker, M.A. Depristo, T. L. Blundell. (2006) Knowledge-Based
   Real-Space Explorations for Low-Resolution Structure Determination
   *Structure* **14 (8)** 1313-1320.

restrain
--------

-   Cruickshank D W J (1965) *Computing Methods in Crystallography*, (J
   S Rollett, ed.), pp. 112-116, Oxford, Pergamon Press.
-   Driessen H, Haneef M I J, Harris G W, Howlin B, Khan G and Moss D S
   (1989) *J Appl Cryst.*, **22**, 510-516.
-   Engh R A and Huber R (1991) *Acta Cryst.* **A47**, 392-400.
-   Haneef I, Moss D S, Stanford M J and Borkakoti N (1985) *Acta
   Cryst.*, **A41**, 426-433.
-   Howlin B, Butler S A, Moss D S, Harris G W and Driessen H P C (1993)
   *J. Appl. Crystallogr.* **26**, 622-624.
-   Johnson C K and Levy M A (1974) in *International Tables for X-ray
   Crystallography*, Vol IV (Ibers, J.A. and Hamilton, W.C., eds.), pp.
   320-332.
-   Jones T A, Zou J Y, Cowan S W and Kjeldgaard M (1991) *Acta Cryst.*,
   **A47**, 110-119.
-   Moss D S (1981) *Refinement of protein structures, Proceedings of
   the Daresbury Study Weekend*, (P Machin, ed.), pp. 9-12, Daresbury,
   SERC.
-   Moss D S & Morffew A J (1982) *Comput Chem*, **6**, 1-3.
-   Nielsen K (1977) *Acta Cryst.*, **A33**, 1009-1010.
-   Rees B (1976) *Acta Cryst.*, **A32**, 483-488.
-   Rollett J S (1965) *Computing Methods in Crystallography*, pp.
   38-56. Oxford, Pergamon Press.
-   Waser J (1963) *Acta Cryst.*, **16**, 1091-1094.

revise
------

-   Fan Hai-fu, Woolfson, M.M. & Yao Jia-xing, (1993). *Proc. R. Soc.
   Lond.* **A** 442, 13-32.

rotamer
-------

-  Lovell, S.C., Word, J.M., Richardson, J.S. & Richardson, D.C. "The
   penultimate rotamer library", Proteins: Structure, Function and
   Genetics, Vol.40, 389-408 (2000).
-  Hooft, R.W.W., Vriend, G., Sander, C. & Abola, E.E. "Errors in
   protein structures", Nature, Vol. 381, 272 (1996).

rsps
----

-  Knight, S.D. (2000): *RSPS version 4.0: a semi-interactive
   vector-search program for solving heavy-atom derivatives*, Acta
   Cryst. D **52**, 42-47
-  Knight, S.D. (1989): *Ribulose 1,5-Bisphosphate Carboxylase/
   Oxygenase - A Structural Study*, Thesis, Swedish University of
   Agricultural Sciences, Uppsala.
-  Blundell, T.L. & Johnson, L.N. (1976). *"Protein Crystallography"*,
   Academic Press, London.
-  Stout, G.H. & Jensen, L.H. (1989). *"X-Ray Structure Determination. A
   Practical Guide"*, 2nd edition, John Wiley & Sons, New York.

sapi
----

-  Q. Hao, Y. X. Gu, J. X. Yao, C. D. Zheng and H. F. Fan (2003) *J.
   Appl. Cryst.* **36** 1274-1276. "SAPI: a direct-methods program for
   finding heavy-atom sites with SAD or SIR data."

sc
--

-  Michael C. Lawrence and Peter M. Colman *J. Mol. Biol.*, **234**,
   p946 - p950 (1993)
-  M. L. Connolly *J. Appl. Crystallogr.*, **16**, p548 - p558 (1983)
-  F. M. Richards *Annu. Rev. Biophys. Bioeng*, **6**, p151-176 (1977)
-  A.J. Nicholls *Biophys. J.*, **64**, A116 (1993)

scaleit
-------

-  Normal Probability Analysis:
   Lynne Howell and Dave Smith, *J.Appl. Cryst.* **25** 81-86 (1992)

sfall
-----

-   Agarwal, R.C., Acta Cryst., (1978), A34, 791-809.
-   International Tables for X-ray Crystallography, Vol.IV, (1974),
   Kynoch Press.
-   Ten Eyck, L.F., Acta Cryst., (1977), A33, 486.
-   Bruenger, A.T., Nature 355, 472-4 (1992)
-   International Tables for Crystallography, vol. C, (1995), Kluwer.
-  "Refinement of protein structures", Proceedings of the Daresbury
   Study Weekend 15-16 November, 1980 (Compiled by P.S. Machin, J.W.
   Campbell and M. Elder).

sftools
-------

-  B. Hazes, unpublished results

sigmaa
------

-  Read, R.J.: Acta Cryst. A42 (1986) 140-149.
-  Srinivasan, R.: Acta Cryst. 20 (1966) 143-144.
-  Hauptman, H.: Acta Cryst. A38 (1982) 289-294.
-  Luzzati, V.: Acta Cryst. 6 (1953) 142-152.
-  Rogers, D. in Computing Methods in Crystallography (Rollett,
   J.S.,ed.) (1985) pp. 126-127, Pergamon Press.
-  Hendrickson, W.A. & Lattman, E.E.: Acta Cryst. B26 (1970) 136-143.
-  Bricogne, G.: Acta Cryst. A32 (1976) 832-847.
-  Sim, G.A.: Acta Cryst. 12 (1959) 813-815; 13 (1960) 511-512.
-  Read, R. J.: Acta Cryst. A46 (1990) 140-9.
-  Read, R. J.: Acta Cryst. A46 (1990) 900-12.
-  Vellieux, F.M.D., Livnah, O., Dym, O., Read, R.J. & Sussman, J.L.,
   manuscript in preparation.

solomon
-------

-  Abrahams J. P. and Leslie A. G. W., *Acta Cryst.* **D52**, 30-42
   (1996)

superpose
---------

-  E.Krissinel and K.Henrick (2004), Acta Cryst. D60, 2256-2268
   Secondary-structure matching (SSM), a new tool for fast protein
   structure alignment in three dimensions

surface
-------

-  B.Lee and F.M.Richards, *J.Mol.Biol.*, **55**, 379 - 400 (1971)
-  Chothia (1975), "Structural Invariants in Protein Folding", *Nature*
   **254**: 304-308

tffc
----

-  *Molecular Replacement*, Proceedings of the Daresbury Study Weekend,
   (1985) DL/SCI/R23
-  *Molecular Replacement*, Proceedings of the Daresbury Study Weekend,
   (1992) DL/SCI/R33

tlsanl
------

-   B.Howlin, S.A.Butler, D.S.Moss, G.W.Harris and H.P.C.Driessen,
   "TLSANL: TLS parameter analysis program for segmented anisotropic
   refinement of macromolecular structures.", *J. Appl. Cryst.*, **26**,
   622-624 (1993)
-   V.Schomaker and K.N.Trueblood *Acta Cryst.*, **B24**, 63 (1968)
-   E.A.Merritt *Acta Cryst.*, **D55**, 1997 (1999)

topdraw
-------

-  `Bond, C.S. (2003) *Bioinformatics* **19**,
   311-2. <http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=12538265&dopt=Abstract>`__

topp
----

-  Lu G., *A WWW service system for automatic comparison of protein
   structures* `Protein Data Bank Quarterly Newsletter, #78, 10-11.
   1996 <ftp://ftp.rcsb.org/pub/pdb/doc/newsletters/bnl/news78_oct96/newslttr.txt>`__
-  Guoguang Lu, *An automatic topological and atomic comparison program
   for protein structures* (in manuscript or
   http://gamma.mbb.ki.se/~guoguang/top.html).

truncate
--------

-  French G.S. and Wilson K.S. Acta. Cryst. (1978), A34, 517.

vecref
------

-   Busing WR and Levy HA (1961) in Computing Methods and the Phase
   Problem in X-ray Crystal Analysis, Pergamon, Oxford.
-   Kraut J, Sieker LC, High DF, Freer ST (1962), Proc. Nat. Acad. Sci.
   USA, 48, 1417-1424.

xdldataman
----------

-  XDLDATAMAN:
   G.J. Kleywegt & T.A. Jones (1996), *Acta Cryst.* **D52**, 826-828.
-  XDL\_VIEW:
   J.W. Campbell (1995). "XDL\_VIEW, an X-windows-based toolkit for
   crystallographic and other applications", *J. Appl. Cryst.* **28**,
   236-242.
-  RAVE:
   G.J. Kleywegt & T.A. Jones (1994). "Halloween ... Masks and Bones",
   in "From First Map to Final Model" (S. Bailey, R. Hubbard & D.
   Waller, Eds.), SERC Daresbury Laboratory, pp. 59-66.
-  O:
   T.A. Jones, J.Y. Zou, S.W. Cowan, & M. Kjeldgaard (1991). "Improved
   methods for building protein models in electron density maps and the
   location of errors in these models", *Acta Cryst.* **A47**, 110-119.
-  GEMINI:
   E. Stanley (1972). "The identification of twins from intensity
   statistics", *J. Appl. Cryst.* **5**, 191-194.
-  GEMINI:
   D.C. Rees (1980). "The influence of twinning by merohedry on
   intensity statistics", *Acta Cryst.* **A36**, 578-581.
-  RFREE:
   A.T. Brunger (1992). "Free R value: a novel statistical quantity for
   assessing the accuracy of crystal structures", *Nature* **355**,
   472-475.
-  CCP4:
   Collaborative Computational Project Number 4 (1994). "The CCP4 suite:
   programs for protein crystallography", *Acta Cryst.* **D50**,
   760-763.

xdlmapman
---------

-  XDLMAPMAN:
   G.J. Kleywegt & T.A. Jones (1996), *Acta Cryst.* **D52**, 826-828.
-  XDL\_VIEW:
   J.W. Campbell (1995). "XDL\_VIEW, an X-windows-based toolkit for
   crystallographic and other applications", *J. Appl. Cryst.* **28**,
   236-242.
-  RAVE:
   G.J. Kleywegt & T.A. Jones (1994). "Halloween ... Masks and Bones",
   in "From First Map to Final Model" (S. Bailey, R. Hubbard & D.
   Waller, Eds.), SERC Daresbury Laboratory, pp. 59-66.
-  MINI-MAPS:
   T.A. Jones (1978). "A graphics model building and refinement system
   for macromolecules", *J. Appl. Cryst.* **11**, 268-272.
-  O/BONES:
   T.A. Jones, J.Y. Zou, S.W. Cowan, & M. Kjeldgaard (1991). "Improved
   methods for building protein models in electron density maps and the
   location of errors in these models", *Acta Cryst.* **A47**, 110-119.
-  SKELETONISATION:
   J. Greer (1974). "Three-dimensional pattern recognition: an approach
   to automated interpretation of electron density maps of proteins",
   *J. Mol. Biol.* **82**, 279-301.
-  PEAK-PICKING:
   G.J. Kleywegt, G.W. Vuister, A. Padilla, R.M.A. Knegtel, R. Boelens,
   & R. Kaptein (1993). "Computer-assisted assignment of homonuclear 3D
   NMR spectra of proteins. Application to pike parvalbumin III", *J.
   Magn. Reson.* **B102**, 166-176.
-  CCP4:
   Collaborative Computational Project Number 4 (1994). "The CCP4 suite:
   programs for protein crystallography", *Acta Cryst.* **D50**,
   760-763.
