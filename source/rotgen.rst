ROTGEN (CCP4: Supported Program)
================================

NAME
----

**rotgen** - simulate X-ray diffraction rotation images.

SYNOPSIS
--------

**rotgen**

DESCRIPTION
-----------

ROTGEN is an X-windows based program for carrying out simulations of
X-ray diffraction rotation images and analysing the unique data coverage
for one or more crystal settings and series of rotations. A strategy
option provides the user with information on the most efficient way to
collect data based on the current crystal setting. Previously processed
data stored in an MTZ reflection data file may be included in the
analyses if required.

See
`rg\_top\_source.html <../x-windows/Rotgen/doc/rg_top_source.html>`__

AUTHORS
-------

John W. Campbell, CCLRC Daresbury Laboratory.
