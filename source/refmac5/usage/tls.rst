::

    ! REFMAC

    TLS From REFMAC                                                                     
    RANGE  'A   1.' 'A 180.' ALL                           

    TLS chain B                                                                         
    RANGE  'B   1.' 'B 180.' ALL                           

    TLS chain C                                                                         
    RANGE  'C   1.' 'C 180.' ALL                           
    ORIGIN   84.148  24.835   1.599

    TLS CHAIN D                                                                         
    RANGE  'D   1.' 'D 180.' ALL                           

    TLS Chain E                                                                         
    RANGE  'E   1.' 'E 180.' ALL                           

    TLS chain F                                                                         
    RANGE  'F   1.' 'F 180.' ALL                           

