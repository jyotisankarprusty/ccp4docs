CLUSTALW INTERFACE
==================

Contents
--------

`Description <#description>`__

`Running ClustalW <#Running%20ClustalW>`__

`Specific Options <#Specific%20Options>`__

`DNA Pairwise Alignment Options <#DNA%20Pairwise%20Alignment>`__

`DNA Multiple Alignment Options <#DNA%20Multiple%20Alignment>`__

`Protein Pairwise Alignment Options <#Protein%20Pairwise%20Alignment>`__

`Protein Multiple Alignment Options <#Protein%20Multiple%20Alignment>`__

`Other Options <#Other%20Options>`__

`Structure Alignments Options <#Structure%20Alignments>`__

`Phylogenetic Trees Options <#Phylogenetic%20Trees>`__

`Observing Output <#Observing%20Output>`__

Description
-----------

This is an interface to allow users to run the ClustalW Multiple
Sequence Alignments program. Note: The ClustalW program is not included
in the CCP4 distribution. ClustalW must be installed on the system
running CCP4i in order to work.

Running ClustalW
----------------

The interface is able to run pairwise and multiple sequence alignments
for both DNA and protein sequences. The minimum information required to
run the program is a file containing DNA or protein sequences that need
to be aligned. This file should ideally be a plain text file in the
standard FASTA format. The default settings within the program should be
sufficient for most cases of DNA and Protein alignment, but there are
other more complicated options available, There are options to set the
output type of the results as well as options to set limits for
structure alignments, phylogenetic trees and options specific for DNA
and protein pairwise/multiple alignments. For full explanations of these
options, please refer to the ClustalW documentation.

Specific Options
----------------

When the mode of the alignment is selected - DNA Pairwise, DNA Multiple,
Protein Pairwise or Protein Multiple Alignments - a folder will appear
with options specific for that mode.

DNA Pairwise Alignment
~~~~~~~~~~~~~~~~~~~~~~

Includes options to set the DNA weight matrix, K-TUPLE value and gap
penalties, including opening and extension penalties.

DNA Multiple Alignment
~~~~~~~~~~~~~~~~~~~~~~

Includes options to set the DNA weight matrix, gap opening and extension
penalties, transitions weighting and the gap separation penalty range.
Can also specify no hydrophilic gaps and no residue-specific gaps.

Protein Pairwise Alignment
~~~~~~~~~~~~~~~~~~~~~~~~~~

As DNA Pairwise Alignment options, except uses Protein Weight Matrices.

Protein Multiple Alignment
~~~~~~~~~~~~~~~~~~~~~~~~~~

| As DNA Multiple Alignment options, except uses Protein Weight
  Matrices.

Other Options
-------------

As well as the options that are specific for the mode of alignment,
there are other general options that can be selected regardless of the
mode.

Structure Alignments Options

| Allows you to set gap penalties for helix core residues, strand core
  residues, loop regions and structure termini. Also allows you to set
  the number of residues inside and outside the helix or strand to be
  treated as termini.

Phylogenetic Trees Options

| Allows you to specify the production and format of phylogenetic trees.
  Includes the use of Kimura's Correction and ignoring positions with
  gaps.

Observing Output
----------------

| The output of the program is written to a specified output file, but
  for a quick glance it is also written to a viewing window in the
  "Output" folder at the bottom of the interface.
