MAKEDICT (CCP4: Supported Program)
==================================

NAME
----

**makedict** - converts PDB file to TNT or PROTIN dictionaries and
PROTIN to PDB.

SYNOPSIS
--------

| **makedict** **XYZIN** [ *foo.pdb \| foo.dict* ] [ **DICT** *foo.dict*
  \| **XYZOUT** *foo.pdb* ]
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

**Note:** PROTIN is now obsolete - use the `new
REFMAC <refmac5.html>`__.

Reads a set of coordinates for the group, in PDB or PROTIN format, and
writes out dictionary entry in format for either TNT or PROTIN. The
program can also convert a PROTIN coordinate entry to PDB format.
Multiple copies of the group may be present (with different residue
numbers), in which case bond lengths & angles will be averaged (only
useful for TNT output).

KEYWORDED INPUT
---------------

The available keywords are:

    `**CENTRE** <#centre>`__, `**CHIRAL** <#chiral>`__,
    `**INPUT** <#input>`__, `**NONBOND** <#nonbond>`__,
    `**OUTPUT** <#output>`__, `**PLANE** <#plane>`__,
    `**RADII** <#radii>`__, `**RESIDUE** <#residue>`__, `**SD** <#sd>`__

INPUT [PROTIN \| PDB]
~~~~~~~~~~~~~~~~~~~~~

Input format, Protin or PDB

OUTPUT [TNT \| PROTIN <numres> <rscode> \| PDB]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 TNT
    Write output dictionary for TNT
 PROTIN
    Write output dictionary for Protin

    <numres>
        Residue number for PROTIN, as it appears in Card 1a of the
        protin dictionary. The default value is 1, but this should
        normally be changed.
    <rscode>
        1-character residue name for PROTIN, as it appears in Card 1a of
        the protin dictionary. The 3-character residue name is taken
        from the RESIDUE keyword.

 PDB
    Output file in PDB format
    (*e.g.* for converting from Protin dictionary form)

RADII <element\_name> <radius>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set bonding radius for elements (C,N,O,S & P are present by default).

RESIDUE <residue\_name>
~~~~~~~~~~~~~~~~~~~~~~~

New residue type name, to override input.

PLANE <atom list>
~~~~~~~~~~~~~~~~~

Define list of atoms in plane.

CHIRAL <central atom> <atom2> <atom3> <atom4>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

List of atoms in chiral group.

Note the TNT convention for chiral groups: 1st atom is central atom Look
from central atom towards the lowest-priority atom (nearly always H),
then list the other atoms (which are towards you) in a clockwise order.

CENTRE <atom name>
~~~~~~~~~~~~~~~~~~

Atom to consider as centre [default 1st atom]

SD <sd\_bond> <sd\_angle> <sd\_plane> <sd\_bfactor>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For TNT output only. Read sd parameters for TNT dictionary.

NONBOND [<type>] <atom pair 1> <atom pair 2> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For Protin output only.

List of atomname pairs to be put in the non-bonded contact list. Type is
either 1 for single-torsion contacts or 2 [default] for multiple-torsion
contacts.

 INPUT AND OUTPUT
-----------------

 XYZIN
    Input PDB file; with INPUT PDB.
 XYZOUT
    Output PDB file; with INPUT PROTIN and OUTPUT PDB.
 DICT
    Output dictionary file; with OUTPUT PROTIN or OUTPUT TNT.

HINTS FOR MAKING PROTIN DICTIONARY ENTRIES
------------------------------------------

MAKEDICT should be used for making dictionary entries for
`PROTIN <protin.html>`__.

You need a set of coordinates for an "ideal" substrate in PDB format
(XYZIN) (the coordinates can be anywhere in space as long as they are
correctly bonded, i.e. their relative positions must be correct). You
also need knowledge of planar and chiral groups (if any), and must
supply this information via the `PLANE <#plane>`__ and
`CHIRAL <#chiral>`__ keywords respectively. Nonbonding contacts can be
defined using the `NONBOND <#nonbond>`__ keyword; covalent bonds should
be defined as `SPECIAL <protin.html#special>`__ distances within PROTIN.

Then: take the DICT file output of MAKEDICT and insert each section
(separated by comment lines; don't copy these) into the appropriate
places of your local copy of the PROTIN dictionary (e.g. mydict.dic).
See `appendix A <protin.html#protindic>`__ of the PROTIN documentation
for a description of the different parts of the dictionary.

Finally, assign the logically variable DICTPROTN to mydict.dat when you
run PROTIN to prepare restraints with your new substrate.

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Example of using MAKEDICT to generate PROTIN dictionary entry:

-  `makedict.exam <../examples/unix/runnable/makedict.exam>`__

General examples of input and output scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here is an example where an input PDB is converted into a PROTIN
dictionary.

Input PDB:

::


    REMARK   This file contains the 1dtx pdb coordinates with
    REMARK   residue 1 renamed
    CRYST1   73.53    39.06    23.15   90.00  90.00  90.00
    SCALE1      0.013600  0.000000  0.000000        0.00000
    SCALE2      0.000000  0.025601  0.000000        0.00000
    SCALE3      0.000000  0.000000  0.043197        0.00000
    ATOM    473  N   GLY    59       9.911  -4.709  16.657  1.00 24.03
    ATOM    474  CA  GLY    59       9.228  -5.696  17.463  1.00 27.54
    ATOM    475  C   GLY    59       9.299  -5.294  18.943  1.00 31.44
    ATOM    476  O   GLY    59       9.804  -6.144  19.719  1.00 33.91
    ATOM    477  OE  GLY    59       9.027  -4.131  19.297  1.00 34.32

Script:

::


    #!/bin/sh

    makedict XYZIN toxd.pdb DICT junk.dict <<eof
    INPUT PDB
    OUTPUT  PROTIN
    RESIDUE JNK
    CENTRE CA
    END
    eof

Output dictionary:

::


    ## Dictionary entry in Protin format: edit out sections separated by ##
    ##  Coordinates
                                                     1    1          JNK
       0.68300   0.98700  -0.80600              2         1   -1               N
       0.00000   0.00000   0.00000              1         2   -1               CA
       0.07100   0.40200   1.48000              1         3   -1               C
       0.57600  -0.44800   2.25600              3         4   -1               O
      -0.20100   1.56500   1.83400              3         5   -1               OE
    ## Distances (bonds & angles)
    JNK          1    8
      2  1 1 1  3  2 1 1  4  3 1 1  5  3 1 1  1  3 2 2  2  4 2 2  2  5 2 2  4  5 2 2

The reverse transformation can be done with the following script (you
must of course remove the lines beginning with an "#" from the protin
entry given above):

::


    #!/bin/sh

    makedict XYZIN junk.dict XYZOUT junk.pdb <<eof
    INPUT PROTIN
    OUTPUT PDB
    END
    eof

AUTHORS
-------

Originator: Phil Evans
