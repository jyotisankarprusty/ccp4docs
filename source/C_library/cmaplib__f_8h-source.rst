`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmaplib\_f.h
============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cmaplib_f.h: header files for cmaplib_f.c
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef __GUARD_MAPLIB_FORTRAN
    00011 #define __GUARD_MAPLIB_FORTRAN
    00012 
    00013 #include "cmaplib.h"
    00014 
    00015 #define MAXMAP MAXFILES
    00016 
    00017 typedef struct _IOConvMap IOConvMap;
    00018 
    00019 struct _IOConvMap {
    00020   int ipc;
    00021   char *logname;
    00022   CMMFile *mapfile;
    00023 };
    00024 
    00025 #endif  /*  __GUARD_MAPLIB_FORTRAN */

.. raw:: html

   </div>
