`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CINCH Compound Members
======================

`a <#index_a>`__ \| `b <#index_b>`__ \| `c <#index_c>`__ \| `d <#index_d>`__ \| `e <#index_e>`__ \| `f <#index_f>`__ \| `g <#index_g>`__ \| `h <#index_h>`__ \| `i <#index_i>`__ \| `j <#index_j>`__ \| `l <#index_l>`__ \| `m <#index_m>`__ \| `n <#index_n>`__ \| `o <#index_o>`__ \| `p <#index_p>`__ \| `r <#index_r>`__ \| `s <#index_s>`__ \| `t <#index_t>`__ \| `u <#index_u>`__ \| `w <#index_w>`__ \| `x <#index_x>`__

Here is a list of all documented class members with links to the classes
they belong to:

- a -
~~~~~

-  active : `MTZCOL <structMTZCOL.html#m2>`__
-  alambd : `bathead <structbathead.html#m36>`__

- b -
~~~~~

-  batch : `MTZ <structMTZ.html#m16>`__
-  bbfac : `bathead <structbathead.html#m27>`__
-  bscale : `bathead <structbathead.html#m26>`__

- c -
~~~~~

-  cell : `bathead <structbathead.html#m16>`__,
   `MTZXTAL <structMTZXTAL.html#m3>`__
-  col : `MTZSET <structMTZSET.html#m4>`__
-  crydat : `bathead <structbathead.html#m19>`__

- d -
~~~~~

-  datum : `bathead <structbathead.html#m20>`__
-  delamb : `bathead <structbathead.html#m37>`__
-  delcor : `bathead <structbathead.html#m38>`__
-  detlm : `bathead <structbathead.html#m43>`__
-  divhd : `bathead <structbathead.html#m39>`__
-  divvd : `bathead <structbathead.html#m40>`__
-  dname : `MTZSET <structMTZSET.html#m1>`__
-  dx : `bathead <structbathead.html#m41>`__

- e -
~~~~~

-  e1 : `bathead <structbathead.html#m31>`__
-  e2 : `bathead <structbathead.html#m32>`__
-  e3 : `bathead <structbathead.html#m33>`__

- f -
~~~~~

-  filein : `MTZ <structMTZ.html#m0>`__
-  fileout : `MTZ <structMTZ.html#m1>`__

- g -
~~~~~

-  gonlab : `bathead <structbathead.html#m2>`__

- h -
~~~~~

-  hist : `MTZ <structMTZ.html#m3>`__
-  histlines : `MTZ <structMTZ.html#m4>`__

- i -
~~~~~

-  iortyp : `bathead <structbathead.html#m3>`__

- j -
~~~~~

-  jsaxs : `bathead <structbathead.html#m10>`__
-  jumpax : `bathead <structbathead.html#m6>`__

- l -
~~~~~

-  label : `MTZCOL <structMTZCOL.html#m0>`__
-  lbcell : `bathead <structbathead.html#m4>`__
-  lbmflg : `bathead <structbathead.html#m13>`__
-  lcrflg : `bathead <structbathead.html#m8>`__
-  ldtype : `bathead <structbathead.html#m9>`__

- m -
~~~~~

-  max : `MTZCOL <structMTZCOL.html#m5>`__
-  min : `MTZCOL <structMTZCOL.html#m4>`__
-  misflg : `bathead <structbathead.html#m5>`__
-  mnf : `MTZ <structMTZ.html#m13>`__
-  mtzsymm : `MTZ <structMTZ.html#m14>`__

- n -
~~~~~

-  n\_orig\_bat : `MTZ <structMTZ.html#m10>`__
-  nbscal : `bathead <structbathead.html#m11>`__
-  nbsetid : `bathead <structbathead.html#m15>`__
-  ncol : `MTZSET <structMTZSET.html#m3>`__
-  ncol\_read : `MTZ <structMTZ.html#m6>`__
-  ncryst : `bathead <structbathead.html#m7>`__
-  ndet : `bathead <structbathead.html#m14>`__
-  next : `bathead <structbathead.html#m44>`__
-  ngonax : `bathead <structbathead.html#m12>`__
-  nref : `MTZ <structMTZ.html#m7>`__
-  nref\_filein : `MTZ <structMTZ.html#m8>`__
-  nset : `MTZXTAL <structMTZXTAL.html#m6>`__
-  nsym : `SYMGRP <structSYMGRP.html#m2>`__
-  nsymp : `SYMGRP <structSYMGRP.html#m4>`__
-  num : `bathead <structbathead.html#m0>`__
-  nxtal : `MTZ <structMTZ.html#m5>`__

- o -
~~~~~

-  order : `MTZ <structMTZ.html#m17>`__

- p -
~~~~~

-  pgname : `SYMGRP <structSYMGRP.html#m6>`__
-  phiend : `bathead <structbathead.html#m22>`__
-  phirange : `bathead <structbathead.html#m30>`__
-  phistt : `bathead <structbathead.html#m21>`__
-  phixyz : `bathead <structbathead.html#m18>`__
-  pname : `MTZXTAL <structMTZXTAL.html#m2>`__

- r -
~~~~~

-  ref : `MTZCOL <structMTZCOL.html#m6>`__
-  refs\_in\_memory : `MTZ <structMTZ.html#m9>`__
-  resmax : `MTZXTAL <structMTZXTAL.html#m5>`__
-  resmax\_out : `MTZ <structMTZ.html#m11>`__
-  resmin : `MTZXTAL <structMTZXTAL.html#m4>`__
-  resmin\_out : `MTZ <structMTZ.html#m12>`__

- s -
~~~~~

-  scanax : `bathead <structbathead.html#m23>`__
-  sdbfac : `bathead <structbathead.html#m29>`__
-  sdbscale : `bathead <structbathead.html#m28>`__
-  set : `MTZXTAL <structMTZXTAL.html#m7>`__
-  setid : `MTZSET <structMTZSET.html#m0>`__
-  so : `bathead <structbathead.html#m35>`__
-  source : `bathead <structbathead.html#m34>`__,
   `MTZCOL <structMTZCOL.html#m3>`__
-  spcgrp : `SYMGRP <structSYMGRP.html#m0>`__
-  spcgrpname : `SYMGRP <structSYMGRP.html#m1>`__
-  sym : `SYMGRP <structSYMGRP.html#m3>`__
-  symtyp : `SYMGRP <structSYMGRP.html#m5>`__

- t -
~~~~~

-  theta : `bathead <structbathead.html#m42>`__
-  time1 : `bathead <structbathead.html#m24>`__
-  time2 : `bathead <structbathead.html#m25>`__
-  title : `MTZ <structMTZ.html#m2>`__,
   `bathead <structbathead.html#m1>`__
-  type : `MTZCOL <structMTZCOL.html#m1>`__

- u -
~~~~~

-  umat : `bathead <structbathead.html#m17>`__

- w -
~~~~~

-  wavelength : `MTZSET <structMTZSET.html#m2>`__

- x -
~~~~~

-  xname : `MTZXTAL <structMTZXTAL.html#m1>`__
-  xtal : `MTZ <structMTZ.html#m15>`__
-  xtalid : `MTZXTAL <structMTZXTAL.html#m0>`__
