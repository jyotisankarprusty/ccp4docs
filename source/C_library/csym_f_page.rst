`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

Fortran API to CSYM
-------------------

File list
---------

-  `csymlib\_f.c <csymlib__f_8c.html>`__

Overview
--------

This library consists of a set of wrappers to the CSYM library giving
the same API as the original symlib.f For details of the API, see the
original `documentation <../symlib.html>`__. This document covers some
peculiarities of the C implementation.

Multiple Spacegroups
--------------------

The set of Fortran calls which mimic the original symlib.f assume you
are working within a single spacegroup. All calls access the same
spacegroup data structure, in analogy with the COMMON blocks of symlib.f
For cases where you wish to work with multiple spacegroups (e.g. in the
program `REINDEX <../reindex.html>`__, a different set of calls is
provided (the names of which generally start with "CCP4SPG\_F\_"). These
identify the spacegroup of interest via an index "sindx" (by analogy
with the "mindx" of mtzlib).

Symmetry information from MTZ files
-----------------------------------

`MTZ <structMTZ.html>`__ file headers contain 2 types of symmetry
records:

SYMINF
    Contains number of symmetry operators, number of primitive symmetry
    operators, lattice type, spacegroup number, spacegroup name and
    point group name.
SYMM
    A series of records holding the symmetry operators.

Note that the spacegroup name is likely to be ambiguous at best, with no
indication of the particular setting used. The primary source of
symmetry information is therefore taken to be the list of symmetry
operators. Note also that the order of operators is important if an ISYM
column is present.
