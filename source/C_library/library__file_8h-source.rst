`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library\_file.h
===============

`Go to the documentation of this file. <library__file_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      library_file.h: header file for library_file.c
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00016 #ifndef __CCP4_LIB_FILE
    00017 #define __CCP4_LIB_FILE
    00018 
    00019 #include "ccp4_sysdep.h"
    00020 #include "ccp4_types.h"
    00021 
    00022 #ifdef __cplusplus
    00023 namespace CCP4 {
    00024 extern "C" {
    00025 #endif
    00026 
    00028 typedef struct _CFileStruct CCP4File;
    00029 
    00030 struct _CFileStruct {
    00031   char *name;
    00032   FILE *stream;
    00033   int fd;
    00034   unsigned int read : 1;
    00035   unsigned int write : 1;
    00036   unsigned int append : 1;
    00037   unsigned int binary : 1;
    00038   unsigned int scratch : 1 , : 3;
    00039   unsigned int buffered : 1;
    00040   unsigned int sync : 1, : 6;
    00041   unsigned int direct : 1, : 7;
    00042   unsigned int open : 1;
    00043   unsigned int own : 1;
    00044   unsigned int last_op : 2;
    00045   unsigned int getbuff : 1, : 4;
    00046   int iostat;
    00047   unsigned int mode : 8;
    00048   unsigned int itemsize : 8;
    00049   unsigned int iconvert : 8;
    00050   unsigned int fconvert: 8;
    00051   off_t length;
    00052   off_t loc;
    00053   size_t stamp_loc;
    00054   int (*_read) (CCP4File *, uint8 *, size_t);
    00055   int (*_write) (CCP4File *, const uint8 *, size_t);
    00056   char buff[8];
    00057   void *priv;
    00058 };
    00059 
    00060 
    00061 CCP4File *ccp4_file_open (const char *, const int);
    00062 
    00063 CCP4File *ccp4_file_open_file (const FILE *, const int);
    00064 
    00065 CCP4File *ccp4_file_open_fd (const int, const int);
    00066 
    00067 int ccp4_file_rarch ( CCP4File*);
    00068 
    00069 int ccp4_file_warch ( CCP4File*);
    00070 
    00071 int ccp4_file_close ( CCP4File*);
    00072 
    00073 int ccp4_file_mode ( const CCP4File*);
    00074 
    00075 int ccp4_file_setmode ( CCP4File*, const int);
    00076 
    00077 int ccp4_file_setstamp( CCP4File *, const size_t);
    00078 
    00079 int ccp4_file_itemsize( const CCP4File*);
    00080 
    00081 int ccp4_file_setbyte( CCP4File *, const int);
    00082 
    00083 int ccp4_file_byteorder( CCP4File *);
    00084 
    00085 int ccp4_file_is_write(const CCP4File *);
    00086 
    00087 int ccp4_file_is_read(const CCP4File *);
    00088 
    00089 int ccp4_file_is_append(const CCP4File *);
    00090 
    00091 int ccp4_file_is_scratch(const CCP4File *);
    00092 
    00093 int ccp4_file_is_buffered(const CCP4File *);
    00094 
    00095 int ccp4_file_status(const CCP4File *);
    00096 
    00097 const char *ccp4_file_name( CCP4File *);
    00098 
    00099 int ccp4_file_read ( CCP4File*, uint8 *, size_t);
    00100 
    00101 int ccp4_file_readcomp ( CCP4File*, uint8 *, size_t);
    00102 
    00103 int ccp4_file_readshortcomp ( CCP4File*, uint8 *, size_t);
    00104 
    00105 int ccp4_file_readfloat ( CCP4File*, uint8 *, size_t);
    00106 
    00107 int ccp4_file_readint ( CCP4File*, uint8 *, size_t);
    00108 
    00109 int ccp4_file_readshort ( CCP4File*, uint8 *, size_t);
    00110 
    00111 int ccp4_file_readchar ( CCP4File*, uint8 *, size_t);
    00112 
    00113 int ccp4_file_write ( CCP4File*, const uint8 *, size_t);
    00114 
    00115 int ccp4_file_writecomp ( CCP4File*, const uint8 *, size_t);
    00116 
    00117 int ccp4_file_writeshortcomp ( CCP4File*, const uint8 *, size_t);
    00118 
    00119 int ccp4_file_writefloat ( CCP4File*, const uint8 *, size_t);
    00120 
    00121 int ccp4_file_writeint ( CCP4File*, const uint8 *, size_t);
    00122 
    00123 int ccp4_file_writeshort ( CCP4File*, const uint8 *, size_t);
    00124 
    00125 int ccp4_file_writechar ( CCP4File*, const uint8 *, size_t);
    00126 
    00127 int ccp4_file_seek ( CCP4File*, long, int);
    00128 
    00129 void ccp4_file_rewind ( CCP4File*);
    00130 
    00131 void ccp4_file_flush (CCP4File *);
    00132 
    00133 long ccp4_file_length ( CCP4File*);
    00134 
    00135 long ccp4_file_tell ( CCP4File*);
    00136 
    00137 int ccp4_file_feof(CCP4File *);
    00138 
    00139 void ccp4_file_clearerr(CCP4File *);
    00140 
    00141 void ccp4_file_fatal (CCP4File *, char *);
    00142 
    00143 char *ccp4_file_print(CCP4File *, char *, char *);
    00144 
    00145 int ccp4_file_raw_seek( CCP4File *, long, int);
    00146 int ccp4_file_raw_read ( CCP4File*, char *, size_t);
    00147 int ccp4_file_raw_write ( CCP4File*, const char *, size_t);
    00148 int ccp4_file_raw_setstamp( CCP4File *, const size_t);
    00149 #ifdef __cplusplus
    00150 }
    00151 }
    00152 #endif
    00153 
    00154 #endif  /* __CCP4_LIB_FILE */

.. raw:: html

   </div>
