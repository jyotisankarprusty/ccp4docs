`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cvecmat.c File Reference
========================

| ``#include "cvecmat.h"``

| 

Functions
---------

 void 

**ccp4\_dcross** (const double a[3], const double b[3], double c[3])

 void 

**ccp4\_3matmul** (double c[3][3], const double a[3][3], const double
b[3][3])

 void 

**ccp4\_4matmul** (float c[4][4], const float a[4][4], const float
b[4][4])

 double 

**invert3matrix** (const double a[3][3], double ai[3][3])

 float 

**invert4matrix** (const float a[4][4], float ai[4][4])

 float 

**ccp4\_pow\_ii** (const float base, const int power)

--------------

Detailed Description
--------------------

C library for vector and matrix manipulations. Martyn Winn
