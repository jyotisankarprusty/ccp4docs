`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library\_f.c File Reference
===========================

| ``#include "ccp4_utils.h"``
| ``#include "ccp4_errno.h"``
| ``#include "ccp4_fortran.h"``

| 

Functions
---------

char \* 

`ccp4\_FtoCString <library__f_8c.html#a1>`__ (fpstr str1, int str1\_len)

void 

`ccp4\_CtoFString <library__f_8c.html#a2>`__ (fpstr str1, int str1\_len,
const char \*cstring)

  

**FORTRAN\_SUBR** (USTENV, ustenv,(fpstr str, int \*result, int
str\_len),(fpstr str, int \*result),(fpstr str, int str\_len, int
\*result))

  

**FORTRAN\_SUBR** (USTIME, ustime,(int \*isec),(int \*isec),(int
\*isec))

  

**FORTRAN\_SUBR** (OUTBUF, outbuf,(),(),())

  

**FORTRAN\_SUBR** (CUNLINK, cunlink,(fpstr filename, int
filename\_len),(fpstr filename),(fpstr filename, int filename\_len))

  

**FORTRAN\_SUBR** (CCPAL1, ccpal1,(void(\*routne)(), int \*n, int
type[], int length[]),(void(\*routne)(), int \*n, int type[], int
length[]),(void(\*routne)(), int \*n, int type[], int length[]))

  

**FORTRAN\_SUBR** (QNAN, qnan,(union float\_uint\_uchar
\*realnum),(union float\_uint\_uchar \*realnum),(union
float\_uint\_uchar \*realnum))

  

**FORTRAN\_FUN** (int, QISNAN, qisnan,(union float\_uint\_uchar
\*realnum),(union float\_uint\_uchar \*realnum),(union
float\_uint\_uchar \*realnum))

  

**FORTRAN\_SUBR** (CCPBML, ccpbml,(int \*ncols, union float\_uint\_uchar
cols[]),(int \*ncols, union float\_uint\_uchar cols[]),(int \*ncols,
union float\_uint\_uchar cols[]))

  

**FORTRAN\_SUBR** (CCPWRG, ccpwrg,(int \*ncols, union float\_uint\_uchar
cols[], float wminmax[]),(int \*ncols, union float\_uint\_uchar cols[],
float wminmax[]),(int \*ncols, union float\_uint\_uchar cols[], float
wminmax[]))

  

**FORTRAN\_SUBR** (HGETLIMITS, hgetlimits,(int \*IValueNotDet, float
\*ValueNotDet),(int \*IValueNotDet, float \*ValueNotDet),(int
\*IValueNotDet, float \*ValueNotDet))

  

**FORTRAN\_SUBR** (CMKDIR, cmkdir,(const fpstr path, const fpstr cmode,
int \*result, int path\_len, int cmode\_len),(const fpstr path, const
fpstr cmode, int \*result),(const fpstr path, int path\_len, const fpstr
cmode, int cmode\_len, int \*result))

--------------

Detailed Description
--------------------

FORTRAN API for library.c. Charles Ballard

--------------

Function Documentation
----------------------

void ccp4\_CtoFString

( 

fpstr 

  *str1*,

int 

  *str1\_len*,

const char \* 

  *cstring*

) 

+--------------------------------------+--------------------------------------+
|                                      | Creates a Fortran string from an     |
|                                      | input C string for passing back to   |
|                                      | Fortran call. Characters after       |
|                                      | null-terminator may be junk, so pad  |
|                                      | with spaces. If input cstring is     |
|                                      | NULL, return blank string.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *str1*         | pointer Fortr |
|                                      | an to string   |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *str1\_len*    | Fortran lengt |
|                                      | h of string    |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *cstring*      | input C strin |
|                                      | g              |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

char\* ccp4\_FtoCString

( 

fpstr 

  *str1*,

int 

  *str1\_len*

) 

+--------------------------------------+--------------------------------------+
|                                      | Creates a null-terminated C string   |
|                                      | from an input string obtained from a |
|                                      | Fortran call. Trailing blanks are    |
|                                      | removed. If input string is blank    |
|                                      | then return string "\\0". Memory     |
|                                      | assigned by malloc, so can be freed. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
|                                      |     | *str1*         | pointer to st |
|                                      | ring          |                      |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
|                                      |     | *str1\_len*    | Fortran lengt |
|                                      | h of string   |                      |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
+--------------------------------------+--------------------------------------+
