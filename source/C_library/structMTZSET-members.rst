`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

MTZSET Member List
==================

This is the complete list of members for `MTZSET <structMTZSET.html>`__,
including all inherited members.

+-----------------------------------------+----------------------------------+----+
| `col <structMTZSET.html#m4>`__          | `MTZSET <structMTZSET.html>`__   |    |
+-----------------------------------------+----------------------------------+----+
| `dname <structMTZSET.html#m1>`__        | `MTZSET <structMTZSET.html>`__   |    |
+-----------------------------------------+----------------------------------+----+
| `ncol <structMTZSET.html#m3>`__         | `MTZSET <structMTZSET.html>`__   |    |
+-----------------------------------------+----------------------------------+----+
| `setid <structMTZSET.html#m0>`__        | `MTZSET <structMTZSET.html>`__   |    |
+-----------------------------------------+----------------------------------+----+
| `wavelength <structMTZSET.html#m2>`__   | `MTZSET <structMTZSET.html>`__   |    |
+-----------------------------------------+----------------------------------+----+
