`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_unitcell.h
================

`Go to the documentation of this file. <ccp4__unitcell_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_unitcell.h: headers for C library for ccp4_unitcell.c
    00003      Copyright (C) 2001  CCLRC, Martyn Winn  
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00016 #ifndef __CCP4_UNITCELL
    00017 #define __CCP4_UNITCELL
    00018 
    00019 #ifdef  __cplusplus
    00020 namespace CCP4uc {
    00021 extern "C" {
    00022 #endif
    00023 
    00024 #include <math.h>
    00025 
    00034 double ccp4uc_frac_orth_mat(const double cell[6], const int ncode, 
    00035                            double ro[3][3], double rf[3][3]);
    00036 
    00042 double ccp4uc_calc_rcell(const double cell[6], double rcell[6]);
    00043 
    00052 void ccp4uc_orth_to_frac(const double rf[3][3], const double xo[3], double xf[3]);
    00053 
    00060 void ccp4uc_frac_to_orth(const double ro[3][3], const double xf[3], double xo[3]);
    00061 
    00068 void ccp4uc_orthu_to_fracu(const double rf[3][3], const double uo[6], double uf[6]);
    00069 
    00076 void ccp4uc_fracu_to_orthu(const double ro[3][3], const double uf[6], double uo[6]);
    00077 
    00082 double ccp4uc_calc_cell_volume(const double cell[6]);
    00083 
    00090 int ccp4uc_cells_differ(const double cell1[6], const double cell2[6], const double tolerance);
    00091 
    00097 int ccp4uc_is_rhombohedral(const float cell[6], const float tolerance);
    00098 
    00104 int ccp4uc_is_hexagonal(const float cell[6], const float tolerance);
    00105 
    00106 #ifdef __cplusplus
    00107 } }
    00108 #endif
    00109 
    00110 #endif  

.. raw:: html

   </div>
