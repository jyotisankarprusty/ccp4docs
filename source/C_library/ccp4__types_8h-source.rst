`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_types.h
=============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_types.h: CCP4 library.c macro definitions etc
    00003      Copyright (C) 2001  CCLRC
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef __CCP4_TYPES
    00011 #define __CCP4_TYPES
    00012 
    00013 #include "ccp4_sysdep.h"
    00014 
    00015 typedef unsigned short uint16;
    00016 #ifdef SIXTEENBIT
    00017 typedef unsigned long uint32;
    00018 #else
    00019 typedef unsigned int uint32;
    00020 #endif
    00021 typedef float float32;
    00022 typedef unsigned char uint8;
    00023 union float_uint_uchar {
    00024     float32 f;
    00025     uint32 i;
    00026     uint8 c[4];
    00027   };
    00028 
    00029 typedef   char     *        pstr;
    00030 
    00031 /* CCP4 library.c macro definitions */
    00032 
    00033 #ifndef FALSE
    00034 #define FALSE 0
    00035 #define TRUE 1
    00036 #endif
    00037 
    00038 typedef struct { double r;             /* real component and */
    00039                  double i;             /* imaginary component of */
    00040                } COMPLEX;              /* a complex number */
    00041 
    00042 typedef struct { double r;             /* radial and */
    00043                  double phi;           /* angular component of */
    00044                } POLAR;                /* a complex number */
    00045 
    00046 /* some simple macros, which may exist anyway */
    00047 #ifndef SQR
    00048 #define SQR(x) ((x)*(x))
    00049 #endif
    00050 #ifndef DEGREE
    00051 #define DEGREE(x) ((((x < 0)?(x)+2*M_PI:(x))*360)/(2*M_PI))
    00052 #endif
    00053 #ifndef RADIAN
    00054 #define RADIAN(x) ((((x<0)?(x)+360:(x))*2*M_PI)/360)
    00055 #endif
    00056 #ifndef MAX
    00057 #define MAX(x, y) (((x)>(y))?(x):(y))
    00058 #endif
    00059 #ifndef MIN
    00060 #define MIN(x, y) (((x)<(y))?(x):(y))
    00061 #endif
    00062 #ifndef ABS
    00063 #define ABS(x) (((x)<0)?-(x):(x))
    00064 #endif
    00065 #ifndef SIGN
    00066 #define SIGN(x) (((x)<0)?-1:1)
    00067 #endif
    00068 
    00069 #endif   /* __CCP4_TYPES */

.. raw:: html

   </div>
