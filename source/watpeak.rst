WATPEAK (CCP4: Supported Program)
=================================

NAME
----

**watpeak** - selects peaks from peakmax and puts them close to the
respective protein atoms

SYNOPSIS
--------

| **watpeak XYZIN** *foo\_in.pdb* **PEAKS** *peaks\_in.pdb* **XYZOUT**
  *foo\_out.pdb*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

Select atoms from one coordinate set (PEAKS, *e.g.* from
`PEAKMAX <peakmax.html>`__) which lie close to atoms in another
coordinate set (XYZIN, *e.g.* your molecule). This is used to choose
peaks from a difference map which are candidates as water molecules
belonging to the molecule you are building (as opposed to belonging to
neighbouring molecules in the cell).

KEYWORDED INPUT
---------------

Available keywords are:

    `**BFACTOR** <#bfactor>`__, `**CELL** <#cell>`__,
    `**CHAIN** <#chain>`__, `**DISTANCE** <#distance>`__,
    `**END** <#end>`__, `**HETATOMONLY** <#hetatomonly>`__,
    `**SYMMETRY** <#symmetry>`__,
    `**TITLE\|REMARK** <#title%7Cremark>`__.

TITLE \| REMARK
~~~~~~~~~~~~~~~

Anything on these lines is just echoed to the output PDB file. A TITLE
line is also echoed to the log file.

DISTANCE <dmin> <dmax>
~~~~~~~~~~~~~~~~~~~~~~

Maximum distance for a peak to be accepted (default = 5.0Å). Minimum
distance for a peak to be accepted (default = 0.1Å).

BFACTOR <Bfactor> [<occupancy>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Values to set Bfactor & occupancy to, default = 30.0, 1.0 Note the input
peak list may have the peak height in the occupancy column (see
`example <#examples>`__). If this command is omitted, the Bfactor &
occupancy will be left as input.

CHAIN <Chain\_id>
~~~~~~~~~~~~~~~~~

Set chain id for output (default blank).

HETATOMONLY
~~~~~~~~~~~

Toggle switch to only match Oxygen or Nitrogen from the XYZIN file to a
new peak in the PEAKS file. Default: accept close contacts to any atom.

CELL <a> <b> <c> [<alpha> <beta> <gamma>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Must be given if cell is not specified in either PDB file. If this is
the case NCODE will be assumed to be 1. If the CRYST1 card is supplied
then the cell and ncode from PEAKS must be the same as XYZIN. See
`RWBROOK <rwbrook.html>`__ for more information about the
orthogonalisation parameter ncode. Defaults alpha=90.0, beta=90.0,
gamma=90.0

SYMMETRY
~~~~~~~~

This card is COMPULSORY and can be given as the spacegroup number, name
or as symmetry cards *e.g.*

::

         SYMM 19
         SYMM x,y,z * -x,-y,-z
         SYMM P21

END
~~~

End input.

INPUT AND OUTPUT FILES
----------------------

Input:
~~~~~~

    XYZIN
        molecule to test against (Brookhaven format).
    PEAKS
        peak list. The peak height (for printing only) is assumed to be
        in the occupancy slot, as from `PEAKMAX <peakmax.html>`__ with
        option OUTPUT BROOKHAVEN.

Output:
~~~~~~~

    XYZOUT
        all atoms from PEAKS within DISTANCE of any atom from XYZIN
        (Brookhaven format).

NOTES
-----

The different rejection criteria are explained below.

#. The number of symmetry rejected waters; any water or its symmetry
   equivalent closer than dmin to a previously accepted water. Note that
   if dmin is set to zero then no symmetry equivalent waters will be
   rejected.
#. The number of waters with near contacts; any water with one or more
   contacts closer than dmin to the protein. Note that the protein
   includes any atoms in XYZIN.
#. The number of waters too far from the protein; any water with all its
   contacts greater than dmax.

Examples
--------

::

        peakmax  mapin  c40_fo-fcx xyzout c40_peaks.pdb << eof-1
        numpeaks 100
        threshold 0.2
        eof-1
        
        watpeak  xyzin a95_40.pdb peaks c40_peaks.pdb xyzout waters_40.pdb <<eof-2
        title Possible waters from C40 DelF
        distance 4.0
        symm 19
        eof-2
