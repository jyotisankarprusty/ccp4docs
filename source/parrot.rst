PARROT (CCP4: Supported Program)
================================

NAME
----

**parrot** - Statistical protein chain tracing

SYNOPSIS
--------

| **cparrot** **-mtzin-ref** *filename* **-pdbin-ref** *filename*
  **-mtzin-wrk** *filename* **-seqin-wrk** *filename* **-pdbin-wrk-ha**
  *filename* **-pdbin-wrk-mr** *filename* **-colin-ref-fo** *colpath*
  **-colin-ref-hl** *colpath* **-colin-wrk-fo** *colpath*
  **-colin-wrk-hl** *colpath* **-colin-wrk-phifom** *colpath*
  **-colin-wrk-fc** *colpath* **-colin-wrk-free** *colpath* **-mtzout**
  *filename* **-colout** *colpath* **-colout-hl** *colpath*
  **-colout-fc** *colpath* **-solvent-flatten** **-histogram-match**
  **-ncs-average** **-rice-probability** **-cycles** *number of cycles*
  **-resolution** *resolution* **-solvent-content** *fraction*
  **-solvent-mask-filter-radius** *radius* **-ncs-mask-filter-radius**
  *radius* **-ncs-asu-fraction** *fraction* **-ncs-operator**
  *alpha,beta,gamma,x,y,z,x,y,z* **-verbose** *verbosity* **-stdin**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

'parrot' performs automated density modification, designed as a modern
replacement for 'dm'. It uses an evolved traditional algorithm, which
means that it runs very quickly, but gives results comparable to
statistical density modification programs.

Electron density histograms are generated by a simulation calculation
using a known 'reference' structure for which calculated phases are
available. The success of the method is dependent on the features of the
reference structure matching those of the unsolved, 'work' structure.
For almost all cases, a single reference structure can be used, with
modifications automatically applied to the reference structure to match
its features to the work structure.

HOW TO RUN BUCCANEER
--------------------

A set of reference structure will have been provided with the program.
The structure 1TQW is good for typical protein problems at resolutions
up to 1.25A, although in practice including data much beyond 2.0A
doesn't make much difference. For exotic cases you might want to provide
your own reference structures.

INPUT/OUTPUT FILES
------------------

-pdbin-ref
    Input PDB file containing the final model for the reference
    structure.
-mtzin-ref
    Input 'reference' MTZ file. This contains the data for a known,
    reference structure. The required columns are F, sigF, and a set of
    Hendrickson-Lattman (HL) coefficients describing the calculated
    phases from the final model. Suitable reference structures can be
    constructed from the PDB using the 'Make Pirate reference' task.
-mtzin-wrk
    Input 'work' MTZ file. This contains the data for the unknown, work
    structure. The required columns are F, sigF, and a set of HL
    coefficients from phasing improvement.
-seqin-wrk
    [Optional] Input sequence file in any common format, e.g. pir,
    fasta.
-pdbin-wrk-ha
    [Optional] Input PDB file containing a heavy atom model, used to
    determine NCS.
-pdbin-wrk-mr
    [Optional] Input PDB file containing a molecular replacement or
    partially built model, used to determine NCS.
-mtzout
    Output MTZ file. This will contain the new HL coefficients and map
    coefficients.

KEYWORDED INPUT
---------------

See `Note on keyword input <#notekeywords>`__.

    .. rubric:: -colin-ref-fo *colpath*
       :name: colin-ref-fo-colpath

    Observed F and sigma for reference structure. See `Note on column
    paths <#notecolpaths>`__.

    .. rubric:: -colin-ref-hl *colpath*
       :name: colin-ref-hl-colpath

    Hendrickson-Lattman coefficients for reference structure. If you do
    not have these, they can be generated using the accompanying
    chltofom program. See `Note on column paths <#notecolpaths>`__.

    .. rubric:: -colin-wrk-fo *colpath*
       :name: colin-wrk-fo-colpath

    Observed F and sigma for work structure. See `Note on column
    paths <#notecolpaths>`__.

    .. rubric:: -colin-wrk-hl *colpath*
       :name: colin-wrk-hl-colpath

    Hendrickson-Lattman coefficients for work structure. See `Note on
    column paths <#notecolpaths>`__.

    .. rubric:: -resolution *resolution/A*
       :name: resolution-resolutiona

    [Optional] Resolution limit for the calculation. All data is
    truncated.

    .. rubric:: -cycles *number of cycles*
       :name: cycles-number-of-cycles

    [Optional] Number of cycles of density modification to run. 3 or 5
    cycles are a good choice if there is no NCS, with NCS more cycles
    can be used. Too many cycles lead to bias.

    .. rubric:: -verbose *verbosity*
       :name: verbose-verbosity

Note on column paths:
^^^^^^^^^^^^^^^^^^^^^

When using the command line, MTZ columns are described as groups using a
slash separated format including the crystal and dataset name. If your
data was generated by another column-group using program, you can just
specify the name of the group, for example '/native/peak/Fobs'. You can
wildcard the crystal and dataset if the file does not contain any
duplicate labels, e.g. '/\*/\*/Fobs'. You can also access individual
non-grouped columns from existing files by giving a comma-separated list
of names in square brackets, e.g. '/\*/\*/[FP,SIGFP]'.

Note on keyword input:
^^^^^^^^^^^^^^^^^^^^^^

Keywords may appear on the command line, or by specifying the '-stdin'
flag, on standard input. In the latter case, one keyword is given per
line and the '-' is optional, and the rest of the line is the argument
of that keyword if required, so quoting is not used in this case.

Reading the Ouput:
------------------

Problems:
---------

AUTHOR
------

Kevin Cowtan, York.

SEE ALSO
--------


