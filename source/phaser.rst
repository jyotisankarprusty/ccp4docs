PHASER (CCP4: Supported Program)
================================

NAME
----

**phaser-2.8.3** - Maximum Likelihood Analysis and Phasing.

SYNOPSIS
--------

**phaser**

DESCRIPTION
-----------

| Molecular Replacement, SAD Phasing, Anisotropy Correction, Cell
  Content Analysis, Translational NCS/Twin Analysis, Gyration
  Refinement, Pruning, Single Atom MR, and SCEDS Substructure
  Determination/Normal Mode Perturbation of PDB files (Model Generation
  for Molecular Replacement).
| There are some changes between this version and previous versions so
  input scripts may need editing.
| **See also**: `Phaserwiki <http://www.phaser.cimr.cam.ac.uk/>`__

AUTHORS
-------

| A. J. McCoy, L. C. Storoni, G. Bunkoczi, R. D. Oeffner, & R. J. Read
| University of Cambridge
| R. W. Grosse-Kunstleve, P. D. Adams
| Lawrence Berkeley National Laboratory, Berkeley
| M. D. Winn
| CCP4

REFERENCES
----------

Citation:
~~~~~~~~~

Other references:
~~~~~~~~~~~~~~~~~

#. Read, R.J. (2001). `Pushing the boundaries of molecular replacement
   with maximum
   likelihood <http://scripts.iucr.org/cgi-bin/paper?ba5014>`__. *Acta
   Cryst*. D\ **57**, 1373-1382
#. Storoni, L.C., McCoy, A.J. & Read, R.J. (2004). `Likelihood-enhanced
   fast rotation
   functions <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/ad5007.pdf>`__\ *.
   Acta Cryst* D\ **60**, 432-438
#. McCoy, A.J., Grosse-Kunstleve, R.W., Storoni, L.C. & Read, R.J.
   (2005). `Likelihood-enhanced fast translation
   functions <http://scripts.iucr.org/cgi-bin/paper?gx5042>`__\ *. Acta
   Cryst* D\ **61**, 458-464
#. McCoy, A.J., Storoni, L.C. & Read, R.J. (2004) `Simple algorithm for
   a maximum-likelihood SAD
   function <http://scripts.iucr.org/cgi-bin/paper?ea5015>`__ *Acta
   Cryst.* D\ **60**, 1220-1228.
#. McCoy, A.J., Adams, P.D. & Read, R.J. (2013) `Intensity statistics in
   the presence of translational non-crystallographic
   symmetry <http://scripts.iucr.org/cgi-bin/paper?dz5268>`__ *Acta
   Cryst.* D\ **69**, 176-183
