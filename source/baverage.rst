BAVERAGE (CCP4: Supported Program)
==================================

NAME
----

**baverage** - averages B over main and side chain atoms

SYNOPSIS
--------

| **baverage XYZIN** *foo\_in.pdb* **RMSTAB** *foo\_out1.tab* **XYZOUT**
  *foo\_out2.pdb*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

A very simple minded program to read a PDB file, tabulate to RMSTAB the
average B values residue by residue (main chain and side chain
separately) and the RMS deviation of the B values from this mean. It
also outputs a PDB file with outlying B factors reset to lie within the
given range.

INPUT AND OUTPUT FILES
----------------------

XYZIN
    Input coordinates.
RMSTAB
    Table of average B factors and rms deviations for main chain
    (columns 2 and 3), side chain (columns 4 and 5) and overall (columns
    6 and 7). The residue number is given in column 1.
XYZOUT
    Output coordinates with B values reset according to keyword
    `BLIMIT <#blimit>`__.

KEYWORDED INPUT
---------------

The available keywords are: `TITLE <#title>`__, `BLIMIT <#blimit>`__,
`BRANGE <#brange>`__, `END <#end>`__.

Title <title>
~~~~~~~~~~~~~

Title to appear in output.

BLIMIT <bminmc> <bmaxmc> <bminsc> <bmaxsc>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reset B factors for XYZOUT to lie in given ranges.

    <bminmc>
        minimum allowed value for the B value of a main chain atom
        (default 0.0).
    <bmaxmc>
        maximum allowed value for the B value of a main chain atom
        (default 100.0).
    <bminsc>
        minimum allowed value for the B value of a side chain atom
        (default 0.0).
    <bmaxsc>
        maximum allowed value for the B value of a side chain atom
        (default 100.0).

BRANGE <brange>
~~~~~~~~~~~~~~~

The output is also tarted up as a histogram showing the number of B
values in ranges of width <brange> (default 5.0).

END
~~~

End input.

PROGRAM FUNCTION
----------------

I think this is a very useful way of monitoring possible errors in the
structure. Although the absolute values are a function of the refinement
protocol and the resolution of the data, spikes in the RMS values are
often symptomatic of an error in the map interpretation. Usually such
errors do not mean that the worker has fitted structure to empty
regions; more often they have branched into density belonging to another
residue or to well defined solvent. This can show up as large deviations
in B values of adjacent atoms ; e.g. CB 22 CG 35 CD1 23 CD2 28 . where
the CG is out of density, and the CDs actually occupy the space of a
missing residue.

EXAMPLE
-------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `baverage.exam <../examples/unix/runnable/baverage.exam>`__

AUTHOR
------

Eleanor Dodson: York 1991

SEE ALSO
--------

Alternative B factor analysis:

-  `act <act.html>`__

Graphical display of B factors:

-  `bplot <bplot.html>`__
