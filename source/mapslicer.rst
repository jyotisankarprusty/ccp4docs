MAPSLICER (CCP4: Supported Program)
===================================

NAME
----

**mapslicer** - interactive viewer for contoured sections through CCP4
map files.

SYNOPSIS
--------

**mapslicer** [ *mapfilename* ]

DESCRIPTION
-----------

MapSlicer 2.1

MapSlicer reads in CCP4 format map files and can display contoured
sections though the map along any of the principal axes. MapSlicer uses
code from the CCP4 graphical user interface `ccp4i <ccp4i.html>`__ in
addition to a set of custom Tcl commands (see
`ccp4mapwish <ccp4mapwish.html>`__).

MapSlicer offers the following functions:

-  Read in CCP4 map file, pick any section or stack of sections
   (\`\`slab'') on any axis and display contour levels
-  Shortcuts to Harker sections
-  Can alter contour levels, section limits and grid
-  Can alter screen scaling
-  Can zoom into areas of the display
-  Can display info on map header and currently displayed section
-  Displays cursor position in fractional coordinates, and the density
   at that point
-  Print section to a postscript file with user-defined scaling (eg mm
   per angstrom)

The mapslicer script runs through `ccp4mapwish <ccp4mapwish.html>`__,
which is a customised version of the TCL/Tk interpreter ``wish``.

USAGE
-----

Most of MapSlicer's functionality can be accessed quickly through the
controls on the left-hand side of the window. Other options are
available through menus at the top of the main window.

File menu:
~~~~~~~~~~

Open new map file
    Select a new map to view. Any existing map currently being viewed
    will be overwritten in the viewer's memory.
Save section
    Save the current section to a postscript file.
Print
    Print the current section to a postscript file.
Exit
    Close the viewer.

Section menu:
~~~~~~~~~~~~~

Section limits
    Allows you to change properties of the grid, the extent of the
    section displayed, and the orientation of the view.
Contour levels
    Allows you to change the contour levels displayed. This is based on
    the old NPO keywords.

Info menu:
~~~~~~~~~~

About map ...
    Displays a window with information about the current map.
About section ...
    Displays a window with information about the current section,
    including the contour levels.
About MapSlicer ...
    Starts a hypertext viewer to display this file.

MapSlicer Modes
~~~~~~~~~~~~~~~

MapSlicer operates in four distinct modes, which are selected via the
**Mode** menu on the left-hand side of the screen. Depending on the
mode, different controls will be become visible:

Section mode
    MapSlicer displays a single section at a time. The current section
    can be changed by editing the section number in the entry box, or by
    clicking on the arrows to move up or down by one section.
    The current axis can be changed by selecting the new axis from the
    menu.
    In this mode the option to display a "greyscale" view is also
    available. If this option is selected then the greyscale is
    superimposed beneath the contours - see the section on `greyscale
    views <#greyscale>`__ for more information.
Slab mode
    Similar to Section mode, except that the display shows a stack of
    sections superimposed one on top of the other. The slab is defined
    by a starting section, a depth (number of sections between the start
    and end) and a step size (specifying how many sections in the slab
    will actually be drawn).
    Any of these attributes can be edited directly to change the current
    slab display. Alternatively the arrows can be used to move the start
    position up or down by one section.
    The current axis can be changed by selecting the new axis from the
    menu.
Harker mode
    This requires that the Harker spacegroup be specified. A menu will
    then list the possible Harker sections for that spacegroup, and
    selecting a particular menu entry will jump directly to the
    appropriate axis and section.
    If the selected Harker section is not in the current map then a
    warning will be displayed. If no Harker sections are available then
    the menu will display "None".
Mask mode
    This is the default mode if a CCP4 mask file is loaded. The display
    shows a black ("zeroes") and white ("ones") view.
    (This is essentially a contourless greyscale view of the map.)

Other controls
~~~~~~~~~~~~~~

The remaining controls are not dependent on the mode

Scale
    Specifies how large each Ångstrom is shown on the display, in either
    mm or screen pixels. The scale is a real number for either units,
    and can be edited directly in the display.
AutoScale
    Resets the view to show the whole section at a scale of 2mm per
    Ångstrom.
Undo Zoom
    Restores the view to that prior to the last zoom operation that was
    performed.

Zoom Facility
~~~~~~~~~~~~~

The Zoom facility allows the user to select a smaller area of the
current display and rescales to show that area only.

The Zoom is enabled by click-and-holding the left mouse button (to set
one corner of the zoom area), dragging the mouse pulls out a box which
defines the zoom area, releasing the mouse button then performs the zoom
operation.

The scale will be reset automatically so that the screen area occupied
by the new view is roughly the same as before the zoom. This is
calculated based on area, and seems to work best for roughly square zoom
areas.

A zoom operation can be reversed using the **Undo Zoom** button.

Greyscale Views
~~~~~~~~~~~~~~~

The greyscale view sets tones of grey for each cell in the visible map
section, based on the value of the map at the grid point at the centre
of the cell, scaled linearly relative to the minimum and maximum values
within the current view. Darker areas have lower density values than
lighter areas.

This means that features in the greyscale view may appear to be
"flattened out" for sections which are dominated by only a few
disproportionately high peaks (such as sections in a patterson map which
include the origin peak). This can be counteracted to some degree by
using the zoom function to "crop out" the high peaks from the section
being viewed.

Comments and feedback on the greyscale view are welcome.

AUTHOR
------

Peter Briggs
