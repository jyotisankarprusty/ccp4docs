HELLIXANG (CCP4: Unsupported Program)
=====================================

NAME
----

**helixang** Program to calculate contacts, angles and separations
between helices.

SYNOPSIS
--------

| **helixang XYZIN** *foo\_in.pdb*
| [Input cards]

DESCRIPTION
-----------

Program to calculate contacts, angles and separations between helices.

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

 XYZIN
    Input coordinates, Brookhaven PDB format.

KEYWORDED INPUT
---------------

All commands are introduced by keywords, with free format numbers. Only
the first 4 characters of the keyword are used, and all are optional.

CYCLes N
~~~~~~~~

Number of cycles (default = 5).

LIST
~~~~

Turn on printed output, default is no list. See also NOLIST.

NOLIst
~~~~~~

Exact opposite of LIST.

CAONly
~~~~~~

Use CA only - default is to use N, CA, C, O.

HELIx N1 N2
~~~~~~~~~~~

Helix from start residue (N1) to end (N2).

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`helixang.exam <../examples/unix/runnable/helixang.exam>`__
