SIMBAD (CCP4: Supported Program)
================================

NAME
----

**SIMBAD** - automated sequence independent molecular
replacement

SYNOPSIS
--------

SIMBAD (Sequence-Independent Molecular replacement Based on Available Databases) is used for both contaminant testing and brute-force approaches for molecular replacement. It is designed to check for the presence of a contaminant in a crystal data set. The pipeline is also useful in cases of the misidentification of a crystal as well scenarios where no obvious homologue is available as a search model or the most suitable search model is not among those most highly ranked by sequence comparisons.

SIMBAD contains three major steps:

* a **lattice-parameter** search against the entire Protein Data Bank, rapidly determining whether or not a homologue exists in the same crystal form.

* a **contaminant** search, a not uncommon occurrence in macromolecular crystallography. To catch for this eventuality, SIMBAD rapidly screens the data against a database of known contaminant structures. This database is compiled to include entries from ContaBase and Dimple.

* the **non redundant PDB MoRDa database** search (referred to as a structural database search). SIMBAD performs the rotation search with ∼90000 domain models using Phaser likelihood-enhanced fast rotation function `[Simpkin et al. 2020] <https://journals.iucr.org/d/issues/2020/01/00/rr5186/>`__. The models are used as they are defined in the MoRDa database with no additional modifications. Each is then ranked by Z-score and the top 200 solutions are passed on to MOLREP followed by REFMAC5 to perform full MR and refinement.

References:

Simpkin, A. J., Simkovic, F., Thomas, J. M. H., Savko, M., Lebedev, A., Uski, V., Ballard, C., Wojdyr, M., Wu, R., Sanishvili, R., Xu, Y., Lisa, M.-N., Buschiazzo, A., Shepard, W., Rigden, D. J. & Keegan, R. M. (2018). `Acta Cryst. D74, 595–605 <https://journals.iucr.org/d/issues/2018/07/00/rr5159/>`__.

Simpkin, A. J., Simkovic, F., Thomas, J. M. H., Savko, M., Lebedev, A., Uski, V., Ballard, C. C., Wojdyr, M., Shepard, W., Rigden, D. J., & Keegan, R. M. (2020). Using Phaser and ensembles to improve the performance of SIMBAD. `Acta Cryst. D76(Pt 1) 1-8 <https://journals.iucr.org/d/issues/2020/01/00/rr5186/>`__.

Full documentation for SIMBAD is available `here <https://simbad.readthedocs.io/en/latest/>`__.
