ABS (CCP4: Supported Program)
=============================

NAME
----

**abs** - A program to determine the absolute configuration (hand) of
the heavy atom substructure and calculate a FOM.

SYNOPSIS
--------

| **abs hklin** *foo\_in.mtz*
| [`Key-worded input <#keywords>`__]

DESCRIPTION
-----------

ABS is a program to determine the absolute configuration (hand) of the
heavy atom substructure or polar space group using anomalous scattering.
It also calculates a figure of merit which is a useful assessment of the
heavy atom substructure. The algorithm is based on the Ps function
method (Woolfson & Yao, 1994 and Hao & Woolfson, 1989).

The program can be used as soon as anomalous sites are determined,
although the result may be more clear-cut after refinement of the sites.

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

 HKLIN
    (compulsory)
    Input MTZ file. This should contain the conventional (CCP4)
    asymmetric unit of data (preferably from TRUNCATE). See the LABIN
    keyword for columns used.

Output
~~~~~~

Results are shown in the log file.

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 characters of a keyword are significant. The cards can be in any
order. Numbers in [ ] are optional and can be left out. The available
keywords are:

    `**TITLE** <#title>`__, `**LABIN** <#labin>`__,
    `**RESOLUTION** <#reso>`__, `**ATOM** <#atom>`__

TITLE <title string>
~~~~~~~~~~~~~~~~~~~~

Upto 80 character title.

LABIN F=...  SIGF=...  DANO=...  SIGDANO=...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (Compulsory.)
| F: mean F magnitude.
| SIGF: standard deviation of F.
| DANO: Friedel difference. Must be column type D.
| SIGDANO: standard deviation of F2.

RESOLUTION <rmin>
~~~~~~~~~~~~~~~~~

High resolution cutoff in Angstrom (default value = 4).

ATOM <x> <y> <z> [<occupancy>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (compulsory)
| Fractional coordinates of anomalous scatterer(s).
|  

PRINTER OUTPUT
--------------

The value of C (the normalised value of C3, see
`reference <#reference1>`__ below) is printed out. If this is positive,
then the input atom configuration is likely correct. If it is negative,
then it is likely incorrect.

The absolute value of C gives an indication of how well the substructure
has been determined. C < 0.01 indicates a poor substructure solution,
while C > 0.02 indicates a good substructure solution.

The program also outputs a figure of merit
<\|sin(phase\_ha\_substructure)\|>

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `abs.exam <../examples/unix/runnable/abs.exam>`__

Non-runnable examples
~~~~~~~~~~~~~~~~~~~~~

::

    abs hklin /ccpdisk/xtal/ccp4/new_programs_4.2/hao_/ompdc.mtz <<eof
    TITLE determination of absolute configuration
    RESO 3.0
    ATOM   0.88364   0.45255   0.20904
    ATOM   0.78647   0.72103   0.20167
    ATOM   0.84051   0.61635   0.11817
    ATOM   0.72479   0.61860   0.11258
    ATOM   0.72077   0.55255   0.14145
    ATOM   0.59536   0.59265   0.24685
    ATOM   0.86602   0.58829   0.40655
    ATOM   0.77843   0.69468   0.11515
    ATOM   0.75302   0.81095   0.19786
    ATOM   0.69114   0.73137   0.22579
    ATOM   0.61948   0.76388   0.20633
    ATOM   0.54273   0.63876   0.12769
    ATOM   0.63899   0.79544  -0.08076
    ATOM   0.48613   0.55253   0.55323
    ATOM   0.61479   0.44768   0.54001
    ATOM   0.55872   0.43179   0.61568
    ATOM   0.62674   0.45720   0.64670
    ATOM   0.53478   0.56807   0.73165
    ATOM   0.66755   0.78155   0.57921
    LABIN F=F2 SIGF=SIGF2 DANO=DANO2 SIGDANO=SIGDANO2
    eof

REFERENCES
----------

#.  Hao, Q. **(2004) *J. Appl. Cryst.* 37, 498-499. `ABS: a program to
   determine absolute configuration and evaluate anomalous scatterer
   substructure <http://scripts.iucr.org/cgi-bin/paper?hi5556>`__.**

AUTHORS
-------

Q. Hao

MacCHESS, Wilson Synchrotron Lab, Cornell University, NY 14850, USA

Email: qh22@cornell.edu
