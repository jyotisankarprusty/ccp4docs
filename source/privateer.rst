privateer
=========

| 

Command line usage
~~~~~~~~~~~~~~~~~~

| ``privateer -pdbin file.pdb -mtzin file.mtz``

Parameter list
~~~~~~~~~~~~~~

``-pdbin [file.pdb]``
    Compulsory argument: input PDB file to examine. If no CRYST1 card is
    present, Privateer will still process it but please bear in mind
    that it may be missing important contacts described by
    crystallographic symmetry.
``-mtzin [file.mtz]`` or ``-cifin [file.cif]``
    An MTZ file containing amplitudes or intensities or a CIF file
    containing amplitudes. If intensities are supplied, Privateer will
    run the CCP4 program 'ctruncate' in order to produce amplitudes.
    Please ensure that CCP4 software is within reach (i.e. included in
    the PATH variable). Privateer is compatible out of the box with most
    of the CIF structure factor files downloaded from the PDB. It is
    also compatible with miniMTZ files from CCP4i2, and generates them
    when running in i2 mode.
``-colin-fo [F,SIGF]``
    Columns containing F and SIGF, e.g. FOBS,SIGFOBS or F,SIGF without
    any spaces in between characters. If not supplied, Privateer will
    try to guess the path using the most commonly used choices.
``-codein [SUG]``
    Three-letter code (those defined by the wwPDB) for the target sugar.
    If this parameter is not provided, Privateer will scan and score all
    monosaccharides present in it's database.
``-showgeom``
    Bond lengths, angles and torsions are reported for each ring.
``-mtzout [file.mtz]``
    Optional: output map coefficients to an MTZ file. A name for the
    file must be provided.
``-radiusin [value in angstroems]``
    Optional: provide a radius for the calculation of the mask around
    the target sugar. Default value is 1.5 Angstroems.
``-mode [normal|ccp4i2]``
    Optional: run mode. Default is normal. Under ccp4i2 mode, Privateer
    will generate miniMTZ files, less text output and XML data
    containing HTML descriptions of any glycosylation trees found, and
    all the validation data.

Source code
-----------

Privateer is Free Software. You can access and/or checkout it's source
code at
`CCP4forge <https://fg.oisin.rc-harwell.ac.uk/projects/privateer/>`__
(Bazaar repository, stable versions).

Authors
-------

| Jon Agirre and Kevin Cowtan
| York Structural Biology Laboratory
| Department of Chemistry, The University of York
| Wentworth Way, YO10 5DD, England.
| Last modified: January 27\ :sup:`th`, 2016.
| Bugs can be reported to jon.agirre (at) york.ac.uk 

Acknowledgements
----------------

| A number of people have shaped the future of Privateer with their
  suggestions. In no particular order: Eleanor Dodson, Jacob Sorensen,
  Keith Wilson, Gideon Davies, Christian 'Bones' Roth, Wendy Offen,
  Thomas Lutteke, Carme Rovira, Jose A. Cuesta Seijo, Stuart McNicholas,
  Antoni Planas, Marcelo Guerin, Dan Wright, Glyn Hemsworth, Andrew
  Thompson, Marcin Wojdyr, Saioa Urresti, Paul Emsley, Shabir Najmudin,
  Anabelle Varrot, Robbie Joosten, Javier Iglesias, Eugene Krissinel and
  Navraj Pannu.

Continue reading for a text-only transcription of the full manual
=================================================================

Privateer
=========

Automated conformational validation of sugar models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| **Synthesis:** Privateer is a fast software package for the automated
  conformational validation of carbohydrates. It works directly with a
  model in PDB format and structure factors (CIF or MTZ). Privateer is
  part of CCP4, and is being distributed since version 6.5.
| `[home] <#home>`__ `[theory] <#theory>`__ `[privateer] <#validate>`__
  `[source code] <#source>`__ `[authors] <#authors>`__
  `[acknowledgements] <#thanks>`__
| 

Theoretical background
----------------------

Conformational validation
~~~~~~~~~~~~~~~~~~~~~~~~~

Cyclic carbohydrates usually have clear conformational preferences in
terms of energy. Enzymes can force sugars into higher-energy
conformations in order to achieve catalysis, and these conformations
should indeed be kept in mind when modelling a sugar in the -1 site of
an enzyme. However, most of the time (*i.e.* in the rest of the cases)
sugars stay in their original lowest-energy conformation, which for
pyranoses is either a :sup:`4`\ C\ :sub:`1` or a :sup:`1`\ C\ :sub:`4`
chair. When a sugar monomer is imported from a dictionary within a
graphical model building program such as
`Coot <http://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot>`__, its
ideal coordinates reflect the lowest-energy conformation, but during the
course of refinement they can get distorted and driven away to other
conformations such as envelopes or half-chairs. These distortions are
usually triggered by:

**Weak density gradients**
    Sometimes there is just partial density for a sugar that is known to
    be there (*e.g.* in a High-Mannose glycosylation tree), and it is
    the crystallographer's decision whether to model it or not. However,
    atempting to refine a model with such data without the appropriate
    restraints - **torsion restraints** can help in this case - will
    nearly always result in distortion.
**Wrong linkage distance specification**
    Forcing refinement software to restrain a link to a wrong distance
    will make it fit the model with an offset equal to the deviation
    from the correct value. This can result just in an increased total
    puckering amplitude (Q) value if the deviation is not too high, or a
    complete conformational change for higher values.
**Mistaken anomer or handedness**
    Trying to fit an alpha anomer into density that supports the beta
    anomer instead will result on a flat ring or a conformational
    change.
``-pdbin [file.pdb]``
    Compulsory argument: input PDB file to examine. If no CRYST1 card is
    present, Privateer will still process it but please bear in mind
    that it may be missing important contacts described by
    crystallographic symmetry.
``-mtzin [file.mtz]`` or ``-cifin [file.cif]``
    An MTZ file containing amplitudes or intensities or a CIF file
    containing amplitudes. If intensities are supplied, Privateer will
    run the CCP4 program 'ctruncate' in order to produce amplitudes.
    Please ensure that CCP4 software is within reach (i.e. included in
    the PATH variable). Privateer is compatible out of the box with most
    of the CIF structure factor files downloaded from the PDB.
    Compatible with miniMTZ files from CCP4i2.
``-colin-fo [F,SIGF]``
    Columns containing F and SIGF, e.g. FOBS,SIGFOBS or F,SIGF without
    any spaces in between characters. If not supplied, Privateer will
    try to guess the path using the most commonly used choices.
``-codein [SUG]``
    Three-letter code (those defined by the wwPDB) for the target sugar.
    If this parameter is not provided, Privateer will scan and score all
    monosaccharides present in it's database.
``-showgeom``
    Bond lengths, angles and torsions are reported for each ring.
``-mtzout [file.mtz]``
    Optional: output map coefficients to an MTZ file. A name for the
    file must be provided.
``-radiusin [value in angstroems]``
    Optional: provide a radius for the calculation of the mask around
    the target sugar. Default value is 1.5 Angstroems.
``-mode [normal|ccp4i2]``
    Optional: run mode. Default is normal. Under ccp4i2 mode, Privateer
    will generate miniMTZ files, less text output and XML data
    containing HTML descriptions of any glycosylation trees found, and
    all the validation data.

| 
| 

Source code
-----------

Privateer is Free Software. You can access and/or checkout it's source
code at
`CCP4forge <https://fg.oisin.rc-harwell.ac.uk/projects/privateer/>`__
(Bazaar repository, stable versions) or `Google Code
repository <http://code.google.com/p/privateer/>`__ (Subversion
repository, experimental stuff that may not work).

Authors
-------

| Jon Agirre and Kevin Cowtan
| York Structural Biology Laboratory
| Department of Chemistry, The University of York
| Wentworth Way, YO10 5DD, England.
| Last modified: December 1\ :sup:`st`, 2014.
| Bugs can be reported to jon.agirre (at) york.ac.uk 

Acknowledgements
----------------

| A number of people have shaped the future of Privateer with their
  suggestions. In no particular order: Eleanor Dodson, Jacob Sorensen,
  Keith Wilson, Gideon Davies, Christian 'Bones' Roth, Wendy Offen,
  Thomas Lutteke, Carme Rovira, Jose A. Cuesta Seijo, Stuart McNicholas,
  Antoni Planas, Marcelo Guerin, Dan Wright, Glyn Hemsworth, Andrew
  Thompson, Marcin Wojdyr, Saioa Urresti, Paul Emsley, Shabir Najmudin,
  Anabelle Varrot, Robbie Joosten, Javier Iglesias, Eugene Krissinel and
  Navraj Pannu.
| `[Back to top] <#home>`__
