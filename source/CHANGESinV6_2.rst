CCP4 v6.2.992 Program Changes
=============================

INDEX

A list of general acknowledgements for testing CCP4 v6.2.992 can be
found `here <ACKNOWLEDGEMENTS_V6_2.html>`__.

--------------

New programs:

-  AIMLESS:
-  ZANUDA:
-  CMAPCUT:
-  (VIEWHKL):

--------------

Program changes:

-  POINTLESS: updated to 1.6.13
-  REFMAC: updated to 5.7

--------------

| Graphical User Interface

-  automation module
-  jligand interface
-  aimless interface
-  zanuda interface
-  prosmart integrated with refmac
-  viewhkl option for viewing mtz files

Changes to CCP4 suite for v6.2.0
================================

INDEX

A list of general acknowledgements for testing CCP4 v6.2.0 can be found
`here <ACKNOWLEDGEMENTS_V6_2.html>`__.

--------------

New programs:

-  SLOOP: loop building 0.5
-  MULTICOMB: Multivariate phase combination in density modification for
   SAD

--------------

Program changes:

-  BUCCANEER: updated to 1.5.1
-  POINTLESS: updated to 1.5.22
-  REFMAC: updated to 5.6.0117
-  MOLREP: updated to 11.0.0
-  BALBES: updated to 1.1.4
-  MRBUMP: updated to 0.5.1
-  PARROT: updated to 1.0.2
-  PHASER: updated to 2.3.0
-  MOSFLM: updated to 7.0.7
-  CRANK: updated to 1.4, multivariate phase combination and other
   improvements
-  AFRO: multivariate FA estimates for SAD data

--------------

Library changes:

-  MMDB: updated to 1.23
-  CCTBX: updated to January 2011

--------------

Graphical User Interface:

-  new Phaser NMA interface
-  sloop interface
-  updated phaser\_EP and phaser\_MR
-  imosflm updated to 1.0.5

--------------

| Bugs fixed:

-  various build problems with shared libraries

CCP4 v6.1.13 Changes
====================

INDEX

A list of general acknowledgements for testing CCP4 v6.1 can be found
`here <ACKNOWLEDGEMENTS_V6_1.html>`__.

--------------

Program changes:

-  BUCCANEER: updated to 1.4

--------------

Library changes:

-  CLIPPER: update to 2.1

--------------

Graphical User Interface:

-  BUCCANEER\_REFMAC: updated to include cycling

--------------

CCP4 v6.1.3 Program Changes
===========================

INDEX

A list of general acknowledgements for testing CCP4 v6.1 can be found
`here <ACKNOWLEDGEMENTS_V6_1.html>`__.

--------------

Building etc.:

-  gnu compiler 4.4 compliant
-  supports Snow Leopard

--------------

Program changes:

-  IPMOSFLM: updated to 7.0.5
-  REFMAC: updated to 5.5.0109
-  MOLREP: updated to 10.2.35
-  SFCHECK: updated to 7.3.16
-  POINTLESS: updated to 1.4.2
-  CTRUNCATE: updated to 1.0.106

--------------

Library changes:

-  MMDB: update to 1.21

--------------

Graphical User Interface:

-  IMOSFLM: updated to 1.03

--------------

| Bugs fixed:

--------------

| Known issues:

-  3488: IMOSFLM: cannot launch batch jobs from interface
-  3630: SCALEPACK2MTZ: does not read symmetry from input file
-  3894: DBhandles not python 3.0 compliant

--------------

CCP4 6.1.2 Program Changes
==========================

INDEX

--------------

Building etc.:

-  ccp4.setup-dist renamed ccp4.setup-csh
-  ccp4i, crank, imosflm launchers copied to bin directory during
   installation
-  csh no longer used during installation, all steps use sh

--------------

Program changes:

-  POINTLESS: updated to 1.3.9
-  SCALA: updated to 3.3.15
-  REFMAC5: updated to 5.5.0101. PHOUT added for building of MR
   solutions with BUCCANEER
-  MOLREP: updated to 10.2.31
-  SFCHECK: updated to 7.3.13
-  SIGMAA: new version from Ian Tickle
-  OASIS: minor update

--------------

Graphical User Interface:

-  CRUNCH2: new interface for experimental phasing program
-  CRANK: update to 1.3
-  database back end off by default

--------------

| Bugs fixed:

-  3596: IMOSFLM: requires project directory to exist, fails otherwise
-  3689: POINTLESS loop variable overflow
-  3690: REFMAC5 interface not backwardly compatible
-  3713: REFMAC5 interface amplitude based twin refinement
-  3722: CTRUNCATE in import scaled task
-  3790: MOLREP multi-copy search failure
-  3797: all\_load shared library builds under newer linux ld
-  3816: TLSANL anisotropic Us
-  various uninitialised variables caught

--------------

| Known issues:

-  3488: IMOSFLM: cannot launch batch jobs from interface
-  3630: SCALEPACK2MTZ: does not read symmetry from input file
-  3894: DBhandles not python 3.0 compliant

CCP4 v6.1.1 Program Changes
===========================

INDEX

--------------

Building etc.:

-  intel 11: mostly supported

--------------

Program changes:

-  PDB\_EXTRACT: updated to 3.004
-  REFMAC5: updated to 5.5.0071
-  POINTLESS: updated to 1.3.1

--------------

Library changes:

-  MMDB: update to 1.19
-  monomer dictionaries: various updates, including CYS-MPR

--------------

| Updated interfaces:

-  REFMAC55: better integration of SAD refinement
-  BP3: interface added

--------------

| Bugs fixed:

-  3628: ccp4i import task
-  3592: SFTOOLS interface
-  3619: REFMAC5 interface ignoring external file
-  3655: CHAINSAW not deleting CISPEP records from PDB
-  3656: PISA writing to {$CCP4/share/pisa} directory
-  3558: CAD segv

--------------

| Known issues:

-  3488: IMOSFLM: cannot launch batch jobs from interface
-  3648: REFMAC5 interface: failure to adjust NCS weights
-  3596: IMOSFLM: requires project directory to exist, fails otherwise
-  3630: SCALEPACK2MTZ: does not read symmetry from input file

--------------

CCP4 v6.1.0 Program Changes
===========================

INDEX

A list of general acknowledgements for testing CCP4 v6.1.0 can be found
`here <ACKNOWLEDGEMENTS_V6_1.html>`__.

--------------

Building etc.:

-  Refmac5.4+ requires a Fortran90 compiler, as do Molrep 10 and Sfcheck
   7.3+. Configure tests for this, and disables refmac, molrep and
   sfcheck builds if not found (e.g. if compiler is g77).
-  ccp4.setup: the environment variable GFORTRAN\_UNBUFFERED\_ALL is
   explicitly set to "Y" in the distributed setup files, to ensure that
   the log file output from gfortran-compiled programs is written in the
   correct sequence. This may incur a performance penalty however.
-  POINTLESS, RAPPER, PISA optionally configured and built with core.

--------------

Program changes:

-  AREAIMOL: uses a new {spiral distribution} to divide up the surfaces
   of the expanded Van der Waals atoms, which gives significant
   improvements to the accuracy and stability of the accessible surface
   area calculations - see the program documentation (Ian Tickle).
-  CAVENV: program now uses symmetry mates of input molecule unless SYMM
   OFF set. Gives protein and solvent volumes within map limits.
-  CRANK: updated to version 1.2.0. Greatly enhanced (Raj Pannu)
-  IMOSFLM: update imosflm to 1.0.0
-  LOGGRAPH: is now able to give a basic view of the XMGR-style normplot
   and correlplot output from SCALA, for systems where xmgr(ace) is
   unavailable.
-  MOLREP: updated to version 10.2.12.
-  MOSFLM: updated to version 7.0.4.
-  OASIS: updated to OASIS-2006.
-  PDBCUR: new option to delete ANISOU records.
-  PDB\_EXTRACT: updated to version 3.0 (since August 2007)
-  PDBSET: estimated molecular weight based on the sequence of residues
   is automatically calculated when the SEQUENCE keyword is specified.
-  PHASER: updated to 2.1.4. This version covers MR, SAD and combined MR
   and SAD.
-  REFMAC5: updated to version 5.5.0066. This adds twinned and anomalous
   refinement (Garib Murshudov, Raj Pannu, Pavol Skubak).
-  SCALA: updated to version 3.3.9
-  SFCHECK: updated to version 7.3.7.

--------------

Library changes:

-  Addition of a new {Diffraction Image} library which also comes with 4
   small jiffies installed in bin - diffdump, printpeaks, automask and
   diff2jpeg.
-  Monomer dictionary updated to 4.13

--------------

New programs:

-  AFRO: multivariate substructure factor amplitude estimation for
   SAD/MAD (Raj Pannu)
-  BAUBLES/SMARTIE: new program for re-rendering CCP4 log files with
   HTML markup (Peter Briggs & Kevin Cowtan).
-  BALBES: automated molecular replacement (Fei Long, Garib Murshudov,
   Alexei Vagin).
-  cBUCCANEER: statistical model building, optimised for experimental
   phasing. (Kevin Cowtan)
-  CRUNCH2: direct mothod experimental phasing as part of CRANK (Jan
   Abrahams)
-  CTRUNCATE: New program for Intensity to Structure Factor conversion
   and checking data quality (Norman Stein).
-  DBCCP4I: new database server for CCP4i, which includes a graphical
   viewer for CCP4i projects plus other tools. (Wanjuan Yang & Peter
   Briggs)
-  IMOSFLM: imported imosflm 1.0.0
-  MrBUMP: Automated molecular replacement, search model retrieval and
   search model preparation for molecular replacement.
-  MTZ2CIF: new program for generating mmCIF reflection files suitable
   for deposition, intended to replace OUTPUT CIF option of MTZ2VARIOUS.
   See program documentation for more information. (Peter Briggs)
-  PARROT: automatic density modification and phase improvement (Kevin
   Cowtan)
-  PISA: the standalone version of the Protein Interfaces, Surfaces and
   Assemblies server. (Eugene Krissinel) -- optional
-  POINTLESS: Laue and Patterson group determination, v1.2.23. (Phil
   Evans) -- optional
-  R500: new program for correcting REMARK 500 lines in PDB files.
-  RAPPER: conformer modelling through sampling residue rotameric
   states. (Nicholas Furnham, Paul de Bakker, Mark DePristo, Reshma
   Shetty, Swanand Gore and Tom Blundell) -- optional
-  cSEQUINS: Statistical protein chain tracing (Kevin Cowtan)
-  SEQWT: new utility to estimate protein molecular weight from the
   sequence. (Eleanor Dodson)
-  SYMCONV: new utility program for interrogating the CCP4 symmetry
   libraries. (Peter Briggs)
-  XIA2: Automated data reduction system designed to work from raw
   diffraction data and a little metadata, and produce usefully reduced
   data in a form suitable for immediately starting phasing and
   structure solution.

--------------

Graphical User Interface:

-  Version 2.0
-  Modules can now include subfolders of tasks to help with grouping
   related tasks together within a module. These can also be included in
   task packages that are exported - however developers should note that
   packages created with subfolders are NOT backwardly compatible with
   older versions of CCP4i.
-  A right-mouse click on the job list presents a {context menu}
   offering job manipulation options for the current selection (for
   example, rerun or delete jobs). Double-clicking with the left-mouse
   on a specific job automatically tries to "rerun" that job.
-  Options for customising the colours of jobs displayed in the job list
   can be accessed via the "System Administration" and the context menu
   described above.
-  Viewing files from job in some cases (e.g. Refmac5) now also provides
   options (when possible) to launch "plugin" applications (e.g. Coot,
   CCP4mg) preloaded with the appropriate data.
-  New option to re-import tasks that have already been installed in a
   previous version of CCP4 (e.g. Arp/Warp): accessed via the
   "Install/Uninstall Tasks" interface in the "System Administration"
   menu. (Note that this function requires the user to have permissions
   to write files to the CCP4i area.)
-  ccp4ish can be run with a {-import\_packages } option, which attempts
   to automatically copy any tasks previously installed in the CCP4
   installation in , and add them to the current CCP4i installation.
-  Disabled (\\"greyed out\\") tasks can be enabled by setting the
   appropriate option in the "Configure Interface" window
-  MOSFLM-in-batch: now recognises keywords SYNCHROTRON POLARISATION,
   DISPERSION and DIVERGENCE (used for data collected at a synchrotron)
   and POSTREFinement keywords WIDTH, MOSADD and MOSSMOOTH.
-  PARROT: new interface for the PARROT program.
-  PISA webservice: the EBI Protein Interfaces Surfaces and Assemblies
   webservice can be launched from the "Structure Analysis" and "Program
   List" modules
-  POINTLESS: new interface to the POINTLESS program (NB this supersedes
   the prerelease interfaces: "Determine Laue Group", "Test Alternative
   Indexing" and "Check Centre of Symmetry" - these interfaces can still
   be accessed e.g. for reviewing old jobs, but are now deprecated).
-  {SFs for Deposition} task (mtz2cif) now uses MTZ2CIF program rather
   than MTZ2VARIOUS.
-  GetMtzColumnResolution: new command that fetches the maximum and
   minimum resolution limits for a specific column in an MTZ file.
-  CURRENT\_PROJECT and MG\_CURRENT\_PROJECT parameters are now stored
   in status.def (previously in directories.def).
-  XIA2: new interface to the XIA2 automated data processing package.
-  Annotated HTML logfiles are generated after each program run using
   the "baubles" program.
-  Made a new task for job colourisation so it is more visible outside
   the big configure task.
-  Added Dyndom and Chooch interfaces files and added the tasks into
   Program list.
-  Added the MrBUMP (automated molecular replacement) interface.
-  Added new Map & Mask Utilities task to calculate an omit map using
   Sfcheck.
-  Clipper Utilities: this module has been removed and the Clipper-based
   tasks have been moved to other modules, mainly "Reflection Data
   Utilities" and "Map & Mask Utilities".
-  Added new interface for the existing getax program.
-  New task interfaces added to {Data Reduction} module (\\"Check Data
   Quality" folder): "Analysis with ctruncate" and "Analysis with
   sfcheck" allow experimental data to be characterised more easily.
-  New task interface added to {Molecular Replacement} module
   (\\"Analysis\\" folder): "Self RF with molrep" provides interface to
   the self-rotation function calculation of Molrep.
-  autoSHARP: new interface to SHARP/autoSHARP. Needs a separate install
   of SHARP/autoSHARP. (Clemens Vonrhein)

--------------

Documentation:

-  AREAIMOL: added figures to clarify the definitions of the different
   surface types.
-  MrBUMP: documentation for MrBUMP, automated molecular replacement
   program.

--------------

Withdrawal:

-  The following programs have been placed in deprecated/src for future
   removal: BEAST (replaced by Phaser), ROTAPREP (replaced by Combat),
   ARP\_WATERS (use latest ARP/wARP), BPLOT, POLYPOSE, RSEARCH,
   RESTRAIN.
-  The following X-windows programs have been taken out of the build
   procedure, prior to future removal: XLOGGRAPH (replaced by loggraph),
   XDLMAPMAN, XDLDATAMAN, IPDISP (replaced by idiffdisp)

--------------

Examples:

-  phaser\_EP.exam: new example for Phaser 2.1.4, using a sulfur-SAD
   dataset for bovine cubic insulin.
-  pointless.exam: new examples for pointless
-  rampage.exam: new example for rampage mode of rapper
-  refmac\_sad: new example for refmac SAD target function
-  acorn: new example running from 1 Zn atom (Deuterolysin)
