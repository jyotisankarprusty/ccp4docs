CONVERT2MTZ (CCP4: Supported Program)
=====================================

NAME
----

**convert2mtz** - CNS to MTZ conversion

SYNOPSIS
--------

| **convert2mtz** **-hklin** *filename* **-mtzout** *filename* **-cell**
  *a,b,c,alpha,beta,gamma* **-spacegroup** *spacegroup* **-pdbin**
  *filename* **-anomalous** **-no-complete** **-colin** *column names*
  **-seed** *seed*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

Convert CNS or other formatted reflection data files to MTZ. Columns are
identified automatically using names specified in the file or on the
command line. A full set of CCP4 FreeR\_flag information is generated,
using existing flags if available. Data is completed to the resolution
limit, unless you specify -no-complete. Cell and spacegroup information
may be specified or taken from a pdb file. Anomalous data is merged
automatically, if anomalous pairs are required add the -anomalous
option.

Cell parameters and spacegroup symbols must be given in quotes if they
contain spaces. Column names may be separated by spaces or commas,
however if spaces are used the column names must also be in quotes.

Column types are deduced from the column name, whether it is present in
the file header or specified on the command line. Column names
containing "SIG", "FOM", "HLA", "FREE" become sigma, weight, HL coeff,
and Free-R flag columns respectively. After that, columns beginning with
the letters "F", "E", "I", "P", and "W" become F's, E's, I's, phases and
weights respectively.

INPUT/OUTPUT FILES
------------------

-hklin
    (Compulsory) Input reflection data file.
-mtzout
    Output MTZ file.
-pdbin
    Input pdb file.

KEYWORDED INPUT
---------------

    .. rubric:: -anomalous
       :name: anomalous

    Data will be output for anomalous pairs

    .. rubric:: -cell *a,b,c,alpha,beta,gamma*
       :name: cell-abcalphabetagamma

    Cell parameters.

    .. rubric:: -colin *column names*
       :name: colin-column-names

    Column names.

    .. rubric:: -no-complete
       :name: no-complete

    Data will not be completed to the resolution limit.

    .. rubric:: -seed *seed*
       :name: seed-seed

    Seed for random number generator used for Free-R flags.

    .. rubric:: -spacegroup *spacegroup*
       :name: spacegroup-spacegroup

    Spacegroup name or number.

Examples:
---------

CNS files
~~~~~~~~~

::

    convert2mtz -hklin test.hkl -cell 50.9,50.9,121.4 -spacegroup 96
    convert2mtz -hklin test.hkl -cell "50.9  50.9  121.4 90  90  90" -spacegroup "P 43 21 2"
    convert2mtz -hklin test.hkl -cell "50.9  50.9  121.4 90  90  90" -spacegroup "P 43 21 2" -no-complete
    convert2mtz -hklin test.hkl -pdbin test.pdb

If the file has incomplete header records for column declaration, you
can also specify -colin.

PHS files
~~~~~~~~~

::

    convert2mtz -hklin test.phs -cell 50.9,50.9,121.4 -spacegroup 96 -colin "FO FC PHI"
    convert2mtz -hklin test.phs -cell 50.9,50.9,121.4 -spacegroup 96 -colin "FO FOM PHI"
    convert2mtz -hklin test.phs -cell 50.9,50.9,121.4 -spacegroup 96 -colin "FO FOM PHI SIG"
    convert2mtz -hklin test.phs -cell 50.9,50.9,121.4 -spacegroup 96 -colin FO,FOM,PHI,A,B,C,D

Change -colin according to the columns actually present in the file.

SHELX HKL files (limited support)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

convert2mtz may work for SHELX HKL files, however it has some
limitations. Firstly, if any number is wide enough to completely fill a
column in the file, the program will fail. Secondly, multiple
measurements of the same reflection (or its symmetry equivalents) are
not merged - all but the last value are discarded.

::

    convert2mtz -hklin test.hkl -cell 50.9,50.9,121.4 -spacegroup 96 -colin "I SIGI FREE"
    convert2mtz -hklin test.hkl -cell 50.9,50.9,121.4 -spacegroup 96 -colin I,SIGI,FREE
    convert2mtz -hklin test.hkl -cell 50.9,50.9,121.4 -spacegroup 96 -colin F,SIGF

| If the file contains Friedel opposites, you can also specify
  -anomalous.
| If you often convert shelx files to MTZ, you could set up an alias,
  e.g. in Unix C-shell

::

    alias shelx2mtz "convert2mtz -colin I,SIGI,FREE"

AUTHOR
------

Kevin Cowtan, York.
