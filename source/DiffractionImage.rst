DiffractionImage Library (CCP4: Library)
========================================

NAME
----

**diffImage** - CCP4 Diffraction Image Handling Library 

DESCRIPTION
-----------

**diffImage** contains a set of C++ classes to handle different
diffraction image format. The library is organised in a generic classe
"DiffractionImage" that internally handle all the formats by using
specific methods dedicated to each format. 

Contents of this file
---------------------

-  `General information <#general>`__
-  `Supported Image Formats <#supported>`__
-  `Using the DiffractionImage Object <#using>`__
-  `Using the DiffractionImage Tcl-Tk, Java and Python
   Modules <#usingPythonTcl>`__
-  `Classes Description <#classes>`__
-  `Examples of programs <#printheader>`__

General Information
-------------------

The Diffraction Image was originally part of XIA-DPA automation project
and is still used by it. However, it has been decided to include this
library with the other CCP4 core libraries because of a wider possible
interest among the comunity.

Even if the library contains several different files implementing
similar methods for different types of images it has been designed so
that you only need to use a single object. This object will perform
internal checks and will load the data according to the image types.
Then accessing any information, is done in a single way on this general
object therefore hiding the unnecessary complexity of the internal
loading.

The classes comes with a Tcl, Java and Python modules available so that
the library can be used with these languages as well.

The library also contains the a PeakList object that can be populated
with the peaks found on particular images.

Supported Detectors
-------------------

Currently the following detector/image format are implemented in this
library:

-  ADSC
-  MAR/MARCCD
-  RIGAKU
-  CBF/MINI-CBF
-  BRUKER
-  OXFORD-DIFFRACTION

Using the Diffraction Image Object
----------------------------------

Firstly, below is the description of the classes from the Diffraction
Image library, there are only two classes / objects availables which
make things a lot simple to use.

The are different ways for initialising a diffraction Image Object.
Which one is more appropriate mainly depends on what you want to do.

-  You can create an empty object and load an image in the object via
   the

   ::

       load

   method.

-  You can create an object giving the filename as a parameter to have
   everything already loaded (i.e. header and image).
-  You can create an object and only load the header via the

   ::

       loadHeader

   method. This is useful if you just need to access information from
   the header.

Using the Diffraction Image Tcl-Tk, Java and Python Modules
-----------------------------------------------------------

Firstly, you need to load or import the module. In tcl, this is done
with

::

           load DiffractionImage.[ext]
            

Where [ext] is so, dll, or dylib depending on your platform. In Python,
you need to import the class definition that is in the Diffraction.py
file with the command.

::

           from DiffractionImage import *
            

In Java, just as in tcl you need to import the shared library with the
command

::

           System.loadLibrary("DiffractionImaJ");
                

Note that you do not need to add the dll/so/dylib extension, nor do you
need to add the "lib" prefix that is part of the shared library
filename. N.B.: Make sure that the tcl module is not at the same place
as DiffractionImage.py file otherwise python will try to use the shared
library instead of the python file.

Next you need to create an "object" to work with. In tcl this is done
with one the following syntax

::

           set mydiffractionimage [new_DiffractionImage]
               or
            set mydiffractionimage [new_DiffractionImage filename]
            

where *mydiffractionimage* is the name of the tcl variable you want to
use as a pointer and where filename is the name of the diffraction image
file. Any function call on the diffraction image can then be done with
the following syntax

::

           set returnval [$mydiffractionimage functionname functionargs]
            

where *returnval* is the possible return value of the function,
*functionname* the name of the function and *functionargs* a list of all
its arguments.

In Python the creation of the object is done with one of the following
syntax

::

           mydiffractionimage = DiffractionImage()
               or
            mydiffractionimage = DiffractionImage(filename)
            

Any other function call is then done with the following syntax.

::

           returnval=mydiffractionimage.functionname(arg1,...,argn)
            

Where *arg1* to *argn* are the arguments of the function *functionname*
.

In Java the creation and use of function is very similar to the C++ one.

::

           mydiffractionimage = new DiffractionImage();
               or
            mydiffractionimage = new DiffractionImage(filename);
            

The functions calls are identical to the way you do them in C++. To
finish here are three examples of how to display the diffraction image
objects variables in python, java and tcl.

::

    if {$tcl_platform(platform) == "windows"} {
        load DiffractionImage.dll
    } else {
        load libDiffractionImage.so
        }

    set diff [new_DiffractionImage]
    $diff loadHeader "[lindex $argv 1]"
    set format [$diff getFormat]
    set epoch [$diff getDate]
    set exposure [$diff getExposureTime]
    set SN [$diff getSerialNo]
    set wl [$diff getWavelength]
    set BeamX [$diff getBeamX]
    set BeamY [$diff getBeamY]
    set dist [$diff getDistance]
    set width [$diff getWidth]
    set height [$diff getHeight]
    set pixX [$diff getPixelX]
    set pixY [$diff getPixelY]
    set oscS [$diff getOscStart]
    set oscE [$diff getOscEnd]
    set oscAx [$diff getOscAxis]
    set twoTheta [$diff getTwoTheta]

    puts "Image type: $format \nExposure Epoch: $epoch"
    puts "Exposure Time: $exposure \nDetector S/N: $SN"
    puts "Wavelength: $wl \nBeam Center: ($BeamX, $BeamY)"
    puts "Distance to detector: $dist"
    puts "Image size: ($width px, $height px)"
    puts "Pixel size: ($pixX mm, $pixY mm)"
    puts "Oscillation($oscAx): $oscS -> $oscE"
    if {$twoTheta > 0} {
        puts "Two theta value: $twoTheta\n"
    } else {
        puts "Two theta value (not in header): 0.0\n"
        }

::

    from DiffractionImage import *
    import sys
    diff=DiffractionImage()
    diff.loadHeader(sys.argv[1])
    print "Image type: ",diff.getFormat()
    print "Exposure Epoch: ",diff.getDate()
    print "Exposure Time: ",diff.getExposureTime()
    print "Detector S/N: ",diff.getSerialNo()
    print "Wavelength: ",diff.getWavelength()
    print "Beam Center: (",diff.getBeamX(),", ",diff.getBeamY(),")"
    print "Distance to Detector: ",diff.getDistance()
    print "Image size : (",diff.getWidth()," px, ",diff.getHeight()," px)"
    print "Pixel size : (",diff.getPixelX()," mm, ",diff.getPixelY()," mm)"
    print "Oscillation(",diff.getOscAxis(),") : ",diff.getOscStart()," -> ",diff.getOscEnd()
    if diff.getTwoTheta() > 0 :
        print "Two theta value: ", diff.getTwoTheta()
    else :
        print "Two theta value (not in header) : 0.0"

::

    public class diffdump
        {
        public static void main(String[] args) 
            {
            System.loadLibrary("DiffractionImaJ");
            DiffractionImage diff=new DiffractionImage();
            diff.loadHeader(args[0]);
            System.out.println("Format : "+diff.getFormat());
            System.out.println("Manufacturer : "+diff.getManufacturer());
            System.out.println("Collection date: "+diff.getDate());
            System.out.println("Exposure Time: "+Float.toString(diff.getExposureTime()));
            System.out.println("Detector S/N: "+diff.getSerialNo());
            System.out.println("Wavelength: "+Float.toString(diff.getWavelength());
            System.out.println("Beam Center: ("+Float.toString(diff.getBeamX())+
                               ", "+Float.toString(diff.getBeamY())+")");
            System.out.println("Distance to Detector: "+Float.toString(diff.getDistance()));
            System.out.println("Image size : ("+Integer.toString(diff.getWidth())+
                               " px, "+Integer.toString(diff.getHeight())+" px)");
            System.out.println("Pixel size : ("+Float.toString(diff.getPixelX())+
                               " mm, "+Float.toString(diff.getPixelY())+" mm)");
            System.out.println("Oscillation ("+diff.getOscAxis()+") :"+
                               +Float.toString(diff.getOscStart())+" -> "+
                               +Float.toString(diff.getOscEnd()));
            if(diff.getTwoTheta() >= 0)
                System.out.println("Two theta value: "+Float.toString(diff.getTwoTheta()));
            else
                System.out.println("Two theta value (not in header) : 0.0");
            }
        }
                    
                    

Classes Descriptions
--------------------

-  | DiffractionImage

   **Method**

   **Description**

   DiffractionImage()

   Default constructor of a Diffraction Image.

   DiffractionImage(\ **string** filename)

   Create and load everything available from the image file called
   "filename".

   **int** getWidth()

   Return the width of image in pixels.

   **int** getHeight()

   Return the height of image in pixels.

   **float** getBeamX()

   Return the x position of the beam center in milimeters.

   **float** getBeamY()

   Return the y position of the beam center in milimeters.

   **float** getWavelength()

   Return the wavelength of X-Ray source in Angstroms.

   **float** getDistance()

   Return the crystal to detector distance in milimeters.

   **float** getTwoTheta()

   Return the two-theta angle used in data collection

   **float** getOscStart()

   Return the scanning angle at the beginning of the oscillation

   **float** getOscEnd()

   Return the scanning angle at the end of the oscillation

   **char\*** getOscAxis()

   Return the name of the scanning axis

   **float** getPixelX()

   Return the width of a pixel in milimeters.

   **float** getPixelY()

   Return the height of a pixel in milimeters.

   **float** getExposureTime()

   Return the exposure time in miliseconds.

   **char\*** getFormat()

   Return the image format as defined by the detector manufacturer

   **char\*** getManufacturer()

   Return the named of the detector manufacturer.

   **char\*** getDate()

   Return the date of the image collection using a formatted string
   (dd-mm-YYYY). 

   **char\*** getSerialNo()

   Return the detector serial number if available return "N/A"
   otherwise.

   **unsigned long \*** getImage()

   Return a pointer to the image.

   **int** getPixelAt(\ **int** x, **int** y)

   Return the value of the pixel in position x,y on the raster image.

   **void** setBeamX(\ **float** x)

   Sets the x-position of the beam center in milimeters.

   **void** setBeamY(\ **float** y)

   Sets the y-position of the beam center in milimeters.

   **int** setWidth()

   Sets the width of image in pixels.

   **int** setHeight()

   Sets the height of image in pixels.

   **void** setWavelength(\ **float** wave)

   Sets the wavelength in Angstroms.

   **void** setDistance(\ **float** dist)

   Sets the crystal to detector distance in milimeters.

   **void** setTwoTheta(\ **float** twotheta)

   Sets the two-theta angle used in data collection.

   **void** setOscStart(\ **float** oscstart)

   Sets the value of the oscillation angle at the beginning of the
   oscillation

   **void** setOscEnd(\ **float** oscend)

   Sets the value of the oscillation angle at the end of the oscillation

   **void** setOscAxis(\ **char\*** oscaxis)

   Sets the name of the scanning axis

   **void** setPixelX(\ **float** pixelwidth)

   Sets the pixel size in milimeters

   **void** setPixelY(\ **float** pixelheight)

   Sets the pixel size in milimeters

   **void** setExposureTime(\ **float** time)

   Sets the exposure time in miliseconds

   **void** load(\ **string** fileName)

   Load in an image from the file "filename".

   **void** loadHeader(\ **string** fileName)

   Load header information only from the file "filename".

   **float\*** assymetry(\ **void**)

   | Measure how "symmetric" the distribution of intensity is as a
     function of rotation about the
   | beam centre

   **float\*** radial(\ **int** bins)

   | Produce a radial profile of the image with the average pixel count
     in each bin as the
   | measurement, from the beam centre

   **int** corner(\ **void**)

   Determine whether the image is squared or round.

   **int\*** histogram(\ **void**)

   Calculate an histogram of the pixel values encountered in the image.

   **float** resolRingRadius(\ **float** resolution)

   Calculate the radius of a ring in mm around the beam centre given a
   resolution in angstroms.

   **float** resolOnRadius(\ **float** radius)

   Calculate the resolution in angstroms on ring given its radius in mm
   around the beam centre.

   **double** gain(\ **void**)

   Calculate an estimation of the detector gain **(under development)**

   **char\*** autoMask(\ **int** beamX, **int** beamY)

   | Attempt to automagically generate a mask for the backstop area and
     the arm of
   | the backstop. The return value is of the form "-circlemask a b c
     -polymask ..."
   | where a b are the centre of the circle in pixels and c is the
     radius. There can be
   | several instance of "-polymask" followed by the coordinates in
     pixels of the vertices
   | of a quad.

   **void** min(\ **DiffractionImage\***  other)

   | Uses the image of the current object and the image from *other* to
     load the minimum image
   | in the current object. The minimum image is an image where each
     pixel is defined as the
   | minimum between the two corresponding pixel from the current object
     and other.

   **void** max(\ **DiffractionImage\***  other)

   | Uses the image of the current object and the image from *other* to
     load the maximum image
   | in the current object. The maximum image is an image where each
     pixel is defined as the
   | minimum between the two corresponding pixel from the current object
     and *other*.

   **void** diff(\ **DiffractionImage\***  other)

   | Uses the image of the current object and the image from *other* to
     load the difference image
   | in the current object. The difference image is an image where each
     pixel is defined as the
   | corresponding pixel from the current object minus the one from
     *other*.

   **void** sum(\ **DiffractionImage\***  other)

   | Uses the image of the current object and the image from *other* to
     load the sum image
   | in the current object. The sum image is an image where each pixel
     is defined as the
   | sum of the two corresponding pixel from the current object and from
     *other*.

   **void** jpeg(\ **string** filename)

   | Write out a jpeg file, named "filename" of the same size as the
     image data with a quality
   | factor of 85%

   **void** jpeg(\ **string** filename,\ **int** quality)

   Same as previous function but the quality factor can be specified

   | **void** jpeg(\ **string** filename,\ **int** quality,
   |                 **int** zoom)

   | Same as previous function but the zoom level can be specified (-2
     means 50%, -3 means
   | 33%, 2 means 200%,...)

   | **void** jpeg(\ **string** filename,\ **int** quality,
   |                 **int** zoom,\ **bool** optmise)

   | Same as previous function but you can specify whether or not to do
     a cycle of optimisation
   | before writing out the image.

-  | PeakList

   **Method**

   **Description**

   PeakList()

   Default constructor of a empty PeakList.

   PeakList(\ **DiffractionImage\*** diffractionimage)

   Construct a PeakList from the given DiffractionImage object.

   **int** length()

   Gives the length of the list.

   **void** clear()

   Clears the list.

   **void** add(\ **float** x, **float** y, **float** intensity)

   Add a peak the image coordinates (x,y) and with the specified
   intensity.

   **void** remove(\ **int** offset)

   Remove the Peak at the specified position in the list.

   **void** add(\ **Peak** peak)

   Add the specified peak to the list.

   **void** find(\ **DiffractionImage\*** diffractionimage)

   Search for the peaks in the specified DiffractionImage object.

   | **void** find(\ **DiffractionImage\*** diffractionimage,
   |                **int** maxPeaks)

   Search for the peaks in the specified DiffractionImage object with a
   maximum of maxPeaks results.

   | **void** find(\ **DiffractionImage\*** diffractionimage,
   |                **int** maxPeaks,
   |                **float** intensityThreshold)

   Search for the peaks in the specified DiffractionImage object with a
   maximum of maxPeaks results having an intensity of at least
   intensityTreshold.

   **void** reciprocal(\ **float** dist, **float** wave)

   Convert the peak coordinates from dectetor coordinates to reciprocal
   space coordinates.

   **void** detector(\ **float** dist, **float** wave)

   Convert the peak coordinates from reciprocal space coordinates to
   detector coordinates.

   **Peak** pop(\ **int** offset)

   Gets the peak from the specified position and removes it from the
   list.

   **Peak \*** getPeaks()

   Return the content of the PeakList as a vector of Peak.

   | **int** circle(\ **int** iter, **float** width,
   |               **float**\ & x, **float**\ & y, **float**\ & r)

   Find a circle in the spot using a stochastic process and return the
   most promising candidate. Width determines how close to the arc a
   spot must be to be accepted. Iter is the number of attemps in fitting
   a circle.

   **int** remove(\ **float** width, **float** x, **float** y, **float**
   r)

   Removes the spots that were found to be on the circle fitted with the
   circle() method described above.

-  Peak is a structure having the following 7 fields,

   -  **float** x : x coordinate of the peak on the image
   -  **float** y : y coordinate of the peak on the image
   -  **float** theta : Theta? what is theta on this context? 
   -  **float** phi : phi? what is phi on this context?
   -  **float** kappa : kappa? what is kappa on this context?
   -  **float**\ [3] p : Array containing the reciprocal spaces
      coordinates of the Peak
   -  **float** intensity : Intensity of the Peak

Programs
--------

Currently the library provides also four simple programs called
"diffdump", "printpeaks", automask and diff2jpeg. These are respectively
used to display all the "standard" information of a Diffraction Image,
printing the list of peaks found on the image, wrapping the Automask
function from the library and converting the image data to a jpeg file.
These are also simple code examples of how to use the object from the
library.

printpeaks usage:       

::

    "printpeaks [-th <intensity_threshold>] <filename>"

-th is an optional keyword to specify the intensity threshold for peak
searching.

diffdump usage:       

::

    "diffdump [-gain] <filename>"

 -gain is an optional keyword to ask for a detector gain estimation.

automask usage:       

::

    "automask [-beam <x> <y>] <filename>"

 -beam is an optional keyword to speficy the start point (in pixels
coordinates) for the algorithm.

diff2jpeg usage:       

::

    "diff2jpeg [-thumbnail] <filename>"

 -thumbnail is an optional keyword to ask for the creation of a
thumbnail version of the image in addition to the real image The
thumbnail image is created to be as near as 400x400 as possible.


