CCP4 v6.4.0 Program Changes
===========================

INDEX

A list of general acknowledgements for testing CCP4 v6.4.0 can be found
`here <ACKNOWLEDGEMENTS_V6_4.html>`__.

--------------

New programs:
\* New programs \* QTRVIEW: output viewing program \* CCP4UM: update
mechanism \* QTPISA: graphical version of pisa \* Program changes: \*
PHASER: update to 2.5.5 \* REFMAC5: update to 5.8 \* CTRUNCATE: update
to 1.12.2 \* AIMLESS: update to 0.2.1 \* POINTLESS: update to 1.8.4 \*
DIMPLE: update to 1.2 (now works on Windows) \* Graphical User Interface
\* import from imosflm quickscale \* crossec interface for estimation on
anomalous scattering \* Documentation \* All documentation is now in the
directory 'html'. The doc and man directories have been removed. \* CCP4
FAQ has been removed. \* Coot \* Coot now included in CCP4 Linux binary
distributions

-  QTRVIEW: output viewing program
-  CCP4UM: update mechanism
-  QTPISA: graphical version of pisa

--------------

Program changes:

-  PHASER: updated to 2.5.5
-  REFMAC: updated to 5.8
-  CTRUNCATE: updated to 1.13.0
-  AIMLESS: updated to 0.2.1
-  POINTLESS: updated to 1.8.4
-  DIMPLE: update to 1.2 (windows support)

--------------

Graphical User Interface:

-  import from imosflm quickscale interface
-  crossec interface for estimation of anomalous scattering
-  output viewing through qtrview

--------------

Documentation:

-  All documation is not in the directory 'html'. The doc and man
   directories have been removed
-  CCP4 FAQ have been removed

--------------

COOT:

-  coot 0.7.1 now included in CCP4 linux and OS X bundles
