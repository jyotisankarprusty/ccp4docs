.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: XML
   :name: xml
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 How to Use XML Output <#How_to_Use_XML_Output>`__                  |
|                                                                          |
|    -  `1.1 Generating XML Output <#Generating_XML_Output>`__             |
|    -  `1.2 Tags <#Tags>`__                                               |
|    -  `1.3 Error Handling <#Error_Handling>`__                           |
|    -  `1.4 Logfile Handling <#Logfile_Handling>`__                       |
|                                                                          |
| -  `2 XML Output Examples <#XML_Output_Examples>`__                      |
|                                                                          |
|    -  `2.1 Anisotropy Correction <#Anisotropy_Correction>`__             |
|    -  `2.2 Cell Content Analysis <#Cell_Content_Analysis>`__             |
|    -  `2.3 Normal Mode Analysis <#Normal_Mode_Analysis>`__               |
|    -  `2.4 Automated Molecular                                           |
|       Replacement <#Automated_Molecular_Replacement>`__                  |
|    -  `2.5 Automated Experimental                                        |
|       Phasing <#Automated_Experimental_Phasing>`__                       |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

Phaser can generate XML output from keyword input. XML output should be
used in preference to grepping logfiles when incorporating Phaser into
automation pipelines. Note that Phaser's python scripting ability is a
more powerful way of calling Phaser for automation pipelines.

We would like to hear from developers who wish to incorporate Phaser
into automation scripts using the XML functionality
cimr-phaser@lists.cam.ac.uk

.. rubric:: How to Use XML Output
   :name: how-to-use-xml-output

.. rubric:: Generating XML Output
   :name: generating-xml-output

Phaser outputs an XML file when called with the command line argument
-xml followed by the filename for output.

::

      phaser -xml <filename>

If no filename is given, Phaser exits immediately and writes an XML file
with filename PHASER.XML describing the error (type="FILE OPENING",
message="No XML filename").

.. rubric:: Tags
   :name: tags

All XML output is wrapped between "phaser" tags with the version number
for the Phaser executable and the operating system on which the output
was produced as attributes.

::

      <phaser version="2.1" ostype="linux">
        ---All Phaser XML output---
      <\phaser> 

Names of files are output using the "file" tags with attributes "type"
specifying reflections ("HKL") or coordinates ("XYZ") and attribute
"format". Currently only "MTZ" and "PDB" formats are output.

::

      <file type="HKL" format="MTZ">filename</file>
      <file type="XYZ" format="PDB">filename</file>

Other XML tags are a combination of those suggested by the SPINE
consortium and Phaser specific tags. There is no schema. Please refer to
the examples.

.. rubric:: Error Handling
   :name: error-handling

Successful Phaser execution (not necessarily structure solution!) is
reported as

::

      <status>ok</status> 

Failure during execution is reported as

::

      <status>error</status> 

More information about the type of error is given with

::

      <error>
        <name>ERROR_NAME</name>
        <message>ERROR_MESSAGE</message>
      </error> 

Allowed values for ERROR\_NAME are given in the table below, and specify
the type of error. The ERROR\_MESSAGE gives more information as to the
specific cause of the error.

::

      ERROR_NAME    Failure due to …
      SYNTAX    Syntax error in keyword input
      INPUT     Input error (e.g. invalid value for an input parameter)
      FILE OPENING  Unable to open file (given in ERROR_MESSAGE) for reading or writing.
      OUT OF MEMORY     Memory exhaustion
      FATAL RUNTIME     Fatal runtime error (e.g. bug in Phaser)
      UNHANDLED     Other unhandled fatal error (e.g. bug in libraries)
      UNKNOWN   Other error (e.g. bug in compiler)

.. rubric:: Logfile Handling
   :name: logfile-handling

Use the keyword MUTE ON to prevent the writing of the logfile to
standard output. Only the XML file and other results files (mtz,pdb)
will be produced by Phaser.

.. rubric:: XML Output Examples
   :name: xml-output-examples

Below are example XML output files produced by running the most popular
modes of Phaser: anisotropy correction, automated molecular replacement,
cell content analysis, normal mode analayis, and SAD phasing.

.. rubric:: Anisotropy Correction
   :name: anisotropy-correction

Output XML file for anisotropy correction of BETA-BLIP

::

      <phaser version="2.1" ostype="linux">
        <status>ok</status>
        <anisotropy_info>
          <file type="HKL" format="MTZ">beta_blip_ano.mtz</file>
          <eigenB type="1">10.88</eigenB>
          <eigenB type="2">10.88</eigenB>
          <eigenB type="3">-21.76</eigenB>
          <eigenB type="delta">32.64</eigenB>
          <wilson type="K">20.25</wilson>
          <wilson type="B">56.97</wilson>
        </anisotropy_info>
      </phaser>

.. rubric:: Cell Content Analysis
   :name: cell-content-analysis

Output XML file for Cell Content Analysis of BETA-BLIP

::

      <phaser version="2.1" ostype="linux">
        <status>ok</status>
        <cell_content_analysis_info>
          <mw>46375.000000</mw>
          <solution id="0"</cca>
            <Z>1</Z>
            <probVM>0.995130</probVM>
            <VM>2.340743</VM>
          </solution>
        </cell_content_analysis_info>
      </phaser>

.. rubric:: Normal Mode Analysis
   :name: normal-mode-analysis

Output XML file for Normal Mode Analysis of beta.pdb (modes 7 and 10,
displacement forward only)

::

      <phaser version="2.1" ostype="linux">
        <status>ok</status>
        <normal_modes id="1">
          <file type="XYZ" format="PDB">beta_nma.1.pdb</file>
          <displacement mode="7">0.000000</displacement>
          <displacement mode="10">0.000000</displacement>
        </normal_modes>
        <normal_modes id="2">
          <file type="XYZ" format="PDB">beta_nma.2.pdb</file>
          <displacement mode="7">0.000000</displacement>
          <displacement mode="10">47.306646</displacement>
        </normal_modes>
        <normal_modes id="3">
          <file type="XYZ" format="PDB">beta_nma.3.pdb</file>
          <displacement mode="7">46.881456</displacement>
          <displacement mode="10">0.000000</displacement>
        </normal_modes>
        <normal_modes id="4">
          <file type="XYZ" format="PDB">beta_nma.4.pdb</file>
          <displacement mode="7">46.881456</displacement>
          <displacement mode="10">47.306646</displacement>
        </normal_modes>
        <normal_modes id="5">
          <file type="XYZ" format="PDB">beta_nma.5.pdb</file>
          <displacement mode="7">93.762912</displacement>
          <displacement mode="10">0.000000</displacement>
        </normal_modes>
        <normal_modes id="6">
          <file type="XYZ" format="PDB">beta_nma.6.pdb</file>
          <displacement mode="7">93.762912</displacement>
          <displacement mode="10">47.306646</displacement>
        </normal_modes>
      </phaser>

.. rubric:: Automated Molecular Replacement
   :name: automated-molecular-replacement

Output XML file for Automated Molecular Replacement of BETA-BLIP

::

      <phaser version="2.1" ostype="linux">
        <status>ok</status>
        <dataset_info>
          <cell>
            <a>75.11</a>
            <b>75.11</b>
            <c>133.31</c>
            <alpha>90</alpha>
            <beta>90</beta>
            <gamma>120</gamma>
          </cell>
          <spacegroup>
            <hall> P 32 2"</hall>
            <number>154</number>
            <lattice>P</lattice>
            <operator id="0">32</operator>
            <operator id="1">2</operator>
            <operator id="2">1</operator>
          </spacegroup>
          <n_symops>6</n_symops>
          <symmetry_operator id="0">
            <aa>1</aa> <ab>0</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>1</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="1">
            <aa>0</aa> <ab>1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>-1</ba> <bb>-1</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0.666667</ctrans>
          </symmetry_operator>
          <symmetry_operator id="2">
            <aa>-1</aa> <ab>-1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>1</ba> <bb>0</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0.333333</ctrans>
          </symmetry_operator>
          <symmetry_operator id="3">
            <aa>0</aa> <ab>1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>1</ba> <bb>0</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="4">
            <aa>-1</aa> <ab>-1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>1</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>0.666667</ctrans>
          </symmetry_operator>
          <symmetry_operator id="5">
            <aa>1</aa> <ab>0</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>-1</ba> <bb>-1</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>0.333333</ctrans>
          </symmetry_operator>
        </dataset_info>
        <solution id="1">
          <file type="HKL" format="MTZ">beta_blip.1.mtz</file>
          <file type="XYZ" format="PDB">beta_blip.1.pdb</file>
          <llg>996.141543</llg>
        </solution>
      </phaser>

.. rubric:: Automated Experimental Phasing
   :name: automated-experimental-phasing

Output XML file for SAD phasing of insulin

::

      <phaser version="2.1" ostype="linux">
        <status>ok</status>
        <dataset_info>
          <cell>
            <a>78.046</a>
            <b>78.046</b>
            <c>78.046</c>
            <alpha>90</alpha>
            <beta>90</beta>
            <gamma>90</gamma>
          </cell>
          <spacegroup>
            <hall> I 2b 2c 3</hall>
            <number>199</number>
            <lattice>I</lattice>
            <operator id="0">21</operator>
            <operator id="1">3</operator>
            <operator id="2">3</operator>
          </spacegroup>
          <n_symops>24</n_symops>
          <symmetry_operator id="0">
            <aa>1</aa> <ab>0</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>1</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="1">
            <aa>-1</aa> <ab>0</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>-1</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="2">
            <aa>1</aa> <ab>0</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>-1</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="3">
            <aa>-1</aa> <ab>0</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>1</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="4">
            <aa>0</aa> <ab>1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>0</bb> <bc>1</bc> <btrans>0</btrans>
            <ca>1</ca> <cb>0</cb> <cc>0</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="5">
            <aa>0</aa> <ab>-1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>0</bb> <bc>1</bc> <btrans>0.5</btrans>
            <ca>-1</ca> <cb>0</cb> <cc>0</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="6">
            <aa>0</aa> <ab>-1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>0</bb> <bc>-1</bc> <btrans>0</btrans>
            <ca>1</ca> <cb>0</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="7">
            <aa>0</aa> <ab>1</ab> <ac>0</ac> <atrans>0</atrans>
            <ba>0</ba> <bb>0</bb> <bc>-1</bc> <btrans>0.5</btrans>
            <ca>-1</ca> <cb>0</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="8">
            <aa>0</aa> <ab>0</ab> <ac>1</ac> <atrans>0</atrans>
            <ba>1</ba> <bb>0</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>1</cb> <cc>0</cc> <ctrans>0</ctrans>
          </symmetry_operator>
          <symmetry_operator id="9">
            <aa>0</aa> <ab>0</ab> <ac>-1</ac> <atrans>0</atrans>
            <ba>1</ba> <bb>0</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>-1</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="10">
            <aa>0</aa> <ab>0</ab> <ac>-1</ac> <atrans>0</atrans>
            <ba>-1</ba> <bb>0</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>1</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="11">
            <aa>0</aa> <ab>0</ab> <ac>1</ac> <atrans>0.5</atrans>
            <ba>-1</ba> <bb>0</bb> <bc>0</bc> <btrans>0</btrans>
            <ca>0</ca> <cb>-1</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="12">
            <aa>1</aa> <ab>0</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>1</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="13">
            <aa>-1</aa> <ab>0</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>-1</bb> <bc>0</bc> <btrans>1</btrans>
            <ca>0</ca> <cb>0</cb> <cc>1</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="14">
            <aa>1</aa> <ab>0</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>-1</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>1</ctrans>
          </symmetry_operator>
          <symmetry_operator id="15">
            <aa>-1</aa> <ab>0</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>1</bb> <bc>0</bc> <btrans>1</btrans>
            <ca>0</ca> <cb>0</cb> <cc>-1</cc> <ctrans>1</ctrans>
          </symmetry_operator>
          <symmetry_operator id="16">
            <aa>0</aa> <ab>1</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>0</bb> <bc>1</bc> <btrans>0.5</btrans>
            <ca>1</ca> <cb>0</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="17">
            <aa>0</aa> <ab>-1</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>0</bb> <bc>1</bc> <btrans>1</btrans>
            <ca>-1</ca> <cb>0</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="18">
            <aa>0</aa> <ab>-1</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>0</bb> <bc>-1</bc> <btrans>0.5</btrans>
            <ca>1</ca> <cb>0</cb> <cc>0</cc> <ctrans>1</ctrans>
          </symmetry_operator>
          <symmetry_operator id="19">
            <aa>0</aa> <ab>1</ab> <ac>0</ac> <atrans>0.5</atrans>
            <ba>0</ba> <bb>0</bb> <bc>-1</bc> <btrans>1</btrans>
            <ca>-1</ca> <cb>0</cb> <cc>0</cc> <ctrans>1</ctrans>
          </symmetry_operator>
          <symmetry_operator id="20">
            <aa>0</aa> <ab>0</ab> <ac>1</ac> <atrans>0.5</atrans>
            <ba>1</ba> <bb>0</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>1</cb> <cc>0</cc> <ctrans>0.5</ctrans>
          </symmetry_operator>
          <symmetry_operator id="21">
            <aa>0</aa> <ab>0</ab> <ac>-1</ac> <atrans>0.5</atrans>
            <ba>1</ba> <bb>0</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>-1</cb> <cc>0</cc> <ctrans>1</ctrans>
          </symmetry_operator>
          <symmetry_operator id="22">
            <aa>0</aa> <ab>0</ab> <ac>-1</ac> <atrans>0.5</atrans>
            <ba>-1</ba> <bb>0</bb> <bc>0</bc> <btrans>1</btrans>
            <ca>0</ca> <cb>1</cb> <cc>0</cc> <ctrans>1</ctrans>
          </symmetry_operator>
          <symmetry_operator id="23">
            <aa>0</aa> <ab>0</ab> <ac>1</ac> <atrans>1</atrans>
            <ba>-1</ba> <bb>0</bb> <bc>0</bc> <btrans>0.5</btrans>
            <ca>0</ca> <cb>-1</cb> <cc>0</cc> <ctrans>1</ctrans>
          </symmetry_operator>
        </dataset_info>
        <solution_info>
          <file type="XYZ" format="PDB">insulin.pdb</file>
          <file type="HKL" format="MTZ">insulin.mtz</file>
          <llg>81640.054723</llg>
        </solution_info>
      </phaser>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
