AFRO Version 0.85 - Documentation
=================================

NAME
----

afro - multivariate substructure factor amplitude estimation for SAD/MAD
and SIRAS.

SYNOPSIS
--------

afro hklin foo.mtz hklout foo\_out.mtz

Keyworded input

DESCRIPTION
-----------

This program will determine normalized substructure factor amplitudes
(ie. E(A) values) which are needed for direct methods substructure
determination programs. In this version, the multivariate formulas are
not implemented, but just a standard approach is taken for finding SAD
or SIR differences (and attempting to find the best wavelength to
calculate the anomalous differences).

GETTING STARTED
---------------

The best way to start is to use the example scripts.

KEYWORDED INPUT
---------------

XTAL <ID>
~~~~~~~~~

<ID>
    The crystal's name/identification string.

XTAL SUBKEYWORDS:
~~~~~~~~~~~~~~~~~

CELL
^^^^

<a> <b> <c> <alpha> <beta> <gamma>

 <a> <b> <c> <alpha> <beta> <gamma>
    Cell parameters for the given XTAL. Default: take the values from
    the mtz file.

DNAMe <ID>
^^^^^^^^^^

The dataset identifier. This keyword is required.

DNAMe SUBKEYWORDS:
^^^^^^^^^^^^^^^^^^

COLUmn F=<f> SF=<sf> F+=<f+> SF+=<sf+> F-=<f-> SF-=<sf->
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Diffraction data for the XTAL and DNAMe defined. If anomalous data is
not to be used, set F and SF only. If using anomalous data, set F+, SF+,
F-, SF-. Setting both F and F+ will result in an error. If only F and
DANO is present in the mtz file, use the ccp4 program mtzMADmod to
change F/DANO to F+/F-.

 <f>
    \|F\| (observed structure factor amplitude \*if no anomalous data is
    present\*).
 <sf>
    Corresponding sigma for <f>.
 <f+>
    \|F+\| (observed structure factor amplitude of positive Bijvoet
    pair).
 <sf+>
    Corresponding sigma of <f+>.
 <f->
    \|F-\| (observed structure factor amplitude of negative Bijvoet
    pair).
 <sf->
    Corresponding sigma of <f->.

RESO <hires> <lores>
''''''''''''''''''''

Specify resolution limits for the given XTAL and DNAMe diffraction data.
Do not use this keyword to specify a resolution cut-off for substructure
detection, but use the HIREs keyword from AFRO instead. Only use this
keyword if you wish to remove some data that you believe to be useless.
Default: use all the data available in the mtz file.

EXCLude [ SIGF <nsigf> ] [ SANO<nsano> ] [SISO ]
''''''''''''''''''''''''''''''''''''''''''''''''

Set criteria for excluding E-values to output for the given XTAL and
DNAMe diffraction data. This is essential for correct substructure
determination. The default is to use all the data available in the mtz
file and this is not optimal for EA calculation.

The criteria and cutoff recommendations for the subkeywords are the
following:

SIGF <nsigf>
''''''''''''

exclude reflections if FP < <nsigf> \* SIGF. The recommended setting is
3.

SANO <nsano>
''''''''''''

exclude reflections if DANO < <nsano> \* SDANO. The recommended setting
is 1. (DANO = abs(\|F+\| - \|F-\|) and SDANO is the standard deviation
of DANO in measurement.

SISO <nsiso>
''''''''''''

exclude reflections if abs(\|Fder\| - \|Fnat\|) < \* SDISO. The
recommended setting is 1. SDISO is sqrt(SIGFder^2 + SIGFnat^2).

BINS
''''

Number of bins for luzzati parameter estimation and refinement and
output of statistics.

OPTIONAL KEYWORDS:
------------------

OUTPut <outputname>
~~~~~~~~~~~~~~~~~~~

<outputname> is the string associated with the ASCII file that afro
writes out. The default <outputname> is "heavy".

ASCIi
~~~~~

Write out an unsorted drear file suitable for crunch2.

HIREs <hireslimit>
~~~~~~~~~~~~~~~~~~

Only output FA values to the following high resolution limit. The
default is for the program to calculate it automatically.

ACENtric
~~~~~~~~

Only acentric reflections will be used. This is the default for SAD
data.

CENTric
~~~~~~~

Only centric reflections will be used.

TITLe <title>
~~~~~~~~~~~~~

Title to be added to the mtz file. Default: "EA values from AFRO".

VERBose <n>
~~~~~~~~~~~

Specify amount of information to be outputted (where n is a positive
integer). n = 0 is the normal output, n = 1 is more output and n = 2 is
for debugging purposes. Default: n = 0.

OUTPUT:
-------

COLUMNS in HKLOUT
~~~~~~~~~~~~~~~~~

EXAMPLES
--------

Example 1 - SAD experiment
--------------------------

::


    #!/bin/sh

    set -e

    afro HKLIN $CEXAM/tutorial/data/gere_MAD_nat.mtz \
        HKLOUT $CCP4_SCR/gere_SADEA.mtz << eof-afro

    Xtal SeMET
     DNAME PEAK
       COLUmn F+=F_peak(+) SF+=SIGF_peak(+) F-=F_peak(-) SF-=SIGF_peak(-)
       EXCLUDE SIGF 3 SANO 1       # don't use data with F/SF < 3 or DANO/SIGDANO < 1
       RESOL HICUt 0.5             # only use data 0.5 Angstroms about hi res limit
    ALLIn

    eof-afro

    Example 2: MAD EA value generation

    #!/bin/sh

    set -e

    afro HKLIN $CEXAM/tutorial/data/gere_MAD_nat.mtz \
        HKLOUT $CCP4_SCR/gere_MADEA.mtz << eof-afro

    Xtal SeMET
     DNAME PEAK
       COLUmn F+=F_peak(+) SF+=SIGF_peak(+) F-=F_peak(-) SF-=SIGF_peak(-)
       EXCLUDE SIGF 3 SANO 1        # don't use data with F/SF < 3 or DANO/SIGDANO < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit
     DNAME INFL
       COLUmn F+=F_infl(+) SF+=SIGF_infl(+) F-=F_infl(-) SF-=SIGF_infl(-)
       EXCLUDE SIGF 3 SANO 1 SISO 1 # don't use data with F/SF < 3 or DANO/SIGDANO < 1
                                    # or isomorphous/dispersive difference < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit
     DNAME HIGH
       COLUmn F+=F_hrm(+) SF+=SIGF_hrm(+) F-=F_hrm(-) SF-=SIGF_hrm(-)
       EXCLUDE SIGF 3 SANO 1        # don't use data with F/SF < 3 or DANO/SIGDANO < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit

                                    # NOTE! Afro takes only isomorphous/dispersive differences between
                                    # the first two defined data sets!!!
                                    # You could also define the low remote as well...
    ALLIn

    eof-afro

    Example 3: SIRAS EA value generation

    #!/bin/sh

    set -e

    afro HKLIN $CEXAM/tutorial/data/gere_MAD_nat.mtz \
        HKLOUT $CCP4_SCR/gere_SIRASEA.mtz << eof-afro

    # define the native first!!

    Xtal NATIVE
     DNAME NATIVE
       COLUmn F=F_nat SF=SIGF_nat
       EXCLUDE SIGF 3               # don't use data with F/SF < 3
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit

    Xtal SeMET
     DNAME PEAK
       COLUmn F+=F_peak(+) SF+=SIGF_peak(+) F-=F_peak(-) SF-=SIGF_peak(-)
       EXCLUDE SIGF 3 SANO 1 SISO 1 # don't use data with F/SF < 3 or DANO/SIGDANO < 1
                                # or isomorphous difference/sigma < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit

    ALLIn

    eof-afro

    Example 4: MAD + Native EA value generation

    #!/bin/sh

    set -e

    afro HKLIN $CEXAM/tutorial/data/gere_MAD_nat.mtz \
        HKLOUT $CCP4_SCR/gere_MADNATEA.mtz << eof-afro

    # define the native first!!

    Xtal NATIVE
     DNAME NATIVE
       COLUmn F=F_nat SF=SIGF_nat
       EXCLUDE SIGF 3               # don't use data with F/SF < 3
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit

    Xtal SeMET
     DNAME PEAK
       COLUmn F+=F_peak(+) SF+=SIGF_peak(+) F-=F_peak(-) SF-=SIGF_peak(-)
       EXCLUDE SIGF 3 SANO 1 SISO 1 # don't use data with F/SF < 3 or DANO/SIGDANO < 1
                                # or isomorphous difference/sigma < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit
     DNAME INFL
       COLUmn F+=F_infl(+) SF+=SIGF_infl(+) F-=F_infl(-) SF-=SIGF_infl(-)
       EXCLUDE SIGF 3 SANO 1 SISO 1 # don't use data with F/SF < 3 or DANO/SIGDANO < 1
                                    # or isomorphous/dispersive difference < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit
     DNAME HIGH
       COLUmn F+=F_hrm(+) SF+=SIGF_hrm(+) F-=F_hrm(-) SF-=SIGF_hrm(-)
       EXCLUDE SIGF 3 SANO 1        # don't use data with F/SF < 3 or DANO/SIGDANO < 1
       RESOL HICUt 0.5              # only use data 0.5 Angstroms about hi res limit

                                    # NOTE! Afro takes only isomorphous/dispersive differences between
                                    # the first two defined data sets!!!
                                    # You could also define the low remote as well...
    ALLIn

    ##############################################################

--------------

Last modified: Fri Aug 12 19:22:44 CEST 2005
