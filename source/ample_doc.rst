**AMPLE (CCP4: Unsupported Program)**
=====================================

**NAME**
--------

**AMPLE** - automated de novo search model generation for molecular
replacement

**SYNOPSIS**
------------

Utilises programs like Rosetta to generate ab initio models to be used
in molecular replacement:

**ample.py** [-h] [-ROSETTA *ROSETTA\_path*] [-RDB *ROSETTA\_database*]
[-fragsexe *path to make\_fragments.pl*] [-Rosetta\_cluster *path to
Rosettas cluster*] [-fasta *fasta\_file*] [-name *priotein name*]
[-NProc *NoProcessors*] [-RunDir *run\_directory*] [-SCWRL *path to
scwrl*] [-LGA *path\_to\_LGA dir*] [-MAX *Maxcluster exe*] [-THESEUS
*Theseus exe required*] [-MTZ *MTZ in*] [-MODELS *folder of decoys*]
[-MakeModels *Do the modelling*] [-ROSETTA\_DIR *Rosetta\_dir*]
[`Command line options <>`__]

**DESCRIPTION**
---------------

AMPLE has three main parts:

#. For a given target sequence, calls Rosetta for the ab initio modeling
   of protein decoys, or reads in a set of pre-made models
#. Preparation of decoys into search ensembles for molecular replacement
#. Running molecular replacement using these search ensembles and
   inputting MR results into SHELX

| 

**DEPENDENCIES**
----------------

Before AMPLE can be used, the following dependencies should be installed
on the local system.

-  **SCWRL** (version 4.0) available from: `**Rosetta** (tested with
   versions 3.2 and
   3.3) <http://dunbrack.fccc.edu/scwrl4/#installation>`__\ `Rosetta <http://www.rosettacommons.org/>`__
-  **Theseus** (ver. 1.4.3)
   `Theseus <http://www.theseus3d.org/src/theseus_align>`__
-  **Maxcluster** (ver 0.6.6) (as a replacement for LGA)
   `Maxcluster <http://www.sbg.bio.ic.ac.uk/%7Emaxcluster/download.html>`__
-  **SHELX** (Beta version of shelxe)
-  phenix (optional)

**INPUT AND OUTPUT FILES**
--------------------------

**HKLIN**
~~~~~~~~~

Input structure factor file for target structure. Must include a FreeR
flag column.

| 

**FASTA**
~~~~~~~~~

Fasta of the target, followed by a new line.

| 

| 

**Intermediate Output files**

Generated models will output to the "Models" directory.

The models are clustered and the clustered models are output to the
"clusters" directory.

Ensembles are made and places in the "ensembles" directory. 

MRBUMP will run in the "MRBUMP" directory.

SHELX will run in each MRBUMP run directory

| 

**Final Output files**

Potential solutions will be placed in the "RESULTS" directory

**COMMAND LINE INPUT**
----------------------

optional arguments:

-  -h, --help     show this help message and exit

-  -ROSETTA *ROSETTA\_path*     path for Rosetta AbinitioRelax

-  -RDB *ROSETTA\_database*     path for Rosetta database

-  -fragsexe *path to make\_fragments.pl*     location of
   make\_fragments.pl

-  -Rosetta\_cluster *path to Rosettas cluster*     location of rosetta
   cluster

-  -fasta *fasta\_file*     protein fasta file. (required)

-  -name *priotein name*     name of protien in the format ABCD

-  -NProc *NoProcessors*     number of processers (default 1)

-  -RunDir *run\_directory*     directory to put files (default current
   dir)

-  -SCWRL *path to scwrl*     pathway to SCWRL exe

-  -LGA *path\_to\_LGA dir*     pathway to LGA folder (not the exe) will
   use the 'lga' executable

-  -MAX *Maxcluster exe*     Maxcluster exe

-  -THESEUS *Theseus exe (required)*     Theseus exe

-  -MTZ *MTZ in*     MTZ in

-  -MODELS *folder of decoys*     folder of decoys

-  -MakeModels *Do the modelling*     run rosetta modeling, set to False
   to import pre-made models (required if making models locally default
   True)

-  -ROSETTA\_DIR *Rosetta\_dir*     the Rosetta install directory

-  -F *Flag for F*

-  -SIGF *Flag for SIGF*

-  -FREE *FreeR flag*

-  -CLUSTER True if using a cluster, false if a multi-core PC 

-  -percent percent of the model to truncate at each interval 

-  -NumShelxCyc number of shelxe cycles to perform?\|

-  -NoShelx will not run shelx, will only run MRBUMP?\|

-  -TRYALL if True will try all ensembles, if False will exit at the
   first success?\|

**Using the GUI**
-----------------

**Input Files**
~~~~~~~~~~~~~~~

HKLIN: mtz file. The flags F, SIGF and FREE must be set.
number of processors: int for parallel running for speed
Fasta: must have a new line char at end of sequence
work directory: where all run files are generated

**Models Import**
~~~~~~~~~~~~~~~~~

Make Models: if you want Rosetta to make the models locally, specify
"True". If you have models already made specify "False" and give a path
to a folder containing them.
Models Path: specify "none" to make them locally. give a path if
importing.

**Rosetta Quick**
~~~~~~~~~~~~~~~~~

Rosetta Install Directory: Install folder of Rosetta will try to find
paths of the executables.

**Rosetta Full Pathways**
~~~~~~~~~~~~~~~~~~~~~~~~~

for a non standard install, the paths may be different. So give the
following paths:
AbinitioRelax.linuxgccrelease (executable)
rosetta\_database (folder pathway)
make\_fragments.pl (executable)
cluster.linuxgccrelease (executable)

**3rd Party Locations**
~~~~~~~~~~~~~~~~~~~~~~~

| For some functions some 3rd party executables are needed, give the
  pathways if not in PATH:
  maxcluster (executable)
  Scwrl4 (executable)
  theseus (executable)

| 

| 

**EXAMPLE USAGE**
-----------------

**PROGRAM OUTPUT**
------------------

**AUTHORS**
-----------

Jaclyn Bibby, University of Liverpool, UK 

**ACKNOWLEDGEMENTS**
--------------------

Daniel Rigden, Martyn Winn, Olga Mayans.

**AMPLE Program References**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Any publication arising from use of AMPLE should include the following
reference:

AMPLE: a cluster-and-truncate approach to solve crystal structures of
small proteins using rapidly computed ab initio models

In addition, authors of specific programs should be referenced where
applicable:

MrBUMP

R.M.Keegan and M.D.Winn (2007) *Acta Cryst.* **D63**, 447-457

CCP4

Collaborative Computational Project, Number 4. (1994), "The CCP4 Suite:
Programs for Protein Crystallography". *Acta Cryst.* **D50**, 760-763

FASTA

W. R. Pearson and D. J. Lipman (1988), "Improved Tools for Biological
Sequence Analysis", *PNAS* **85**, 2444-2448

MOLREP

A.A.Vagin & A.Teplyakov (1997) *J. Appl. Cryst.* **30**, 1022-1025

PHASER

McCoy, A.J., Grosse-Kunstleve, R.W., Storoni, L.C. & Read, R.J. (2005).
"Likelihood-enhanced fast translation functions" *Acta Cryst* **D61**,
458-464

REFMAC

G.N. Murshudov, A.A.Vagin and E.J.Dodson, (1997) "Refinement of
Macromolecular Structures by the Maximum-Likelihood Method" *Acta
Cryst.* **D53**, 240-255
